USE expert_game
GO

CREATE TABLE dbo.Producto
(
P_id_pro int PRIMARY KEY ,
Nombre_Producto varchar(50) NOT NULL,
Tipo_producto varchar(50) NOT NULL,
Valor_producto money NOT NULL,
Fecha_ingreso Datetime NOT NULL
)
GO

CREATE TABLE dbo.Sucursal
(
P_id_suc int PRIMARY KEY,
TipoSucursal varchar(50) NOT NULL,

)
GO

CREATE TABLE dbo.Administrador
(
rut_adm varchar(20) NOT NULL PRIMARY KEY,
P_id_suc numeric,
Nombre_adm Varchar(50),
Apellido_adm Varchar(50),
pass Varchar(50),
pfl_adm int 
)
GO

CREATE TABLE dbo.tipoProducto
(
P_id_tipoProducto int PRIMARY KEY NOT NULL,
Nombre_categoria varchar(50) NOT NULL,
Estado_producto numeric(10)
)
GO

CREATE TABLE dbo.Vendedor
(
rut_vendedor varchar(20) NOT NULL PRIMARY KEY,
Nombre_vendedor Varchar(50),
Apellido_vendedor Varchar(50),
pass_vendedor Varchar(50),
pfl_vendedor int
)
GO

CREATE TABLE dbo.Clientes
(
rut_cliente varchar(20) NOT NULL PRIMARY KEY,
Nombre_cliente Varchar(50),
Apellido_cliente Varchar(50)
)
GO

