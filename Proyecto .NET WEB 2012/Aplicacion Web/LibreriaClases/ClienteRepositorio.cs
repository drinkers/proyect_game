﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL = Biblioteca.DAL;
using System.Data.Entity;


namespace LibreriaClases.BLL
{
    public class ClienteRepositorio
    {
           public Cliente Create(Cliente cliente)
        {
            try
            {
                DAL.Clientes clientesDAL = new DAL.Clientes();

                clientesDAL.rut_cliente = cliente.RutCliente;
                 clientesDAL.Nombre_cliente = cliente.NombreCliente;
                 clientesDAL.Apellido_cliente = cliente.ApellidoP;
                
                CommonBC.ModeloEntities.AddToClientes(clientesDAL);
                CommonBC.ModeloEntities.SaveChanges();

                return cliente;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public Cliente Read(string id)
        {
            try
            {
                DAL.Clientes clienteDAL = CommonBC.ModeloEntities.Clientes.FirstOrDefault(a => a.rut_cliente == id);
                Cliente cliente = new Cliente();

               cliente.RutCliente = clienteDAL.rut_cliente;
                cliente.NombreCliente = clienteDAL.Nombre_cliente;
                cliente.ApellidoP = clienteDAL.Apellido_cliente;

                return cliente;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public Cliente Update(Cliente cliente)
        {
            try
            {
               DAL.Clientes clientesDAL = CommonBC.ModeloEntities.Clientes.FirstOrDefault(a => a.rut_cliente == cliente.RutCliente);

                 clientesDAL.rut_cliente = cliente.RutCliente;
                 clientesDAL.Nombre_cliente = cliente.NombreCliente;
                 clientesDAL.Apellido_cliente = cliente.ApellidoP;

                CommonBC.ModeloEntities.SaveChanges();

                return cliente;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public bool Delete(string id)
        {
            try
            {
                  DAL.Clientes clienteDAL = CommonBC.ModeloEntities.Clientes.FirstOrDefault(a => a.rut_cliente == id);

                CommonBC.ModeloEntities.DeleteObject(clienteDAL);
                CommonBC.ModeloEntities.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public List<DAL.Clientes> ReadAllDAL()
        {
            return CommonBC.ModeloEntities.Clientes.ToList();
        }

    }
}
    

