﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL = Biblioteca.DAL;

namespace LibreriaClases.BLL
{
    public class SucursalCollection
    {
        private List<Sucursal> GenerarListado(List<DAL.Sucursal> sucursalesDAL)
        {
            List<Sucursal> sucursales = new List<Sucursal>();

            foreach (var sucursalDAL in sucursalesDAL)
            {
                Sucursal sucursal = new Sucursal();

                sucursal.IdSucursal = sucursalDAL.P_id_suc;
                sucursal.NombreSucursal = (TipoSucursal)Enum.Parse(typeof(TipoSucursal), sucursalDAL.TipoSucursal, true);

                sucursales.Add(sucursal);
            }
            return sucursales;
        }

        public List<Sucursal> ReadAll()
        {
            SucursalRepositorio repo = new SucursalRepositorio();

            return GenerarListado(repo.ReadAllDAL());
        }
    }
}
