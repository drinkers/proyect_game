﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL = Biblioteca.DAL;
using System.Data.Entity;

namespace LibreriaClases.BLL
{
    public class VendedorRepositorio
    {
        public Vendedor Create(Vendedor vendedor)
        {
            try
            {
                DAL.Vendedor vendedorDAL = new DAL.Vendedor();

                vendedorDAL.rut_vendedor = vendedor.IdVendedor.ToString();
                vendedorDAL.Nombre_vendedor = vendedor.NombreVend;
                vendedorDAL.Apellido_vendedor = vendedor.ApellidoP;
                vendedorDAL.pass_vendedor = vendedor.Clave;
                vendedorDAL.pfl_vendedor = 2;

                CommonBC.ModeloEntities.AddToVendedor(vendedorDAL);
                CommonBC.ModeloEntities.SaveChanges();

                return vendedor;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public Vendedor Read(int id)
        {
            try
            {
                DAL.Vendedor vendedorDAL = CommonBC.ModeloEntities.Vendedor.FirstOrDefault(a => int.Parse(a.rut_vendedor) == id);
                Vendedor vendedor = new Vendedor();

                vendedor.IdVendedor = int.Parse(vendedorDAL.rut_vendedor);
                vendedor.NombreVend = vendedorDAL.Nombre_vendedor;
                vendedor.ApellidoP = vendedorDAL.Apellido_vendedor;
                vendedor.Clave = vendedorDAL.pass_vendedor;

                return vendedor;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public Vendedor Update(Vendedor vendedor)
        {
            try
            {
                DAL.Vendedor vendedorDAL = CommonBC.ModeloEntities.Vendedor.FirstOrDefault(a => a.rut_vendedor == vendedor.IdVendedor.ToString());

                vendedorDAL.rut_vendedor = vendedor.IdVendedor.ToString();
                vendedorDAL.Nombre_vendedor = vendedor.NombreVend;
                vendedorDAL.Apellido_vendedor = vendedor.ApellidoP;
                vendedorDAL.pass_vendedor = vendedor.Clave;
                vendedorDAL.pfl_vendedor = 2;

                CommonBC.ModeloEntities.SaveChanges();

                return vendedor;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                DAL.Vendedor vendedorDAL = CommonBC.ModeloEntities.Vendedor.FirstOrDefault(a => int.Parse(a.rut_vendedor) == id);

                CommonBC.ModeloEntities.DeleteObject(vendedorDAL);
                CommonBC.ModeloEntities.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public List<DAL.Vendedor> ReadAllDAL()
        {
            return CommonBC.ModeloEntities.Vendedor.ToList();
        }
    }
}
