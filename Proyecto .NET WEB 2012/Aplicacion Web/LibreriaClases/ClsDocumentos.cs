﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using dim.rutinas;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;

namespace dim.datos
{
    public class ClsDocumentos
    {
        SqlQuery sqlquery = new SqlQuery();
        RutinasGenerales rg = new RutinasGenerales();

        public int codigo { get; set; }
        public string descripcion { get; set; }
        public string MENSAJE { get; set; }
        public int num_nomina { get; set; }


        public DataSet Devuelve_Planes(long rutPVR, long rutGPG)
        {
            DataSet ds;
            String sqlstr;

            sqlstr = "SP_BUSCAR_PLANES " + rutPVR + ", " + rutGPG;

            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;
        }

        // 31/03/2014 DEVUELVE MONTO DIPONIBLE entre Cliente - Proveedor
        public double Estado_Linea(string RUT_PVR, string RUT_GPG, int TIP_MON)
        {
            DataSet ds;
            String sqlstr = "";

            sqlstr = " EXEC SP_DEV_MTO_DIS_LIN_FIN_X_UNO '" + RUT_PVR + "','" + RUT_GPG + "'," + TIP_MON;

            ds = sqlquery.ExecuteDataSet(sqlstr);

            return double.Parse(ds.Tables[0].Rows[0]["MONTO_DIPONIBLE"].ToString());

        }
       
        //elimina los documentos de la tabla temp_doc_eliminar, que esten asociados al usuario conectado
        public void BorraDocumentosParaEliminar(string usr)
        {
            String sqlstr = "";

            sqlstr = " DELETE TEMP_DOC_ELIMINAR WHERE usuario = '" + usr + "'";
            sqlstr += " AND  doc_est_int IN (10,1,4) ";

            sqlquery.ExecuteNonQuery(sqlstr);


        }


        public DataSet Dev_Cliente_Temp_Doc_Eliminar(string usr)
        {
            DataSet ds;
            string sqlstr = "";

            sqlstr = " SELECT GPG_RUT FROM TEMP_DOC_ELIMINAR WHERE USUARIO = '" + usr + "' GROUP BY GPG_RUT";
            sqlquery.ExecuteNonQuery(sqlstr);

            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;

        }


        // VALIDA QUE EL DOCUMENTO QUEDE DENTRO DEL PLAN SEGUN PLAZO DE FACT. DEL PROVEEDOR 
        // PUEDE SER UNA CANTIDAD DEFINIDA (SIN PLAZO VARIABLE) O UN RANGO DE DIAS(PLAZO VARIABLE) 
        public bool ValidaDiasDoc_VS_DiasPlan(int id_plan, int dias)
        {

            int result = 0;
            string sqlstr = "";
            DataSet ds;

            sqlstr = "SP_MA_VALIDA_DIAS_PLAN " + id_plan + ", " + dias;
            ds = sqlquery.ExecuteDataSet(sqlstr);

            if (ds.Tables[0].Rows.Count > 0)
            {
                result = int.Parse(ds.Tables[0].Rows[0]["VALIDA"].ToString());

                if (result == 1)
                    return true;
                else
                    return false;
            }
            return false;
        }


        public string Dev_plazo_plan(int id_plan)
        {
            DataSet ds;
            string sqlstr = "";
            string plazo_min, plazo_pvr;

            sqlstr = " SELECT PLN_DDF_PLAZO_VAR AS PLAZO,PLN_DDF_PLZ_FAC_PVR AS DIAS_PVR,";
            sqlstr += " PLN_DDF_PLAZO_MIN_FACT DIAS_DESDE FROM PLN_AUX WHERE ID_PLAN = " + id_plan;
            ds = sqlquery.ExecuteDataSet(sqlstr);

            if (ds.Tables[0].Rows.Count > 0)
            {
                if (int.Parse(ds.Tables[0].Rows[0]["PLAZO"].ToString()) == 2)
                {
                    plazo_pvr = ds.Tables[0].Rows[0]["DIAS_PVR"].ToString();
                    return plazo_pvr;
                }
                else
                {
                    plazo_min = ds.Tables[0].Rows[0]["DIAS_DESDE"].ToString();
                    plazo_pvr = ds.Tables[0].Rows[0]["DIAS_PVR"].ToString();
                    return plazo_min + " - " + plazo_pvr;
                }
            }
            return "";
        }




    }
//*************************************************************************************************

    public static class ProductsSelectionManager
    {

        public static void KeepSelection(GridView grid)
        {
            //
            // se obtienen los id de producto checkeados de la pagina actual
            //
            List<int> checkedProd = (from item in grid.Rows.Cast<GridViewRow>()
                                     let check = (CheckBox)item.FindControl("CKB_RUT_PROVEEDOR")
                                     where check.Checked
                                     select Convert.ToInt32(grid.DataKeys[item.RowIndex].Value)).ToList();

            //
            // se recupera de session la lista de seleccionados previamente
            //
            List<int> productsIdSel = HttpContext.Current.Session["ProdSelection"] as List<int>;

            if (productsIdSel == null)
                productsIdSel = new List<int>();

            //
            // se cruzan todos los registros de la pagina actual del gridview con la lista de seleccionados,
            // si algun item de esa pagina fue marcado previamente no se devuelve
            //
            productsIdSel = (from item in productsIdSel
                             join item2 in grid.Rows.Cast<GridViewRow>()
                                on item equals Convert.ToInt32(grid.DataKeys[item2.RowIndex].Value) into g
                             where !g.Any()
                             select item).ToList();

            //
            // se agregan los seleccionados
            //
            productsIdSel.AddRange(checkedProd);

            HttpContext.Current.Session["ProdSelection"] = productsIdSel;

        }

        public static void RestoreSelection(GridView grid)
        {

            List<int> productsIdSel = HttpContext.Current.Session["ProdSelection"] as List<int>;

            if (productsIdSel == null)
                return;

            //
            // se comparan los registros de la pagina del grid con los recuperados de la Session
            // los coincidentes se devuelven para ser seleccionados
            //
            List<GridViewRow> result = (from item in grid.Rows.Cast<GridViewRow>()
                                        join item2 in productsIdSel
                                        on Convert.ToInt32(grid.DataKeys[item.RowIndex].Value) equals item2 into g
                                        where g.Any()
                                        select item).ToList();

            //
            // se recorre cada item para marcarlo
            //
            result.ForEach(x => ((CheckBox)x.FindControl("CKB_RUT_PROVEEDOR")).Checked = true);

        }


    }

     




 }
