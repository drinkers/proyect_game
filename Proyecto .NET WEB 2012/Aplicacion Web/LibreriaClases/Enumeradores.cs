﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LibreriaClases.BLL
{
    public enum TipoGenero
    {
        Seleccione, Aventura, Accion, FPS, RPG, Terror, Estrategia, Supervivencia, Deporte, Carreras, Plataformas
    }

    public enum TipoSucursal
    {
        Seleccione, Santiago, Maipu, Cerrillos, Pudahuel
    }
}
