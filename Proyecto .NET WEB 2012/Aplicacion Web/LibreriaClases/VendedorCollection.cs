﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL = Biblioteca.DAL;

namespace LibreriaClases.BLL
{
    public class VendedorCollection
    {
        private List<Vendedor> GenerarListado(List<DAL.Vendedor> vendedoresDAL)
        {
            List<Vendedor> vendedores = new List<Vendedor>();

            foreach (var vendedorDAL in vendedoresDAL)
            {
                Vendedor vendedor = new Vendedor();

                vendedor.IdVendedor = int.Parse(vendedorDAL.rut_vendedor);
                vendedor.NombreVend = vendedorDAL.Nombre_vendedor;
                vendedor.ApellidoP = vendedorDAL.Apellido_vendedor;
                vendedor.Clave = vendedorDAL.pass_vendedor;

                vendedores.Add(vendedor);
            }
            return vendedores;
        }

        public List<Vendedor> ReadAll()
        {
            VendedorRepositorio repo = new VendedorRepositorio();

            return GenerarListado(repo.ReadAllDAL());
        }
    }
}
