﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL = Biblioteca.DAL;
using System.Data.Entity;

namespace LibreriaClases.BLL
{
    public class ProductoRepositorio
    {
        public Producto Create(Producto producto)
        {
            try
            {
                DAL.Producto productoDAL = new DAL.Producto();

                productoDAL.P_id_pro = producto.IdProducto;
                productoDAL.Nombre_Producto = producto.NombreProducto;
                productoDAL.Tipo_producto = producto.CategoriaProducto.ToString();
                productoDAL.Valor_producto = producto.ValorProducto;
                productoDAL.Fecha_ingreso = producto.FechaIngreso;

                CommonBC.ModeloEntities.AddToProducto(productoDAL);
                CommonBC.ModeloEntities.SaveChanges();

                return producto;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public Producto Read(int id)
        {
            try
            {
                DAL.Producto productoDAL = CommonBC.ModeloEntities.Producto.FirstOrDefault(a => a.P_id_pro == id);
                Producto producto = new Producto();

                producto.IdProducto = productoDAL.P_id_pro;
                producto.NombreProducto = productoDAL.Nombre_Producto;
                producto.CategoriaProducto = (TipoGenero)Enum.Parse(typeof(TipoGenero), productoDAL.Tipo_producto);
                producto.ValorProducto = (int)productoDAL.Valor_producto;
                producto.FechaIngreso = (DateTime)productoDAL.Fecha_ingreso;

                return producto;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public Producto Update(Producto producto)
        {
            try
            {
                DAL.Producto productoDAL = CommonBC.ModeloEntities.Producto.FirstOrDefault(a => a.P_id_pro == producto.IdProducto);

                productoDAL.P_id_pro = producto.IdProducto;
                productoDAL.Nombre_Producto = producto.NombreProducto;
                productoDAL.Tipo_producto = producto.CategoriaProducto.ToString();
                productoDAL.Valor_producto = producto.ValorProducto;
                productoDAL.Fecha_ingreso = producto.FechaIngreso;

                CommonBC.ModeloEntities.SaveChanges();

                return producto;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                DAL.Producto productoDAL = CommonBC.ModeloEntities.Producto.FirstOrDefault(a => a.P_id_pro == id);

                CommonBC.ModeloEntities.DeleteObject(productoDAL);
                CommonBC.ModeloEntities.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public List<DAL.Producto> ReadAllDAL()
        {
            return CommonBC.ModeloEntities.Producto.ToList();
        }

    }
}
