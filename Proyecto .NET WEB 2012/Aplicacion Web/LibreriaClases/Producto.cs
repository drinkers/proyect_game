﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LibreriaClases.BLL
{
    public class Producto
    {
        public int IdProducto { get; set; }
        public string NombreProducto { get; set; }
        public TipoGenero CategoriaProducto { get; set; }
        public TipoProducto TipoProducto { get; set; }
        public int ValorProducto { get; set; }
        public DateTime FechaIngreso { get; set; }

        public Producto()
        {
            this.Init();
        }

        private void Init()
        {
            this.IdProducto = 0;
            this.NombreProducto = string.Empty;
            this.CategoriaProducto = TipoGenero.Seleccione;
            this.ValorProducto = 0;
            this.FechaIngreso = DateTime.Now;
            TipoProducto = new TipoProducto();

        }

    }
}
