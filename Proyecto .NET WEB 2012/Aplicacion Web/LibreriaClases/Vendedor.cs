﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LibreriaClases.BLL
{
    public class Vendedor
    {
        public int IdVendedor { get; set; }
        public string NombreVend { get; set; }
        public string ApellidoP { get; set; }
        public string ApellidoM { get; set; }
        public string Clave { get; set; }

        public Vendedor()        
        {
            this.Init();
        }

        private void Init()
        {
            this.IdVendedor = 0;
            this.NombreVend = string.Empty;
            this.ApellidoP = string.Empty;
            this.ApellidoM = string.Empty;
            this.Clave = string.Empty;
        }





    }
}
