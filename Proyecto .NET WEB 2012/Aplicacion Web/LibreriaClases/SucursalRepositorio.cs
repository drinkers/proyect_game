﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL = Biblioteca.DAL;
using System.Data.Entity;

namespace LibreriaClases.BLL
{
    public class SucursalRepositorio
    {
        public Sucursal Create(Sucursal sucursal)
        {
            try
            {
                DAL.Sucursal sucursalDAL = new DAL.Sucursal();

                sucursalDAL.P_id_suc = sucursal.IdSucursal;
                sucursalDAL.TipoSucursal = sucursal.NombreSucursal.ToString();

                CommonBC.ModeloEntities.AddToSucursal(sucursalDAL);
                CommonBC.ModeloEntities.SaveChanges();

                return sucursal;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public Sucursal Read(int id)
        {
            try
            {
                DAL.Sucursal sucursalDAL = CommonBC.ModeloEntities.Sucursal.FirstOrDefault(a => a.P_id_suc == id);
                Sucursal sucursal = new Sucursal();

                sucursal.IdSucursal = sucursalDAL.P_id_suc;
                sucursal.NombreSucursal = (TipoSucursal)Enum.Parse(typeof(TipoSucursal), sucursalDAL.TipoSucursal, true);

                CommonBC.ModeloEntities.AddToSucursal(sucursalDAL);
                CommonBC.ModeloEntities.SaveChanges();

                return sucursal;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public Sucursal Update(Sucursal sucursal)
        {
            try
            {
                DAL.Sucursal sucursalDAL = CommonBC.ModeloEntities.Sucursal.FirstOrDefault(a => a.P_id_suc == sucursal.IdSucursal);

                sucursalDAL.P_id_suc = sucursal.IdSucursal;
                sucursalDAL.TipoSucursal = sucursal.NombreSucursal.ToString();

                CommonBC.ModeloEntities.SaveChanges();

                return sucursal;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                DAL.Sucursal sucursalDAL = CommonBC.ModeloEntities.Sucursal.FirstOrDefault(a => a.P_id_suc == id);

                CommonBC.ModeloEntities.DeleteObject(sucursalDAL);
                CommonBC.ModeloEntities.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public List<DAL.Sucursal> ReadAllDAL()
        {
            return CommonBC.ModeloEntities.Sucursal.ToList();
        }
    }
}
