﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL = Biblioteca.DAL;

namespace LibreriaClases.BLL
{
    public class ProductoCollection
    {
        private List<Producto> GenerarListado(List<DAL.Producto> productosDAL)
        {
            List<Producto> productos = new List<Producto>();

            foreach (var productoDAL in productosDAL)
            {
                Producto producto = new Producto();

                producto.IdProducto = productoDAL.P_id_pro;
                producto.NombreProducto = productoDAL.Nombre_Producto;
                producto.CategoriaProducto = (TipoGenero)Enum.Parse(typeof(TipoGenero), productoDAL.Tipo_producto);
                producto.ValorProducto = (int)productoDAL.Valor_producto;
                producto.FechaIngreso = (DateTime)productoDAL.Fecha_ingreso;

                productos.Add(producto);
            }
            return productos;
        }

        public List<Producto> ReadAll()
        {
            ProductoRepositorio repo = new ProductoRepositorio();

            return GenerarListado(repo.ReadAllDAL());
        }


    }
}
