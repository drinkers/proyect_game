﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL = Biblioteca.DAL;

namespace LibreriaClases.BLL
{
    public class Administrador
    {
        public int IdAdministrador { get; set; }
        public Sucursal Sucursal { get; set; }
        public string NombreAdmin { get; set; }
        public string RutAdmin { get; set; }
        public string ApellidoAdmin { get; set; }
        public string ClaveIngreso { get; set; }
        public string Email { get; set; }
        public int Pfl { get; set; }

        public Administrador()
        {
            Init();
        }

        private void Init()
        {
            this.IdAdministrador = 0;
            this.Sucursal = new Sucursal();
            this.NombreAdmin = string.Empty;
            this.RutAdmin = string.Empty;
            this.ApellidoAdmin = string.Empty;
            this.ClaveIngreso = string.Empty;
            this.Email = string.Empty;
            this.Pfl = 0;
            
        }


        public Administrador Create(Administrador BiblioAdm)
        {
            try
            {


                DAL.Administrador biblioDAL = new DAL.Administrador();

                biblioDAL.Nombre_adm = BiblioAdm.NombreAdmin;
                biblioDAL.rut_adm = BiblioAdm.RutAdmin;
                biblioDAL.P_id_suc = BiblioAdm.Sucursal.IdSucursal;
                biblioDAL.Apellido_adm = BiblioAdm.ApellidoAdmin;
                biblioDAL.email = BiblioAdm.Email;
                biblioDAL.pass = BiblioAdm.ClaveIngreso;
                biblioDAL.pfl_adm = BiblioAdm.Pfl;

                CommonBC.ModeloEntities.AddToAdministrador(biblioDAL);
                CommonBC.ModeloEntities.SaveChanges();

                return BiblioAdm;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }


        public Administrador ValidaAdmin(string rut)
        {
            try
            {
                DAL.Administrador biblioDAL = CommonBC.ModeloEntities.Administrador.FirstOrDefault
                                          (a => a.rut_adm == rut);


                Administrador biblioBO = new Administrador();

                if (biblioDAL != null)
                {
                    biblioBO.RutAdmin = biblioDAL.rut_adm;
                    biblioBO.Sucursal.IdSucursal = int.Parse(biblioDAL.P_id_suc.ToString());
                }
                else
                {
                    biblioBO.RutAdmin = string.Empty;

                }

                return biblioBO;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }


        public Administrador ValidaIngresoAdmin(string rut, string pass)
        {
            try
            {
                DAL.Administrador biblioDAL = CommonBC.ModeloEntities.Administrador.FirstOrDefault
                                          (a => a.rut_adm == rut && a.pass == pass);


                Administrador biblioBO = new Administrador();

                if (biblioDAL != null)
                {
                    biblioBO.RutAdmin = biblioDAL.rut_adm;
                    biblioBO.ClaveIngreso =biblioDAL.pass;
                }
                else
                {
                    biblioBO.RutAdmin = string.Empty;

                }

                return biblioBO;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }



        public Administrador BuscarPerfil(string rut)
        {
            try
            {
                DAL.Administrador biblioDAL = CommonBC.ModeloEntities.Administrador.FirstOrDefault
                                          (a => a.rut_adm == rut);


                Administrador biblioBO = new Administrador();

                if (biblioDAL != null)
                {
                    biblioBO.Pfl = int.Parse(biblioDAL.pfl_adm.ToString());
                }
                else
                {
                    biblioBO.Pfl = 3;

                }

                return biblioBO;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }



    }  
}
