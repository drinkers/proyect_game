﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LibreriaClases.BLL
{
    public class Cliente
    {
        public string RutCliente { get; set; }
        public string NombreCliente { get; set; }
        public string ApellidoP { get; set; }

        public Cliente()
        {
            Init();
        }

        private void Init()
        {
            this.RutCliente = string.Empty;
            this.NombreCliente = string.Empty;
            this.ApellidoP = string.Empty;
        }



    }
}
