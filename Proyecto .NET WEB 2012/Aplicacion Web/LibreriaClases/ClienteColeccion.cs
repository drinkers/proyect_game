﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL = Biblioteca.DAL;

namespace LibreriaClases.BLL
{
        public class ClienteColeccion
        {
            private List<Cliente> GenerarListado(List<DAL.Clientes> clientesDAL)
            {
                List<Cliente> clientes = new List<Cliente>();

                foreach (var clienteDAL in clientesDAL)
                {
                    Cliente cliente = new Cliente();

                    cliente.RutCliente = clienteDAL.rut_cliente;
                    cliente.NombreCliente = clienteDAL.Nombre_cliente;
                    cliente.ApellidoP = clienteDAL.Apellido_cliente;

                    clientes.Add(cliente);
                }
                return clientes;
            }

            public List<Cliente> ReadAll()
            {
                ClienteRepositorio repo = new ClienteRepositorio();

                return GenerarListado(repo.ReadAllDAL());
            }
        }
    
}
