﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LibreriaClases.BLL
{
    public class Venta
    {
        public DateTime FechaVenta { get; set; }
        public Producto Producto { get; set; }
        public Vendedor Vendedor { get; set; }
        public Cliente Cliente { get; set; }
        public Sucursal Sucursal { get; set; }

        public Venta()
        {
            this.Init();
        }

        private void Init()
        {
            this.FechaVenta = DateTime.Now;
            this.Producto = new Producto();
            this.Vendedor = new Vendedor();
            this.Cliente = new Cliente();
            this.Sucursal = new Sucursal();

        }



    }
}
