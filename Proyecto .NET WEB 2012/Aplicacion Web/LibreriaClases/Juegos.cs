﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LibreriaClases.BLL
{
    public class Juegos
    {
        public string NombreJuego { get; set; }
        public string CodigoJuego { get; set; }
        public int TipoJuego { get; set; }
        public double PrecioJuego { get; set; }


        public Juegos()
        {
            this.init();
    
        }

        private void init()
        {
            NombreJuego = string.Empty;
            CodigoJuego = string.Empty;
            TipoJuego = 0;
            PrecioJuego = 0;
        }

    }
}

