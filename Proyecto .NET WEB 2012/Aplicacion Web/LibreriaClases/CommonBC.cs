﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Biblioteca.DAL;


namespace LibreriaClases.BLL
{
   public class CommonBC
    {
       private static expert_gameEntities _expert_gameEntities;

       public static expert_gameEntities ModeloEntities
       {
           get {

               if (_expert_gameEntities == null)
               {
                   _expert_gameEntities = new expert_gameEntities();
               
               }

               return _expert_gameEntities;
           }
       
       }
    }
}
