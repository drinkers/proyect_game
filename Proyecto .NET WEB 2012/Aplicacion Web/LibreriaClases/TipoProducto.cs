﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LibreriaClases.BLL
{
    public class TipoProducto
    {
        public int id_tipoProducto { get; set; }
        public string categoria { get; set; }
        public bool estado { get; set; }

        public TipoProducto()
        {
            this.Init();
        }

        private void Init()
        {
            this.id_tipoProducto = 0;
            this.categoria = string.Empty;
            this.estado = false;
        }



    }
}
