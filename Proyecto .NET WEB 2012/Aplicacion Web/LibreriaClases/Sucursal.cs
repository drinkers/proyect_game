﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LibreriaClases.BLL
{
    public class Sucursal
    {
        public int IdSucursal { get; set; }
        public TipoSucursal NombreSucursal { get; set; }

        public Sucursal()
        {
            Init();
        }

        private void Init()
        {
            this.IdSucursal = 0;
            this.NombreSucursal = TipoSucursal.Seleccione;
        }

    }
}

