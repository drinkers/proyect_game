﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using dim.rutinas;
using System.Data;
using System.Data.SqlClient;


namespace dim.datos
{
    public class ClsOperacion:ClsOperacionCls
    {
        SqlQuery sqlquery = new SqlQuery();
        RutinasGenerales rg = new RutinasGenerales();
        
        #region Funciones Operaciones

        public DataSet DevuelveOperacionesGPG(long rutproveedor, int rutpagador,
                                           int ano, int mes)
        {
            DataSet ds;
            String sqlstr = "";

            sqlstr = "EXEC SP_WEB_LIQUIDACION_OPERACION_GPG " + rutpagador + ", " + rutproveedor + ", " +
                                                                    ano + ",  " + mes;
                                                                        

            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;

        }

		public DataSet DevuelveOperacionesGE(string rutpro1, string rutpro2, string num_ope1, string num_ope2,
                                            DateTime fecha_desde, DateTime fecha_hasta, string EST_OPE_SIM, string EST_OPE_GER,
                                            string EST_OPE_AUT, string EST_REC_GER, string EST_REC_OPE, string EST_OPE_OTO,
                                            string EST_OPE_ANU, string TIPO_CONSUL, string OPC_ORI_SIS, string OPC_ORI_INT,
                                            string TIP_MON1, string TIP_MON2, DateTime FEC_OTO1, DateTime FEC_OTO2, string usr)
        {
            string sql_str = "";
            DataSet ds;

            sql_str = "EXEC SP_GE_DEVUELVE_OPERACIONES " + rutpro1 + "," +
                      rutpro2 + "," + num_ope1 + "," + num_ope2 + "," +
                      "'" + rg.FechaJuliana(fecha_desde) + "','" + rg.FechaJuliana(fecha_hasta) + "'," +
                      EST_OPE_SIM + "," + EST_OPE_GER + "," + EST_OPE_AUT + "," +
                      EST_REC_GER + "," + EST_REC_OPE + "," + EST_OPE_OTO + "," +
                      EST_OPE_ANU + "," + TIPO_CONSUL + ",'" + OPC_ORI_SIS + "','" +
                      OPC_ORI_INT + "'," + TIP_MON1 + ","  + TIP_MON2 + ",'" + 
                      FEC_OTO1 + "','" + FEC_OTO2 + "','"+usr+"'" ;


            ds = sqlquery.ExecuteDataSet(sql_str);

            return ds;                              

        }

        public DataSet DevuelveOperacionesAyudaProveedor(long rutproveedor, DateTime fechadesde, DateTime fechahasta,
                                                         int estado1, int estado2, int estado3, int estado4)
        {
            DataSet ds;
            String sqlstr = "";

            sqlstr = "EXEC SP_CO_DEVUELVE_OPERACION " + rutproveedor + ", '" +
                                                        rg.FechaJuliana(fechadesde) + "', '" +
                                                        rg.FechaJuliana(fechahasta) + "', " +
                                                        estado1 + ", " +
                                                        estado2 + ", " +
                                                        estado3 + ", " +
                                                        estado4;
                                                        


            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;

        }
        
        public DataSet DevuelveOperacion(int nrooperacion)
        {
            DataSet ds;
            String sqlstr = "";

            sqlstr = "EXEC SP_CO_DEVUELVE_OPERACION_POR_NRO " + nrooperacion;

            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;

        }

        public DataSet DevuelveOperacionTodosProveedor(string opcion, DateTime FechaIni, DateTime FechaFin, string usr)
        {
            //se crea para trae todos las operaciones para la pizarra de VB_Confirming 
            DataSet ds;
            String sqlstr = "";

            sqlstr = "EXEC Sp_Devuelve_Operaciones_Todo_Proveedor '" + opcion+"','" + rg.FechaJuliana(FechaIni) +"','"+ rg.FechaJuliana(FechaFin) +"','"+usr+"'";

            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;
        }

        public ClsOperacionCls DevuelveOperacionXNum_Ope(int Num_Ope)
        {
            ClsOperacionCls operacion = new ClsOperacionCls();
            DataSet ds = new DataSet();
            String sqlstr = "";
            try
            {
                sqlstr = "Exec Sp_Retorna_Tabla_Operacion " + Num_Ope;

                ds = sqlquery.ExecuteDataSet(sqlstr);
            }
            catch (Exception e)
            {
                ds = null;
            }


            if (ds.Tables.Count > 0 & ds.Tables[0].Rows.Count > 0)
            {
                #region oculto
                //operacion.Num_Ope = ds.Tables[0].Rows[0]["opc_num_cfg"] == null ? 0 : int.Parse(ds.Tables[0].Rows[0]["opc_num_cfg"].ToString());
                //operacion.Num_Oto_Conf = int.Parse(ds.Tables[0].Rows[0]["opc_otg_cfg"].ToString());
                //operacion.Num_oto_Fact = int.Parse(ds.Tables[0].Rows[0]["opc_otg_ftg"].ToString());
                //operacion.Cos_Resp = int.Parse(ds.Tables[0].Rows[0]["opc_cos_res"].ToString());
                //operacion.Tip_Doc = int.Parse(ds.Tables[0].Rows[0]["par_tip_doc"].ToString());
                //operacion.Cant_Doc = int.Parse(ds.Tables[0].Rows[0]["opc_can_doc"].ToString());
                //operacion.Tip_Mon = int.Parse(ds.Tables[0].Rows[0]["par_mon"].ToString());
                //operacion.Mto_Doc = double.Parse(ds.Tables[0].Rows[0]["opc_mto_doc"].ToString());
                //operacion.Est_Ope = int.Parse(ds.Tables[0].Rows[0]["par_est"].ToString());
                //operacion.Fec_Simu = ds.Tables[0].Rows[0]["opc_fec_sim"].ToString();
                //operacion.Tasa_Base = double.Parse(ds.Tables[0].Rows[0]["opc_tas_bas"].ToString());
                //operacion.Spread = double.Parse(ds.Tables[0].Rows[0]["opc_spr_ead"].ToString());
                //operacion.Tasa_Max_Conv = double.Parse(ds.Tables[0].Rows[0]["opc_tmc_dsi"].ToString());
                //operacion.Apro_REC = (ds.Tables[0].Rows[0]["opc_apb_rec"] == null ? 0 : int.Parse(ds.Tables[0].Rows[0]["opc_apb_rec"].ToString()));
                //operacion.Fec_Apro_REC = ds.Tables[0].Rows[0]["opc_fec_apb_rec"].ToString();
                //operacion.Obs_Apro_REC = (ds.Tables[0].Rows[0]["opc_obs_apb_rec"].ToString() == null ? "" : ds.Tables[0].Rows[0]["opc_obs_apb_rec"].ToString());
                //operacion.Fec_Oto = ds.Tables[0].Rows[0]["opc_fec_oto"].ToString();
                //operacion.Fec_Vec_Oto = ds.Tables[0].Rows[0]["opc_fev"].ToString();
                //operacion.Tip_Mon_Comis = int.Parse(ds.Tables[0].Rows[0]["par_mon_com"].ToString());
                //operacion.Porcj_Comis = Double.Parse(ds.Tables[0].Rows[0]["opc_por_com"].ToString());
                //operacion.Com_Min = double.Parse(ds.Tables[0].Rows[0]["opc_com_min"].ToString());
                //operacion.Com_Max = double.Parse(ds.Tables[0].Rows[0]["opc_com_max"].ToString());
                //operacion.Com_Tot_Peso = int.Parse(ds.Tables[0].Rows[0]["opc_com_tot_pes"].ToString());
                //operacion.Iva_Comis = double.Parse(ds.Tables[0].Rows[0]["opc_iva_com"].ToString());
                //operacion.Fact_Camb = double.Parse(ds.Tables[0].Rows[0]["opc_fac_cam"].ToString());
                //operacion.Fec_Fac_Camb = ds.Tables[0].Rows[0]["opc_fec_fac_cam"].ToString();
                //operacion.Porcj_Antic = double.Parse(ds.Tables[0].Rows[0]["opc_por_ant"].ToString());
                //operacion.Mto_Antic = double.Parse(ds.Tables[0].Rows[0]["opc_mto_ant"].ToString());
                //operacion.Prec_Compra = double.Parse(ds.Tables[0].Rows[0]["opc_pre_com"].ToString());
                //operacion.Dif_Prec_inter = double.Parse(ds.Tables[0].Rows[0]["opc_dif_pre"].ToString());
                //operacion.Sald_Pend = double.Parse(ds.Tables[0].Rows[0]["opc_sal_pen"].ToString());
                //operacion.Sald_Pagad = double.Parse(ds.Tables[0].Rows[0]["opc_sal_pag"].ToString());
                //operacion.Mont_Gast = double.Parse(ds.Tables[0].Rows[0]["opc_mon_gas"].ToString());
                //operacion.Tot_Gir_Ope = double.Parse(ds.Tables[0].Rows[0]["opc_tot_gir"].ToString());
                //operacion.Oper_Puntual = ds.Tables[0].Rows[0]["opc_ptl"].ToString();
                //operacion.Cod_Ejecutv = int.Parse(ds.Tables[0].Rows[0]["eje_cod"].ToString());
                //operacion.Fec_Pag_Prov = ds.Tables[0].Rows[0]["opc_fec_pag_pvr"].ToString();
                //operacion.Fec_Ant_Prov = ds.Tables[0].Rows[0]["opc_fec_ant"].ToString();
                //operacion.Form_Pag = int.Parse(ds.Tables[0].Rows[0]["par_for_pag"].ToString());
                //operacion.Rev_Nomina = (ds.Tables[0].Rows[0]["opc_rev_nom"] == null ? "" : ds.Tables[0].Rows[0]["opc_rev_nom"].ToString());
                //operacion.Rev_Area_Credto = (ds.Tables[0].Rows[0]["opc_rev_cre"] == null ? "" : ds.Tables[0].Rows[0]["opc_rev_cre"].ToString());
                //operacion.Obs_Rev_Nomina = (ds.Tables[0].Rows[0]["opc_obs_rev_nom"] == null ? "" : ds.Tables[0].Rows[0]["opc_obs_rev_nom"].ToString());
                //operacion.Obs_Rev_Area_Credto = (ds.Tables[0].Rows[0]["opc_obs_rev_cre"] == null ? "" : ds.Tables[0].Rows[0]["opc_obs_rev_cre"].ToString());
                //operacion.Emis_Carta_Pago = (ds.Tables[0].Rows[0]["opc_emi"] == null ? "" : ds.Tables[0].Rows[0]["opc_emi"].ToString());
                //operacion.Tasa_Final = double.Parse(ds.Tables[0].Rows[0]["opc_tas_fin"].ToString());
                //operacion.Mto_Gasto_Fijo = double.Parse(ds.Tables[0].Rows[0]["opc_gas_fij"].ToString());
                //operacion.Mto_Gasto_Notario = double.Parse(ds.Tables[0].Rows[0]["opc_gas_not"].ToString());
                //operacion.Fec_Emis_Carta_Pago = ds.Tables[0].Rows[0]["opc_fec_emi"].ToString();
                //operacion.Mto_Iva_Gasto = double.Parse(ds.Tables[0].Rows[0]["opc_iva_gas"].ToString());
                //operacion.Dep_Vale_Vista = (ds.Tables[0].Rows[0]["opc_dep_vis"] == null ? 0 : int.Parse(ds.Tables[0].Rows[0]["opc_dep_vis"].ToString()));
                //operacion.Anul_Otorgamiento = (ds.Tables[0].Rows[0]["opc_anl_otg"] == null ? "" : ds.Tables[0].Rows[0]["opc_anl_otg"].ToString());
                //operacion.Cod_Banco_Depos_Proveedor = int.Parse(ds.Tables[0].Rows[0]["opc_cod_bco"].ToString());
                //operacion.Cta_Cte_Proveedor = ds.Tables[0].Rows[0]["opc_cta_cte"].ToString();
                //operacion.Tasa_Impuest_Oper = double.Parse(ds.Tables[0].Rows[0]["opc_tas_imp"].ToString());
                //operacion.Mto_Impuest_Oper = double.Parse(ds.Tables[0].Rows[0]["opc_tot_imp"].ToString());
                //operacion.Mto_Giro_Operacion = (ds.Tables[0].Rows[0]["opc_ope_gir2"] == null ? 0.0 : double.Parse(ds.Tables[0].Rows[0]["opc_ope_gir2"].ToString()));
                //operacion.Apli_Pizarr = (ds.Tables[0].Rows[0]["opc_piz_arr"] == null ? 0 : int.Parse(ds.Tables[0].Rows[0]["opc_piz_arr"].ToString()));
                //operacion.Num_Repertorio = (ds.Tables[0].Rows[0]["opc_nro_rep"] == null ? "" : ds.Tables[0].Rows[0]["opc_nro_rep"].ToString());
                //operacion.Fec_Envio_Repertorio = ds.Tables[0].Rows[0]["opc_fec_env_rep"].ToString();
                //operacion.Fec_Recep_Repertorio = ds.Tables[0].Rows[0]["opc_fec_rec_rep"].ToString();
                //operacion.Tasa_TIP = double.Parse(ds.Tables[0].Rows[0]["opc_tas_tip"].ToString());
                //operacion.Obs_Comercial = (ds.Tables[0].Rows[0]["opc_obs_com"] == null ? "" : ds.Tables[0].Rows[0]["opc_obs_com"].ToString());
                //operacion.Envio_Arch_Sodex = (ds.Tables[0].Rows[0]["opc_arc_env_son"] == null ? "" : ds.Tables[0].Rows[0]["opc_arc_env_son"].ToString());
                //operacion.Fact_Operacio = (ds.Tables[0].Rows[0]["opc_fct"] == null ? "" : ds.Tables[0].Rows[0]["opc_fct"].ToString());
                //operacion.Fact_Correlativo = (ds.Tables[0].Rows[0]["fct_crr"] == null ? 0 : int.Parse(ds.Tables[0].Rows[0]["fct_crr"].ToString()));
                //operacion.Oper_Giro_100 = ds.Tables[0].Rows[0]["opc_gir_tot_son"].ToString();
                //operacion.Num_Cta_CXC_100 = (ds.Tables[0].Rows[0]["opc_num_cta"] == null ? "" : ds.Tables[0].Rows[0]["opc_num_cta"].ToString());
                //operacion.Fec_Anul_Operacion = ds.Tables[0].Rows[0]["opc_fec_anl"].ToString();
                //operacion.Usuario_Anulacion = (ds.Tables[0].Rows[0]["opc_usu_anl"] == null ? "" : ds.Tables[0].Rows[0]["opc_usu_anl"].ToString());
                //operacion.Bloqueo_Operacion = (ds.Tables[0].Rows[0]["opc_blk_opc"] == null ? "" : ds.Tables[0].Rows[0]["opc_blk_opc"].ToString());
                //operacion.Obs_Aprob_Gerencia = (ds.Tables[0].Rows[0]["opc_obs_apr"] == null ? "" : ds.Tables[0].Rows[0]["opc_obs_apr"].ToString());
                //operacion.Obs_Recha_Gerencia = (ds.Tables[0].Rows[0]["opc_obs_rec"] == null ? "" : ds.Tables[0].Rows[0]["opc_obs_rec"].ToString());
                //operacion.Rev_Legal = ds.Tables[0].Rows[0]["opc_rev_leg"].ToString();
                //operacion.Usuario_Rev_Legal = (ds.Tables[0].Rows[0]["opc_usu_leg"] == null ? "" : ds.Tables[0].Rows[0]["opc_usu_leg"].ToString());
                //operacion.Origen_Operacion = ds.Tables[0].Rows[0]["opc_ori_opc"].ToString();
                //operacion.Pagador_Dev_Intereses = ds.Tables[0].Rows[0]["opc_dev_int_son"].ToString();
                //operacion.Usuario_Otorgado = ds.Tables[0].Rows[0]["opc_usu_oto"].ToString();
                //operacion.Hora_Otorgamiento = ds.Tables[0].Rows[0]["opc_hor_oto"].ToString();
                //operacion.Cierre = ds.Tables[0].Rows[0]["opc_cie_son"].ToString();
                //operacion.Fec_Cierre = ds.Tables[0].Rows[0]["opc_fec_cie"].ToString();
                //operacion.Eje_Cod_Pvr = (ds.Tables[0].Rows[0]["eje_cod_pvr"] == null ? 0 : int.Parse(ds.Tables[0].Rows[0]["eje_cod_pvr"].ToString()));
                //operacion.Tipo_Comision = int.Parse(ds.Tables[0].Rows[0]["opc_tip_com"].ToString());
                //operacion.Descrp_Forma_Pago = ds.Tables[0].Rows[0]["par_des"].ToString();
                #endregion

                operacion.LlenaClaseOperacion
                (
                int.Parse(ds.Tables[0].Rows[0]["opc_num_cfg"].ToString()),
                int.Parse(ds.Tables[0].Rows[0]["opc_otg_cfg"].ToString()),
                int.Parse(ds.Tables[0].Rows[0]["opc_otg_ftg"].ToString()),
                int.Parse(ds.Tables[0].Rows[0]["opc_cos_res"].ToString()),
                int.Parse(ds.Tables[0].Rows[0]["par_tip_doc"].ToString()),
                int.Parse(ds.Tables[0].Rows[0]["opc_can_doc"].ToString()),
                int.Parse(ds.Tables[0].Rows[0]["par_mon"].ToString()),
                double.Parse(ds.Tables[0].Rows[0]["opc_mto_doc"].ToString()),
                int.Parse(ds.Tables[0].Rows[0]["par_est"].ToString()),

                ds.Tables[0].Rows[0]["opc_fec_sim"].ToString(),
                    //10
                double.Parse(ds.Tables[0].Rows[0]["opc_tas_bas"].ToString()),
                double.Parse(ds.Tables[0].Rows[0]["opc_spr_ead"].ToString()),
                double.Parse(ds.Tables[0].Rows[0]["opc_tmc_dsi"].ToString()),
                (ds.Tables[0].Rows[0]["opc_apb_rec"] == null ? 0 : int.Parse(ds.Tables[0].Rows[0]["opc_apb_rec"].ToString())),

                ds.Tables[0].Rows[0]["opc_fec_apb_rec"].ToString(),
                (ds.Tables[0].Rows[0]["opc_obs_apb_rec"].ToString() == null ? "" : ds.Tables[0].Rows[0]["opc_obs_apb_rec"].ToString()),

                ds.Tables[0].Rows[0]["opc_fec_oto"].ToString(),

                ds.Tables[0].Rows[0]["opc_fev"].ToString(),
                int.Parse(ds.Tables[0].Rows[0]["par_mon_com"].ToString()),
                Double.Parse(ds.Tables[0].Rows[0]["opc_por_com"].ToString()),
                    //20
                double.Parse(ds.Tables[0].Rows[0]["opc_com_min"].ToString()),
                double.Parse(ds.Tables[0].Rows[0]["opc_com_max"].ToString()),
                int.Parse(ds.Tables[0].Rows[0]["opc_com_tot_pes"].ToString()),
                double.Parse(ds.Tables[0].Rows[0]["opc_iva_com"].ToString()),
                double.Parse(ds.Tables[0].Rows[0]["opc_fac_cam"].ToString()),
                ds.Tables[0].Rows[0]["opc_fec_fac_cam"].ToString(),
                double.Parse(ds.Tables[0].Rows[0]["opc_por_ant"].ToString()),
                double.Parse(ds.Tables[0].Rows[0]["opc_mto_ant"].ToString()),
                double.Parse(ds.Tables[0].Rows[0]["opc_pre_com"].ToString()),
                double.Parse(ds.Tables[0].Rows[0]["opc_dif_pre"].ToString()),
                    //30
                double.Parse(ds.Tables[0].Rows[0]["opc_sal_pen"].ToString()),
                double.Parse(ds.Tables[0].Rows[0]["opc_sal_pag"].ToString()),
                double.Parse(ds.Tables[0].Rows[0]["opc_mon_gas"].ToString()),
                double.Parse(ds.Tables[0].Rows[0]["opc_tot_gir"].ToString()),
                ds.Tables[0].Rows[0]["opc_ptl"].ToString(),
                int.Parse(ds.Tables[0].Rows[0]["eje_cod"].ToString()),

                ds.Tables[0].Rows[0]["opc_fec_pag_pvr"].ToString(),

                 ds.Tables[0].Rows[0]["opc_fec_ant"].ToString(),
                int.Parse(ds.Tables[0].Rows[0]["par_for_pag"].ToString()),
                (ds.Tables[0].Rows[0]["opc_rev_nom"] == null ? "" : ds.Tables[0].Rows[0]["opc_rev_nom"].ToString()),
                    //40
                (ds.Tables[0].Rows[0]["opc_rev_cre"] == null ? "" : ds.Tables[0].Rows[0]["opc_rev_cre"].ToString()),
                (ds.Tables[0].Rows[0]["opc_obs_rev_nom"] == null ? "" : ds.Tables[0].Rows[0]["opc_obs_rev_nom"].ToString()),
                (ds.Tables[0].Rows[0]["opc_obs_rev_cre"] == null ? "" : ds.Tables[0].Rows[0]["opc_obs_rev_cre"].ToString()),
                (ds.Tables[0].Rows[0]["opc_emi"] == null ? "" : ds.Tables[0].Rows[0]["opc_emi"].ToString()),
                double.Parse(ds.Tables[0].Rows[0]["opc_tas_fin"].ToString()),
                double.Parse(ds.Tables[0].Rows[0]["opc_gas_fij"].ToString()),
                double.Parse(ds.Tables[0].Rows[0]["opc_gas_not"].ToString()),

                ds.Tables[0].Rows[0]["opc_fec_emi"].ToString(),
                double.Parse(ds.Tables[0].Rows[0]["opc_iva_gas"].ToString()),
                (ds.Tables[0].Rows[0]["opc_dep_vis"] == null ? 0 : int.Parse(ds.Tables[0].Rows[0]["opc_dep_vis"].ToString())),
                    //50
                (ds.Tables[0].Rows[0]["opc_anl_otg"] == null ? "" : ds.Tables[0].Rows[0]["opc_anl_otg"].ToString()),
                 int.Parse(ds.Tables[0].Rows[0]["opc_cod_bco"].ToString()),
                 ds.Tables[0].Rows[0]["opc_cta_cte"].ToString(),
                 double.Parse(ds.Tables[0].Rows[0]["opc_tas_imp"].ToString()),
                 double.Parse(ds.Tables[0].Rows[0]["opc_tot_imp"].ToString()),
                 (ds.Tables[0].Rows[0]["opc_ope_gir2"] == null ? 0.0 : double.Parse(ds.Tables[0].Rows[0]["opc_ope_gir2"].ToString())),
                 (ds.Tables[0].Rows[0]["opc_piz_arr"] == null ? 0 : int.Parse(ds.Tables[0].Rows[0]["opc_piz_arr"].ToString())),
                 (ds.Tables[0].Rows[0]["opc_nro_rep"] == null ? "" : ds.Tables[0].Rows[0]["opc_nro_rep"].ToString()),

                 ds.Tables[0].Rows[0]["opc_fec_env_rep"].ToString(),

                  ds.Tables[0].Rows[0]["opc_fec_rec_rep"].ToString(),
                    //60
                 double.Parse(ds.Tables[0].Rows[0]["opc_tas_tip"].ToString()),
                 (ds.Tables[0].Rows[0]["opc_obs_com"] == null ? "" : ds.Tables[0].Rows[0]["opc_obs_com"].ToString()),
                 (ds.Tables[0].Rows[0]["opc_arc_env_son"] == null ? "" : ds.Tables[0].Rows[0]["opc_arc_env_son"].ToString()),
                 (ds.Tables[0].Rows[0]["opc_fct"] == null ? "" : ds.Tables[0].Rows[0]["opc_fct"].ToString()),
                 (ds.Tables[0].Rows[0]["fct_crr"] == null ? 0 : int.Parse(ds.Tables[0].Rows[0]["fct_crr"].ToString())),
                 ds.Tables[0].Rows[0]["opc_gir_tot_son"].ToString(),
                 (ds.Tables[0].Rows[0]["opc_num_cta"] == null ? "" : ds.Tables[0].Rows[0]["opc_num_cta"].ToString()),

                 ds.Tables[0].Rows[0]["opc_fec_anl"].ToString(),
                  (ds.Tables[0].Rows[0]["opc_usu_anl"] == null ? "" : ds.Tables[0].Rows[0]["opc_usu_anl"].ToString()),
                  (ds.Tables[0].Rows[0]["opc_blk_opc"] == null ? "" : ds.Tables[0].Rows[0]["opc_blk_opc"].ToString()),
                    //70
                  (ds.Tables[0].Rows[0]["opc_obs_apr"] == null ? "" : ds.Tables[0].Rows[0]["opc_obs_apr"].ToString()),
                  (ds.Tables[0].Rows[0]["opc_obs_rec"] == null ? "" : ds.Tables[0].Rows[0]["opc_obs_rec"].ToString()),
                  ds.Tables[0].Rows[0]["opc_rev_leg"].ToString(),
                  (ds.Tables[0].Rows[0]["opc_usu_leg"] == null ? "" : ds.Tables[0].Rows[0]["opc_usu_leg"].ToString()),
                  ds.Tables[0].Rows[0]["opc_ori_opc"].ToString(),
                  ds.Tables[0].Rows[0]["opc_dev_int_son"].ToString(),
                  ds.Tables[0].Rows[0]["opc_usu_oto"].ToString(),
                  ds.Tables[0].Rows[0]["opc_hor_oto"].ToString(),
                  ds.Tables[0].Rows[0]["opc_cie_son"].ToString(),

                  ds.Tables[0].Rows[0]["opc_fec_cie"].ToString(),
                    //80
                  (ds.Tables[0].Rows[0]["eje_cod_pvr"] == null ? 0 : int.Parse(ds.Tables[0].Rows[0]["eje_cod_pvr"].ToString())),
                  int.Parse(ds.Tables[0].Rows[0]["opc_tip_com"].ToString()),
                  ds.Tables[0].Rows[0]["par_des"].ToString(),
                  ds.Tables[0].Rows[0]["par_des_par"].ToString(),
                  (ds.Tables[0].Rows[0]["ope_val_gmf"].ToString().Equals("") ? 0 : double.Parse(ds.Tables[0].Rows[0]["ope_val_gmf"].ToString()))

                  // ID_PLAN
                  ,ds.Tables[0].Rows[0]["ID_PLAN"].ToString()
                  
                );
            }// fin del IF
            else
            {
                operacion = null;
            }
            return operacion;
        }//fin de la ClsOperacionCls

        public DataSet Devuelve_OperacionXnum_ope_Plan(int num_ope)
        {
            DataSet ds;
            String sqlstr = "";

            sqlstr = "EXEC SP_OP_DEVOLVER_OPERACION_X_NUM_OPE_X_PLAN " + num_ope;

            ds = sqlquery.ExecuteDataSet(sqlstr);


            return ds;
        }


        public DataSet Devuelve_DOC_Operacion_Proveedor(int Num_Operacion)
        {
            //se crea para traer documentos de una operacion especifica pizarra VB_COnfirming
            DataSet ds;
            String sqlstr = "";

            sqlstr = "EXEC Sp_Devuelve_DOC_Operacion_Proveedor " + Num_Operacion;
            
            ds = sqlquery.ExecuteDataSet(sqlstr);


            return ds;
        }
        
        public bool EnvioGerencia_Operacion(string Observ_Operacion, int Numero_Negocio)
        {
            string Sql;
            bool valido = false;
            
            try
            {
                Sql = "";
                Sql = "UPDATE OPC SET PAR_EST = 9, OPC_OBS_REV_CRE ='";
                Sql = Sql + Observ_Operacion + "'";
                Sql = Sql + "WHERE OPC_NUM_CFG = " + Numero_Negocio;
                                
                sqlquery.ExecuteNonQuery(Sql);
                if (sqlquery.estadoconsulta != 99)
                    valido = true;
                else
                    valido = false;
            }
            catch (Exception ex)
            {
                Sql = ex.Message.ToString();
                valido = false;
            }
            return valido;
        }

        public bool Guardar_Operacion(  string Observ_Operacion, string Perfil_Usuario, string Rev_Lega, 
                                        string Lbl_Usuario, int Numero_Negocio)
        {
            String Sql = "";
            bool respuesta = false;
            try
            {
                Sql = "";
                Sql = "UPDATE OPC SET OPC_OBS_REV_CRE ='";
                Sql = Sql + Observ_Operacion + "'";

                if (Perfil_Usuario == "LEGAL")
                {
                    Sql = Sql + ", OPC_REV_LEG = '" + Rev_Lega + "'";
                    Sql = Sql + ", OPC_USU_LEG = '" + Lbl_Usuario + "'";
                }
                
                Sql = Sql + " WHERE OPC_NUM_CFG =  " + Numero_Negocio;
                
                sqlquery.ExecuteNonQuery(Sql);
                respuesta = true;
            }
            catch (Exception ex)
            {
                Sql = ex.Message.ToString();
                respuesta = false;
            }
            return respuesta;

        }
        
        public bool Rechazar_Operacion(string Tipo_Operacion, string Observ_Operacion, int Numero_Negocio)
        {
            string  Sql = "",
                    Sql_Doc = "";
             //FY SE AGREGA INT
             bool respuesta=false;
            try
            {
                //'SI LA OPERACION ES INTERNET EL RECHAZO ANULA LA OPERACION
                if (Tipo_Operacion == "I")
                {
                    Sql = "";
                    Sql = "UPDATE OPC SET PAR_EST = 20,";
                    Sql = Sql + " OPC_FEV_APB_REC = '" + rg.FechaJuliana((DateTime.Today.Date).ToString()) + "',";
                    Sql = Sql + " OPC_OBS_REV_CRE = '" + Observ_Operacion + "'";
                    Sql = Sql + " WHERE OPC_NUM_CFG  = " + Numero_Negocio;
                    
                    sqlquery.ExecuteNonQuery(Sql);
                    
                    Sql_Doc = "";
                    Sql_Doc = "UPDATE DOC SET OPC_NUM  = null,";
                    Sql_Doc = Sql_Doc + " DOC_SAL_PEN  = DOC_MTO,";
                    Sql_Doc = Sql_Doc + " DOC_MTO_FIN  = DOC_MTO,";
                    Sql_Doc = Sql_Doc + " PAR_EST =  10,";
                    Sql_Doc = Sql_Doc + " DOC_DIF_PRE_GPG = null,";
                    Sql_Doc = Sql_Doc + " DOC_DIF_PRE_GPG_ORI = null";
                    Sql_Doc = Sql_Doc + " WHERE opc_num  = " + Numero_Negocio;
                    
                    if (sqlquery.ExecuteNonQuery(Sql_Doc) > 0)
                    {
                        respuesta = true;
                    }

                }
                else
                {
                    Sql = "";
                    Sql = Sql + "UPDATE OPC SET PAR_EST = 21,";
                    Sql = Sql + " OPC_FEC_APB_REC = '" + rg.FechaJuliana((DateTime.Today.Date).ToString()) + "',";
                    Sql = Sql + " OPC_OBS_REV_CRE = '" + Observ_Operacion + "'";
                    Sql = Sql + " WHERE OPC_NUM_CFG  = " + Numero_Negocio;


                    if (sqlquery.ExecuteNonQuery(Sql) > 0)
                    {
                        respuesta = true;
                    }

                }

                  if (respuesta == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }


             }
            catch (Exception ex)
            {
                Sql = ex.Message.ToString();
                respuesta = false;
                return false;
            }
        }
        
        public bool Otorgar_Operacion(string USER_MACHINE, string Observ_Operacion, int Numero_Negocio, 
                                        string Txt_Oper_Giro, long Gran_Pagador, double Monto_Cuenta, string cod_eje,
                                        string cod_eje_pvr)
        {
            string  Sql = "",
                    Sql_Doc="",
                    Sql2="";

            bool respuesta = false;
            try
            {

                Sql = "";
                Sql = "UPDATE OPC SET PAR_EST = 6 ,";
                Sql = Sql + " OPC_FEC_OTO = '" + rg.FechaJuliana((DateTime.Today.Date).ToString()) + "',";
                Sql = Sql + " OPC_HOR_OTO = '" + (DateTime.Now.TimeOfDay).ToString().Substring(0,8) + "',";
                Sql = Sql + " OPC_USU_OTO = '" + USER_MACHINE + "',";
                Sql = Sql + " OPC_OBS_REV_CRE = '" + Observ_Operacion + "',";
                Sql = Sql + " eje_cod = " + cod_eje +",";
                Sql = Sql + " eje_cod_PVR = " + cod_eje_pvr ;
                Sql = Sql + " WHERE opc_num_cfg  = " + Numero_Negocio;
                sqlquery.ExecuteNonQuery(Sql);

                Sql_Doc = "";
                Sql_Doc = "UPDATE DOC SET PAR_EST = 1  WHERE OPC_NUM  = " + Numero_Negocio;
                sqlquery.ExecuteNonQuery(Sql_Doc);

                //'Si operacion es giro 100% se crea cuenta en cxc y el Nº cta en tabla opc
                if (Txt_Oper_Giro == "SI")
                {
                    Sql2 = "";
                    Sql2 = "EXEC SP_OP_GENERA_CXC_GIRO_TOTAL ";
                    Sql2 = Sql2 + Gran_Pagador + ",";
                    Sql2 = Sql2 + Monto_Cuenta + ",";
                    Sql2 = Sql2 + Numero_Negocio + ",'";
                    Sql2 = Sql2 + USER_MACHINE + "','";
                    Sql2 = Sql2 + rg.FechaJuliana((DateTime.Today.Date).ToString()) + "'";
                    sqlquery.ExecuteNonQuery(Sql2);
                }

                respuesta = true;
            }
            catch (Exception ex)
            {
                Sql = ex.Message.ToString();
                respuesta = false;
            }
            
            return respuesta;

            
                
        }

        public DataSet DevuelveOperacionesXfecha(DateTime Fecha_desde, DateTime Fecha_fin)
        {
        // se crea para traer operaciones con fecha desde la pizarra de VB_Confirming
            DataSet ds;
            string strsql = "";

            strsql = "Exec Sp_Devuelve_Operaciones_Fecha '" + rg.FechaJuliana(Fecha_desde) + "','" + rg.FechaJuliana(Fecha_fin) + "'";
            ds = sqlquery.ExecuteDataSet(strsql);
            return ds;
        }
        
        public DataSet Devuelve_Monto_Documentos_Morosos(string Rut_Pagador)
        {
            DataSet ds;
            string strsql = "";
            strsql = "SELECT ISNULL(SUM(DOC_MTO),0) as DOC_MTO FROM DOC WHERE GPG_RUT ="+ Rut_Pagador;
            strsql += "AND PAR_EST = 2";
            ds = sqlquery.ExecuteDataSet(strsql);
            return ds;

        }

        public DataSet Devuelve_operaciones_Gran_Pagador(int Tip_Con, DateTime Fecha, string Estado, string usr)
        {
            DataSet ds;
            string sql = "";
            sql = "EXEC SP_OP_DEVUELVE_GP " + Tip_Con + ",'" + rg.FechaJuliana(Fecha) + "','" + Estado + "','"+usr+"'";
            ds = sqlquery.ExecuteDataSet(sql);
            return ds;
        }

        public DataSet Devuelve_Num_Operacion_Aprob()
        {
            DataSet ds = new DataSet();
            string sql = "";
            sql = "SELECT ISNULL(SIS_NRO_APR_GER,0) SIS_NRO_APR_GER FROM SIS";
            ds = sqlquery.ExecuteDataSet(sql);
            return ds;
        }

        public DataSet Devuelve_Operacion_x_Estado(int Estado,string usr)
        {
            DataSet ds = new DataSet();
            string sql = "";
            sql = "Sp_Buscar_X_Estado_Operacion " + Estado+",'"+usr+"'";
            ds = sqlquery.ExecuteDataSet(sql);
            return ds;
        }

        public DataSet Devuelve_Bloqueo_Operacion(string codigo, string num_ope)
        {
            DataSet ds = new DataSet();
            string sql = "";
            sql = "Sp_Buscar_Bloqueo_Operacion '" + codigo + "'," + num_ope;
            ds = sqlquery.ExecuteDataSet(sql);
            return ds;
        }

        public bool Actualiza_Bloqueo_Operacion(string codigo, string num_ope, string user)
        {
            bool valido = new bool();
            string sql = "";
            sql = "Sp_Actualizar_Bloqueo_Operacion '" + codigo + "'," + num_ope + "," + user;
            try
            {
                sqlquery.ExecuteNonQuery(sql);
                if (sqlquery.estadoconsulta != 99)
                    valido = true;
                else
                    valido = false;
            }
            catch (Exception e)
            {
                valido = false;                
            }
            return valido;
        }

        public bool Actualiza_Desbloqueo_Operacion(string codigo, string num_ope)
        {
            bool valido = new bool();
            string sql = "";
            sql = "Sp_Actualizar_Desbloqueo_Bloqueo_Operacion '" + codigo + "'," + num_ope;
            try
            {
                sqlquery.ExecuteNonQuery(sql);
                if (sqlquery.estadoconsulta != 99)
                    valido = true;
                else
                    valido = false;
               
            }
            catch (Exception e)
            {
                valido = false;
            }
            return valido;

        }

        public DataSet Devuelve_Aprobacion_Gcia(string codigo, string num_ope, string user)
        {
            DataSet ds = new DataSet();
            string sql = "";
            sql = "SELECT * FROM AOC  WHERE AOC_NRO_OPE = " + num_ope ;
            sql += " AND AOC_COD_USR = '" + user + "'" ;
            sql += " AND   AOC_TIP_DOC = '" + codigo + "'" ;
            ds = sqlquery.ExecuteDataSet(sql);
            return ds;
        }

        public DataSet Devuelve_Maximo_Cod_Aprob_Gcia(string codigo, string num_ope)
        {
            DataSet ds = new DataSet();
            string sql = "";

            sql = "SELECT isnull(MAX(AOC_COD_AOC),1) as AOC_COD_AOC FROM AOC WHERE AOC_NRO_OPE ="+num_ope;
            sql +=" AND AOC_TIP_DOC =' "+ codigo + "'";
            ds = sqlquery.ExecuteDataSet(sql);
            return ds;
        }

        public bool Actualiza_Aprobacion_Gcia_AOC(string estado, DateTime fecha, string hora, string num_ope, 
                                                    string cod, string user, string tip_doc)
        {
            bool valido= false;
            string sql = "";
            sql = "Exec Sp_Ope_Actualiza_AOC " + estado + ",'" + rg.FechaJuliana(fecha) + "','" + hora + "'," + num_ope + "," + cod + ",'" + user+"','" + tip_doc + "'" ;
            try
            {
                sqlquery.ExecuteNonQuery(sql);
                if (sqlquery.estadoconsulta != 99)
                    valido = true;
                else
                    valido = false;
            }
            catch (Exception e)
            {
                valido = false;
            }
            return valido; 
        }

        public bool Inserta_Aprobacion_Gcia_AOC(string estado, DateTime fecha, string hora, string num_ope, 
                                                string user, string tip_doc, string max_cod)
        {
            bool valido = false;
            string sql = "";
            sql = "Exec Sp_Ope_Inserta_AOC " + estado + ",'" + rg.FechaJuliana(fecha) + "','" + hora + "'," + num_ope + ",'" + user + "','" + tip_doc +"'," + max_cod;
            try
            {
                sqlquery.ExecuteNonQuery(sql);
                if (sqlquery.estadoconsulta != 99)
                    valido = true;
                else
                    valido = false;
            }
            catch (Exception e)
            {
                valido = false;
            }
            return valido;
        }

        public int Contador_Registro_AOC(string num_ope, string estado, string tip_doc)
        {
            int contador = 0;
            DataSet ds = new DataSet();
            string sql = "";
            sql = " SELECT ISNULL(COUNT(*),0) CONTADOR FROM AOC";  
            sql +=" WHERE AOC_NRO_OPE = " + num_ope;
            sql +=" AND AOC_EST_AOC = " + estado; 
            sql +=" AND AOC_TIP_DOC = '" + tip_doc + "'";
            try
            {
                ds = sqlquery.ExecuteDataSet(sql);
                contador = int.Parse(ds.Tables[0].Rows[0]["CONTADOR"].ToString());
            }
            catch (Exception e)
            {
                contador = int.Parse(estado);
            }
            return contador;
        }

        public bool Actualiza_Registro_OPC(string estado, string observaciones, string num_ope)
        {
            bool valido=false;

            string sql = "";
            sql = "Exec SP_OP_ACTUALIZA_TABLA_OPC " + estado + ",'" + observaciones + "'," + num_ope;
            try
            {
                sqlquery.ExecuteNonQuery(sql);
                if (sqlquery.estadoconsulta != 99)
                    valido = true;
                else
                    valido = false;
            }
            catch (Exception e)
            {
                valido = false;
            }
            return valido;
        }

        public bool Anula_Operacion_Rechazo_Internet(string estado, DateTime fecha, string observ, string num_ope)
        {
            bool valido = false;

            string sql = "";
            sql = "Exec SP_ANULA_OPERACION_INTERNET " + estado + ",'" + rg.FechaJuliana(fecha) +"','"+observ + "',"+ num_ope;
            try
            {
                sqlquery.ExecuteNonQuery(sql);
                if (sqlquery.estadoconsulta != 99)
                    valido = true;
                else
                    valido = false;
            }
            catch (Exception e)
            {
                valido = false;
            }
            return valido;
        }

        public DataSet Devuelve_Operaciones_X_Proveedor(string rut, DateTime fecha_ini, DateTime fecha_fin, string codigo, string usr)
        {
            DataSet ds = new DataSet();
            string sql = "";

            sql = "Exec Sp_OP_Devuelve_Operaciones_Proveedor '" + rut + "','" + rg.FechaJuliana(fecha_ini) + "','" + rg.FechaJuliana(fecha_fin) + "'," + codigo+",'"+usr+"'";            
            ds = sqlquery.ExecuteDataSet(sql);
            return ds;
        }

        public bool Actualiza_Oper_OPC(string estado, string letra_otorg, DateTime fecha, string user, 
                                        string num_ope)
        {
            bool valido = false;

            string sql = "";
            sql = "Update OPC set ";
            sql += " par_est = " + estado + ",";
            sql += " opc_anl_otg ='" + letra_otorg + "',";
            sql += " opc_fec_anl ='" + rg.FechaJuliana(fecha) + "',";
            sql += " opc_usu_anl ='" + user + "'";
            sql += " where opc_num_cfg = " + num_ope;
            try
            {
                sqlquery.ExecuteNonQuery(sql);
                if (sqlquery.estadoconsulta != 99)
                    valido = true;
                else
                    valido = false;
            }
            catch (Exception e)
            {
                valido = false;
            }
            return valido;

        }

        public bool Insertar_DOC_Anulado(string num_ope)
        {
            bool valido = false;
            string sql = "";
            sql = " Insert into dan Select * from doc ";
            sql += " where opc_num = " + num_ope;
           
            try
            {
                sqlquery.ExecuteNonQuery(sql);
                if (sqlquery.estadoconsulta != 99)
                    valido = true;
                else
                    valido = false;
            }
            catch (Exception e)
            {
                valido = false;
            }
            return valido;
        }

        public bool Actualiza_Documentos_Operacion_DOC(string estado,string num_ope)
            {
                bool valido = false;
                string sql = "";
                sql = " Update doc set par_est =" + estado + ",";
                sql += " DOC_SAL_PEN = DOC_MTO_FIN,";
                sql += " opc_num = NULL,doc_dif_pre_gpg_ori = NULL, doc_dif_pre_gpg = NULL";
                sql += " where opc_num =" + num_ope ;
                try
                {
                    sqlquery.ExecuteNonQuery(sql);
                    if (sqlquery.estadoconsulta != 99)
                        valido = true;
                    else
                        valido = false;
                }
                catch (Exception e)
                {
                    valido = false;
                }
                return valido;
            }

        public bool Eliminar_CuentaXCobrar_Giro100(string num_cuenta)
        {
            bool valido = false;
            string sql = "";
            sql = "DELETE FROM CXC WHERE  CXC_NUM_CTA = " + num_cuenta;             
            try
            {
                sqlquery.ExecuteNonQuery(sql);
                if (sqlquery.estadoconsulta != 99)
                    valido = true;
                else
                    valido = false;
            }
            catch (Exception e)
            {
                valido = false;
            }
            return valido;
        }

        public bool SumarMtoDisp_Pagador_a_ANT(string monto, string rut_pagador,string tip_mon)
          {
            bool valido = false;
            string sql = "";
            sql  = " UPDATE ANT SET";
            sql += " ANT_MON_DIS = ANT_MON_DIS + " + monto;
            sql += " WHERE ANT_RUT_CLI = " + rut_pagador;
            sql += " AND ANT_TIP_DOC = 20";
            sql += " AND ANT_TIP_MON = " + tip_mon;
            try
            {
                sqlquery.ExecuteNonQuery(sql);
                
                if (sqlquery.estadoconsulta != 99)
                    valido = true;
                else
                    valido = false;
                
            }
            catch (Exception e)
            {
                valido = false;
            }
            return valido;
        }

        public DataSet Devuelve_Saldo_Gran_Pagador(string rut, string cod_tip_mon)
        {
            DataSet ds = new DataSet();
            string sql = "";

            sql = "Exec SP_OP_DEVUELVE_SALDOS " + rut + "," + cod_tip_mon;
            ds = sqlquery.ExecuteDataSet(sql);

            return ds;
        }

        public bool Actualiza_CTA_CXP(string RUT_GPG, string RUT_DEU_CXP, string TIP_CTA_CXP, 
                                      string NUM_CTA_CXP, string MTO_PAG_CXP, string correl_pago)
        {
            string SQL_CTA = "";
            try
            {
                SQL_CTA = "EXEC SP_OP_ACTUALIZA_CTA ";
                SQL_CTA += RUT_GPG + ",";
                SQL_CTA += RUT_DEU_CXP + ",";
                SQL_CTA += TIP_CTA_CXP + ",";
                SQL_CTA += NUM_CTA_CXP + ",";
                SQL_CTA += MTO_PAG_CXP + ",";
                SQL_CTA += correl_pago;

                sqlquery.ExecuteNonQuery(SQL_CTA);

                if (sqlquery.estadoconsulta != 99)
                     return true;
                else
                    return false;

            }
            catch (Exception e)
            {
                return false;
            }
        }
        
        public bool Transaccion_Pago(List<string> lista_Query)
        {
            if (sqlquery.ExecuteTransaccion(lista_Query))
                return true;
            else
                return false;
        }
        
        public DataSet DevuelveGPxOperacion(int num_ope)
        {
            DataSet ds;
            String sql = "";

            sql = "select top 1 d.gpg_rut,d.par_mon,o.par_est " +
                  "from DOC d " +
                   "inner join OPC o on o.opc_num_cfg=d.opc_num " +
                   "where opc_num= " + num_ope;

            ds = sqlquery.ExecuteDataSet(sql);
            return ds;
        }

        public bool Actualiza_Linea_Finan_PVR(string rut_prov)
        {
            bool valido = false;
            string sql = "";
            sql = " EXEC SP_CDIA_LINEASYDOCTOS_PVR " + rut_prov;
           
            try
            {
                sqlquery.ExecuteNonQuery(sql);
                if(sqlquery.estadoconsulta != 99)
                    valido = true;
                else
                    valido = false;

            }
            catch (Exception e)
            {
                valido = false;
            }
            return valido;

        }
        
        public bool Actualiza_Linea_Finan_GPG(string rut_Gpg)
        {
            bool valido = false;
            string sql = "";
            sql = " EXEC SP_CDIA_LINEASYDOCTOS_GPG " + rut_Gpg;

            try
            {
                sqlquery.ExecuteNonQuery(sql);

                if (sqlquery.estadoconsulta != 99)
                    valido = true;
                else
                    valido = false;
                
            }
            catch (Exception e)
            {
                valido = false;
            }
            return valido;

        }

        public bool Asigna_Nro_Contrato(DateTime fecha_hoy)
        {
            bool valido = false;
            string sql = "";
            sql = " EXEC sp_prc_asignacontrato '" + rg.FechaJuliana(fecha_hoy) + "'";

            try
            {
                sqlquery.ExecuteNonQuery(sql);
                if (sqlquery.estadoconsulta != 99)
                    valido = true;
                else
                    valido = false;
            }
            catch (Exception e)
            {
                valido = false;
            }
            return valido;
        }

        public bool Inserta_GMF_CXC(string rut_pag, string monto, string user, DateTime fecha, string tip_moneda, string factor_cambio, string num_ope)
        {

            string sql = "";
            sql = " EXEC SP_OP_AGREGA_VALOR_GMF_A_CXC " + rut_pag
                                                        + "," + monto
                                                        + ",'" + user
                                                        + "','" + rg.FechaJuliana(fecha)
                                                        + "'," + tip_moneda
                                                        + "," + factor_cambio
                                                        + "," + num_ope;

            
            try
            {
                sqlquery.ExecuteNonQuery(sql);
                
                if(sqlquery.estadoconsulta.Equals("99"))
                {
                    return false;
                }

            }
            catch (Exception e)
            {
                return false;
            }



            return true;
                 
        }

        public DataSet Devuelve_Utiliza_Plan_OPC(int Num_Opc)
        {
            DataSet ds;
            String sql = "";

            sql = " EXEC DEVUELVE_NUM_PLAN_X_OPERACION " + Num_Opc;

            ds = sqlquery.ExecuteDataSet(sql);
            if (sqlquery.estadoconsulta != 99)
            {
                return ds;
            }
            return null;
        }


        public DataSet Devolver_Cond_Fin_x_DOC(string num_doc)
        {
            DataSet ds;
            String sql = "";

            sql = " EXEC SP_OP_DEVOLVER_COND_FINAC_X_DOC " + rg.LimpiaRut(num_doc);

            ds = sqlquery.ExecuteDataSet(sql);
            if (sqlquery.estadoconsulta != 99)
            {
                return ds;
            }
            return null;
        }
        

#endregion

    }
    
    
}
