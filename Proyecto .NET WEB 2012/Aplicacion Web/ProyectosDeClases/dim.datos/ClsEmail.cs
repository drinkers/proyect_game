﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using dim.rutinas;
using System.Data;
using System.Data.SqlClient;

namespace dim.datos
{
    public class ClsEmail
    {
        //--------------------------------------------------------------------//
        SqlQuery sqlquery = new SqlQuery();
        RutinasGenerales rg = new RutinasGenerales();

        public string descripcion { get; set; }


        //--------------------------------------------------------------------//
        
        public bool InsertaEmail(int Codigo, string Nombre, string Email)
        {
            DataSet ds;
            String sqlstr = "";
            
            try
            {
                sqlstr = "SP_Add_Email " + Codigo + ",'" + Nombre + "','" + Email + "'";

                ds = sqlquery.ExecuteDataSet(sqlstr);
                //return ds;
                return true;
            }

            catch
            {
                return false;
            }


        }

        public DataSet DevuelvecorEmail(int Cod_email)
        {
            DataSet ds;
            String sqlstr = "";

            sqlstr = "Sp_Get_Cod_Email " + Cod_email;

            ds = sqlquery.ExecuteDataSet(sqlstr);
            return ds;
        }

        public bool ModificaEmail(int Codigo, string Nombre, string Email)
        {

            DataSet ds;
            String sqlstr = "";

            try
            {
                sqlstr = "Sp_Modifica_Email " + Codigo + ",'" + Nombre + "','" + Email + "'";
                ds = sqlquery.ExecuteDataSet(sqlstr);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool EliminaEmail(int Tipo,int Codigo )
        {

            DataSet ds;
            String sqlstr = "";

            try
            {
                sqlstr = "Sp_Del_Email " + Tipo +","+Codigo ;
                ds = sqlquery.ExecuteDataSet(sqlstr);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public DataSet ExisteEmail(String email)
        {
            DataSet ds;
            String sqlstr = "";

            sqlstr = "SP_BUSCAR_EMAIL '" + email + "'";

            ds = sqlquery.ExecuteDataSet(sqlstr);
            return ds;
        }

        public DataSet Devuelve_Datos_Mensajeria(int Numero_Operacion, string Tipo_Operacion)
        {
            DataSet ds = null; ;
            String sqlstr = "";

            sqlstr = "SP_OP_DEVUELVE_DATOS_MENSAJERIA " + Numero_Operacion + ",'" + Tipo_Operacion + "'";
            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;
        }
    }    

}