﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using dim.rutinas;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;

namespace dim.datos
{
    public class ClsDocumentos
    {
        SqlQuery sqlquery = new SqlQuery();
        RutinasGenerales rg = new RutinasGenerales();

        public int codigo { get; set; }
        public string descripcion { get; set; }
        public string MENSAJE { get; set; }
        public int num_nomina { get; set; }


        #region PAGADOR

        public DataSet DevuelveDocumentosVctosFuturos(long rutproveedor, long rutpagador,
                                                      DateTime fecdesde, DateTime fechasta,
                                                      string quien)
        {
            DataSet ds;
            String sqlstr = "";

            if (quien.Equals("P"))
                sqlstr = "EXEC SP_WEB_ResumenVencimientosFuturosPRV " + rutproveedor + ", " + rg.digitoVerificador(rutproveedor) + ", " +
                                                                        rutpagador + ",  " + rg.digitoVerificador(rutpagador) + ", '" +
                                                                        rg.FechaJuliana(fecdesde) + "', '" + rg.FechaJuliana(fechasta) + "'";
            if (quien.Equals("G"))
                sqlstr = "EXEC SP_WEB_ResumenVencimientosFuturosGPG " + rutproveedor + ", " + rg.digitoVerificador(rutproveedor) + ", " +
                                                                        rutpagador + ",  " + rg.digitoVerificador(rutpagador) + ", '" +
                                                                        rg.FechaJuliana(fecdesde) + "', '" + rg.FechaJuliana(fechasta) + "'";

            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;

        }

        public DataSet DevuelveDocumentosOrdenNoPagoGPG(long rutproveedor, long rutpagador, string nrofactura)
        {
            DataSet ds;
            String sqlstr = "";

            sqlstr = "SP_WEB_DEVUELVE_DOCUMENTO_PARA_ORDEN_DE_NO_PAGO " + rutproveedor + ", " + rutpagador + ", " + nrofactura + "";

            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;

        }

        public DataSet DevuelveDocumentosConsulta(long rutpagador, long rutproveedor, int nrofactura, int nronomina)
        {
            DataSet ds;
            String sqlstr = "";

            sqlstr = "SP_WEB_ConsultaDocumentosNominaGpg " + rutpagador + ", " + rutproveedor + ", " + nrofactura + ", " + nronomina;

            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;

        }

        //public DataSet DevuelveDocumentosCambioVcto(long rutpagador)
        //{
        //    DataSet ds;
        //    String sqlstr = "";

        //    sqlstr = "SP_WEB_DOCUMENTOS_CAMBIO_VCTO " + rutpagador;

        //    ds = sqlquery.ExecuteDataSet(sqlstr);

        //    return ds;

        //}

        //public DataSet DevuelveDetalleDocumentosCambioVcto(long rutpagador, long rutproveedor, DateTime fecha)
        //{
        //    DataSet ds;
        //    String sqlstr = "";

        //    sqlstr = "SP_WEB_DOCUMENTOS_CAMBIO_VCTO_DETALLE " + rutpagador + ", " + rutproveedor + ", '" + rg.FechaJuliana(fecha) + "'";

        //    ds = sqlquery.ExecuteDataSet(sqlstr);

        //    return ds;

        //}

        public bool AgregaGrupoDocumentosCambioVcto(long rutpagador, long rutproveedor, DateTime fecha, int accion)
        {
            String sqlstr = "";

            sqlstr = "SP_WEB_ACCION_AGRUPACION_VCTO " + rutpagador + ", " + rutproveedor + ", '" + rg.FechaJuliana(fecha) + "', " + accion;

            sqlquery.ExecuteNonQuery(sqlstr);

            return true;

        }

        public bool AgregaGrupoDocumentosCambioVctoNro(long rutpagador, long rutproveedor, DateTime fecha, string numero, int accion)
        {
            String sqlstr = "";

            sqlstr = "SP_WEB_ACCION_AGRUPACION_VCTO_NRO " + rutpagador + ", " + rutproveedor + ", '" + rg.FechaJuliana(fecha) + "', " + numero + ", " + accion;

            sqlquery.ExecuteNonQuery(sqlstr);

            return true;

        }

        public bool EliminaDocumentosCambioVcto(long rutpagador)
        {

            string strSQL = "";

            strSQL = "Delete From Doc_Fev Where gpg_rut = " + rutpagador;

            sqlquery.ExecuteNonQuery(strSQL);

            return true;

        }

        public bool GuardaDocumentosCambioVcto(long rutpagador, DateTime fecha)
        {

            string strSQL = "";

            strSQL = "SP_WEB_GUARDA_CAMBIO_FECHA_VCTO " + rutpagador + ", " + rg.FechaJuliana(fecha);

            sqlquery.ExecuteNonQuery(strSQL);

            return true;

        }

        public DataSet Devuelve_Dctos_por_Comprar(string rut_pagador, DateTime fec_ini, DateTime fec_fin, string cod_moneda, string usr)
        {
            DataSet ds;
            string sql_str = "";

            sql_str = "EXEC SP_OP_DEVUELVE_DOCTOS_POR_COMPRAR " + rut_pagador + ",'" + rg.FechaJuliana(fec_ini) + "','" + rg.FechaJuliana(fec_fin) + "',";
            sql_str += "10,12," + cod_moneda + ",'" + usr + "'";
            ds = sqlquery.ExecuteDataSet(sql_str);

            return ds;
        }

        public DataSet Devuelve_Dctos_por_Comprar_Totales(string rut_pagador, DateTime fec_ini, DateTime fec_fin, string cod_moneda, string usr)
        {
            DataSet ds;
            string sql_str = "";

            sql_str = "EXEC SP_OP_DEVUELVE_DOCTOS_POR_COMPRAR_TOTAL " + rut_pagador + ",'" + rg.FechaJuliana(fec_ini) + "','" + rg.FechaJuliana(fec_fin) + "',";
            sql_str += "10,12," + cod_moneda + ",'" + usr + "'";
            ds = sqlquery.ExecuteDataSet(sql_str);

            return ds;
        }
        //GNI aumento de 2 a 4 estado 
        public DataSet Devuelve_Dctos_por_Comprar_Ope(string rut_pagador, DateTime fec_ini, DateTime fec_fin, string cod_moneda, string usr, int num_ope)
        {
            DataSet ds;
            string sql_str = "";

            sql_str = "EXEC [SP_OP_DEVUELVE_DOCTOS_POR_COMPRAR_OPE] " + rut_pagador + ",'" + rg.FechaJuliana(fec_ini) + "','" + rg.FechaJuliana(fec_fin) + "',";
            sql_str += "1,4," + cod_moneda + ",'" + usr + "'," + num_ope;
            ds = sqlquery.ExecuteDataSet(sql_str);

            return ds;
        }

        //GNI aumento de 2 a 4 estado 
        public DataSet Devuelve_Dctos_por_Comprar_Totales_Ope(string rut_pagador, DateTime fec_ini, DateTime fec_fin, string cod_moneda, string usr, int num_ope)
        {
            DataSet ds;
            string sql_str = "";

            sql_str = "EXEC [SP_OP_DEVUELVE_DOCTOS_POR_COMPRAR_TOTAL_OPE] " + rut_pagador + ",'" + rg.FechaJuliana(fec_ini) + "','" + rg.FechaJuliana(fec_fin) + "',";
            sql_str += "1,4," + cod_moneda + ",'" + usr + "'," + num_ope;
            ds = sqlquery.ExecuteDataSet(sql_str);

            return ds;
        }




        public bool Transaccion_Elimina_Dctos_GP(List<string> lista_Query)
        {
            if (sqlquery.ExecuteTransaccion(lista_Query))
                return true;
            else
                return false;
        }

        #endregion

        #region PROVEEDOR

        public DataSet DevuelveDocumentos(long rutproveedor, long rutpagador,
                                          DateTime fecdesde, DateTime fechasta)
        {
            DataSet ds;
            String sqlstr = "";

            sqlstr = "EXEC SP_WEB_ResumenVencimientosFuturosPRV " + rutproveedor + ", " + rg.digitoVerificador(rutproveedor) + ", " +
                                                                 rutpagador + ",  " + rg.digitoVerificador(rutpagador) + ", '" +
                                                                 rg.FechaJuliana(fecdesde) + "', '" + rg.FechaJuliana(fechasta) + "'";

            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;

        }

        public DataSet DevuelveDocumentos(long rutproveedor, long rutpagador,
                                          int nrooperacion)
        {
            DataSet ds;
            String sqlstr = "";

            sqlstr = "EXEC SP_CO_DEVUELVE_DOCTOS_POR_NRO_OPERACION " + rutproveedor + ", " + rutpagador + ",  " + nrooperacion;

            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;

        }

        public DataSet DevuelveDocumentos(long rutpvr_desde, long rutpvr_hasta,
                                          long rutgpg_desde, long rutgpg_hasta,
                                          string docto_desde, string docto_hasta,
                                          long operac_desde, long operac_hasta,
                                          long estado_desde, long estado_hasta,
                                          DateTime vcto_desde, DateTime vcto_hasta,
                                          DateTime Otorga_desde, DateTime Otorga_hasta,
                                          long tipo_desde, long tipo_hasta,
                                          string usuario, string num_contrato, int nro_carga, int nro_carga2)
        {


            DataSet ds;
            String sqlstr = "";


            sqlstr = "EXEC SP_GE_DEVUELVE_DOC_PVR_GPG " + rutpvr_desde + ", " + rutpvr_hasta + ",  " +
                                      rutgpg_desde + ", " + rutgpg_hasta + ",  '" +
                                      docto_desde + "', '" + docto_hasta + "',  " +
                                      operac_desde + ", " + operac_hasta + ",  " +
                                      estado_desde + ", " + estado_hasta + ",  '" +
                                      rg.FechaJuliana(vcto_desde) + "', '" + rg.FechaJuliana(vcto_hasta) + "',  '" +
                                      rg.FechaJuliana(Otorga_desde) + "', '" + rg.FechaJuliana(Otorga_hasta) + "',  " +
                                      tipo_desde + ", " + tipo_hasta + ", '" +
                                      usuario + "','" + num_contrato + "'," + nro_carga + "," + nro_carga2;

            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;

        }

        public DataSet DevuelveDocumentosDetalle(long rutproveedor, long rutpagador, DateTime fecvcto)
        {
            DataSet ds;
            String sqlstr = "";

            sqlstr = "EXEC SP_WEB_ResumenVencimientosFuturosDetallePRV " + rutproveedor + ", " +
                                                                        rutpagador + ",  '" +
                                                                        rg.FechaJuliana(fecvcto) + "'";

            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;

        }

        public DataSet Devuelve_calificacion_gpg(string rutpagador)
        {
            DataSet ds;
            String sqlstr = "";

            sqlstr = "select gpg_cal_ope from gpg where gpg_rut = '" + rutpagador + "'";

            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;

        }

        public DataSet DevuelveFacPagadas(string rutproveedor, string rutpagador, DateTime fecdesde, DateTime fechasta, int val, string desde, string hasta)
        {
            DataSet ds;
            String sqlstr = "";

            sqlstr = "EXEC SP_WEB_ResumenPagadosPRV " + rutproveedor + ", " + rutpagador + ", '" + rg.FechaJuliana(fecdesde) + "', '" + rg.FechaJuliana(fechasta) + "'," + val + " , " + desde + "," + hasta;
            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;

        }

        public DataSet DevuelveConsultaFact(string rutproveedor, string rutpagador, string mes, string año, int num, int ope)
        {
            DataSet ds;
            String sqlstr = "";

            sqlstr = "EXEC SW_WEB_ConsultaFacturasPVR " + rutproveedor + ", " + rutpagador + ", '" + mes + "', '" + año + "' ," + num + " , " + ope;
            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;

        }

        public DataSet DevuelveConsultaNoPagador(string rutproveedor, string rutpagador, DateTime desde, DateTime hasta, string inicio, string final)
        {
            DataSet ds;
            String sqlstr = "";

            sqlstr = "EXEC SW_WEB_ConsultaOrdenNoPagoPVR " + rutproveedor + " , " + rutpagador + ",  '" + desde + "', '" + hasta + "', '" + inicio + "', '" + final + "'";
            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;

        }

        public DataSet DevuelveNotaCreditoDescuento(string rutprv, string rutgpg, DateTime desde, DateTime hasta)
        {
            DataSet ds;
            String sqlstr = "";

            sqlstr = "EXEC SP_WEB_ConsultaNotaCreditoDescuentos " + rutprv + ",  " + rutgpg + ", '" + rg.FechaJuliana(desde) + "', '" + rg.FechaJuliana(hasta) + "'";
            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;

        }

        public DataSet DevuelveRazonProv(string rutprv)
        {
            DataSet ds;
            String sqlstr = "";

            sqlstr = "EXEC SP_WEB_DevuelveRazonProv " + rutprv;
            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;

        }

        public DataSet DevuelvePagador(string num_ope)
        {
            DataSet ds;
            String sqlstr = "";

            sqlstr = "SELECT D.GPG_RUT, G.GPG_RSN_SOC FROM DOC D INNER JOIN GPG G ON(D.GPG_RUT = G.GPG_RUT) WHERE D.OPC_NUM = " + num_ope;
            ds = sqlquery.ExecuteDataSet(sqlstr);
            return ds;
        }

        public DataSet DevuelvePagador_Tbla_DAN(string num_ope)
        {
            DataSet ds;
            String sqlstr = "";

            sqlstr = "SELECT D.GPG_RUT, G.GPG_RSN_SOC FROM DAN D INNER JOIN GPG G ON(D.GPG_RUT = G.GPG_RUT) WHERE D.OPC_NUM =" + num_ope;
            ds = sqlquery.ExecuteDataSet(sqlstr);
            return ds;
        }



        #endregion

        #region ORDEN DE NO PAGO

        public int DarOrdenDeNoPago(long rutproveedor, int nrodocto)
        {
            String sqlstr = "";

            sqlstr = "Exec sp_web_da_orden_no_pago " + rutproveedor + ", " + nrodocto;

            return sqlquery.ExecuteNonQuery(sqlstr);

        }

        #endregion

        #region NEGOCIACION

        public DataSet DevuelveDocumentosNegociacion(long rutproveedor, long rutpagador, DateTime fecdesde, DateTime fechasta, int idmoneda, int nrooperacion)
        {
            DataSet ds;
            String sqlstr = "";

            sqlstr = "EXEC SP_WEB_DEVUELVE_GRUPOS_GPG_POR_FECHA " + rutproveedor
                                                                + ", " +
                                                                    rutpagador
                                                                + ", '" +
                                                                    rg.FechaJuliana(fecdesde)
                                                                + "', '" +
                                                                    rg.FechaJuliana(fechasta)
                                                                + "', " +
                                                                    idmoneda
                                                                + ", " +
                                                                    nrooperacion;

            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;

        }

        public DataSet DevuelveDocumentosNegociacion_Plan(long rutproveedor, long rutpagador, DateTime fecdesde, DateTime fechasta, int idmoneda, int nrooperacion, DateTime fecha_sim)
        {
            DataSet ds;
            String sqlstr = "";

            sqlstr = "EXEC SP_OP_DEVUELVE_DOCTOS_X_MONEDA_USO_PLAN " + rutproveedor + ", " +
                                                                       rutpagador + ", '" +
                                                                       rg.FechaJuliana(fecdesde) + "', '" +
                                                                       rg.FechaJuliana(fechasta) + "', " +
                                                                       idmoneda + ", " +
                                                                       nrooperacion + ",'" + rg.FechaJuliana(fecha_sim) + "'";

            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;

        }


        public Double DevuelveDocumentosMorosos(long rutpagador)
        {
            DataSet ds;
            String sqlstr = "";
            Double morosos = 0;

            sqlstr = "SELECT ISNULL(SUM(DOC_SDO_GPG),0) FROM DOC WHERE GPG_RUT = " + rutpagador + " AND PAR_EST = 2";

            ds = sqlquery.ExecuteDataSet(sqlstr);

            if (ds.Tables[0].Rows.Count > 0)
                morosos = double.Parse(ds.Tables[0].Rows[0][0].ToString());

            ds.Dispose();

            return morosos;

        }

        public DataSet DevuelveDocumentosXOperacion(int num_ope, long gpg_rut, long prv_rut)
        {
            DataSet ds;
            String sqlstr = "";

            sqlstr = " EXEC SP_CO_DEVUELVE_DOC_OPE " + num_ope + ","
                                                     + gpg_rut + ","
                                                     + prv_rut;
            ds = sqlquery.ExecuteDataSet(sqlstr);
            return ds;
        }

        #endregion

        #region MANTENCION DOCUMENTOS

        public DataSet Valida_Doctos_Factoring(long rutPVR, long rutGPG, string nro_fac, int tip_doc)
        {
            DataSet ds;
            String sqlstr;

            sqlstr = "SP_CO_VALIDA_DOCTOS_FACTORING " + rutPVR + ", "
                                                      + rutGPG + ", '"
                                                      + nro_fac + "', "
                                                      + tip_doc;
            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;

        }

        public DataSet Devuelve_Doc(long rutGPG, long rutPVR, int tip_doc, string nro_doc)
        {
            DataSet ds;
            String sqlstr;

            sqlstr = "SP_MA_DEVUELVE_DOC " + rutGPG + ", "
                                           + rutPVR + ", "
                                           + tip_doc + ", '"
                                           + nro_doc + "'";
            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;
        }


        public DataSet Devuelve_Doc_NOPAGO(long rutPVR, int tip_doc, string nro_doc)
        {
            DataSet ds;
            String sqlstr;

            sqlstr = "SP_CO_DOC_NOPAGO " + rutPVR + ", "
                                           + tip_doc + ", '"
                                           + nro_doc + "'";
            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;
        }




        public bool ValidaCalPago(long rutGPG)
        {
            String sqlstr;
            DataSet ds;

            sqlstr = "SP_MA_DEVUELVE_GPG_FACOPE " + rutGPG;

            ds = sqlquery.ExecuteDataSet(sqlstr);

            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["GPG_CAL_PAG_SON"].ToString() == "S")
                    return true;
                else
                    return false;
            }
            else
            {
                return false;
            }


        }

        public bool EXISTE_DOC(long prv_rut, string AUX_NRO_DOC, int AUX_TIP_DOC)
        {
            DataSet ds;
            string nro_doc = AUX_NRO_DOC;
            int tip_doc = AUX_TIP_DOC;
            string sqlstr;



            sqlstr = "SP_MA_VALIDA_DOC " + prv_rut + ", '"
                                         + nro_doc + "', 0, "
                                         + tip_doc;
            ds = sqlquery.ExecuteDataSet(sqlstr);

            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0][1].ToString() == "SI")
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }

            return false;

        }

        public bool EliminaDocumento(long PRV_RUT, int tip_doc, string NRO_DOC)
        {
            string sqlstr;

            sqlstr = "";
            sqlstr = "SP_MA_ELIMINA_DOC " + PRV_RUT + ", "
                                          + tip_doc + ", '"
                                          + NRO_DOC + "', 0";
            if (sqlquery.ExecuteNonQuery(sqlstr) > 0)
                return true;
            else
                return false;
        }

        public bool ValidaFactorCambio(int mon, DateTime fecha)
        {
            String sqlstr;
            DataSet ds;

            sqlstr = "SELECT * FROM MON WHERE ID_MON = " + mon + " AND FEC_MON = '" + rg.FechaJuliana(fecha) + "'";
            ds = sqlquery.ExecuteDataSet(sqlstr);

            if (ds.Tables[0].Rows.Count > 0)
                return true;
            else
                return false;

        }


        #endregion

        public bool ActualizaDocumentosSimulacion(int num_ope, long pRutGpg, long pRutProv)
        {
            String sqlstr = "";
            sqlstr = "UPDATE DOC_INT SET ";
            sqlstr += "OPC_NUM = " + num_ope + ", ";
            sqlstr += "DOC_EST_INT = 2 ";
            sqlstr += "WHERE GPG_RUT = " + pRutGpg;
            sqlstr += " AND PRV_RUT = " + pRutProv;
            sqlstr += " AND DOC_EST_INT = 1";

            if (sqlquery.ExecuteNonQuery(sqlstr) > 0)
                return true;
            else
                return false;


        }

        public bool GuardaDocumento(long GPG_RUT, long PRV_RUT, string NRO_DOC, int tip_doc, double MTO_DOC, double MTO_DOC_ORI,
                                 int tip_mon, DateTime FEC_EMI, DateTime FEC_VTO, DateTime FEC_VTO_ORI, DateTime FEC_VEN_REA,
                                 int NRO_NOT_CRE, int DNC_MTO_DOC, DateTime DNC_FEC_EMI, DateTime DNC_FEC_VTO, DateTime FEC_ENV,
                                 string AUX_NRO_DOC, int AUX_TIP_DOC, int AUX_NUM_CRE, string ELI_NOT_CRE, string NRO_SAP,
                                 int EJE_CON, string OPP_OBS, string OPP_SON, string ACCION, string DOC_FOR_PGO, int id_plan)
        {
            try
            {
                string sqlstr;
                sqlstr = "";

                if (ACCION.Trim().ToUpper() == "MODIFICA")
                {
                    sqlstr = "SP_MA_MODIFICA_DOC " + PRV_RUT + ", '"
                                                   + NRO_DOC + "', "
                                                   + tip_doc + ", "
                                                   + MTO_DOC.ToString().Replace(",", ".") + ", "
                                                   + MTO_DOC_ORI.ToString().Replace(",", ".") + ", "
                                                   + tip_mon + ", '"
                                                   + rg.FechaJuliana(FEC_EMI) + "', '"
                                                   + rg.FechaJuliana(FEC_VTO) + "', '"
                                                   + rg.FechaJuliana(FEC_VTO_ORI) + "', '"
                                                   + rg.FechaJuliana(FEC_VEN_REA) + "', "
                                                   + NRO_NOT_CRE + ", "
                                                   + DNC_MTO_DOC + ", '"
                                                   + rg.FechaJuliana(DNC_FEC_EMI) + "', '"
                                                   + rg.FechaJuliana(DNC_FEC_VTO) + "', '"
                                                   + AUX_NRO_DOC + "', "
                                                   + AUX_TIP_DOC + ", "
                                                   + AUX_NUM_CRE + ", '"
                                                   + ELI_NOT_CRE + "', '"
                                                   + NRO_SAP + "', "
                                                   + EJE_CON + ", '"
                                                   + OPP_OBS + "', '"
                                                   + OPP_SON + "','"
                                                   + DOC_FOR_PGO + "','"
                                                   + id_plan + "'";

                    descripcion = "Documento modificado exitosamente";
                }
                else
                {
                    sqlstr = "SP_MA_INSERTA_DOC " + GPG_RUT + ", "
                                                  + PRV_RUT + ", "
                                                  + tip_doc + ", '"
                                                  + NRO_DOC + "', "
                                                  + MTO_DOC.ToString().Replace(",", ".") + ", "
                                                  + MTO_DOC_ORI.ToString().Replace(",", ".") + ", "
                                                  + tip_mon + ", '"
                                                  + rg.FechaJuliana(FEC_EMI) + "', '"
                                                  + rg.FechaJuliana(FEC_VTO) + "', '"
                                                  + rg.FechaJuliana(FEC_VTO_ORI) + "', '"
                                                  + rg.FechaJuliana(FEC_VEN_REA) + "', "
                                                  + NRO_NOT_CRE + ", "
                                                  + DNC_MTO_DOC + ", '"
                                                  + rg.FechaJuliana(DNC_FEC_EMI) + "', '"
                                                  + rg.FechaJuliana(DNC_FEC_VTO) + "', '"
                                                  + rg.FechaJuliana(FEC_ENV) + "', '"
                                                  + NRO_SAP + "', "
                                                  + EJE_CON + ", '"
                                                  + OPP_OBS + "', '"
                                                  + OPP_SON + "','"
                                                  + DOC_FOR_PGO + "','"
                                                  + id_plan + "'";

                    descripcion = "Documento guardado exitosamente";
                }

                int filas = sqlquery.ExecuteNonQuery(sqlstr);

                if (filas == 0)
                {
                    codigo = 99;
                    descripcion = "Error al guardar Documento";
                    return false;
                }

            }
            catch (Exception ex)
            {
                codigo = 99;
                descripcion = "Error: " + ex.Message;
                return false;
            }

            codigo = 0;
            return true;

        }



        public DataSet Devuelve_Planes(long rutPVR, long rutGPG)
        {
            DataSet ds;
            String sqlstr;

            sqlstr = "SP_BUSCAR_PLANES " + rutPVR + ", " + rutGPG;

            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;
        }



        #region OPERACIONES
        public bool Actualiza_Documento_x_Pago(string doc_num, string correlativo_pago,
                                               string RUT_GPG, string rut_prv, string TIP_DOC, string int_doc,
                                               string TAS_MOR, string monto_aplicar, string DIF_PRE_GPG, string DPG_TIP_DOC,
                                               string MTO_INT_DEV, DateTime FEC_LIB, string Mto_Pagado)
        {
            try
            {
                string Sql1;
                Sql1 = "";
                Sql1 = "EXEC sp_op_mod_docto ";
                Sql1 += doc_num + ",";
                Sql1 += correlativo_pago + ",";
                Sql1 += RUT_GPG + ",";
                Sql1 += rut_prv + ",";
                Sql1 += TIP_DOC + ",";
                Sql1 += int_doc + ",";
                Sql1 += TAS_MOR + ",";
                Sql1 += monto_aplicar + ",";
                Sql1 += DIF_PRE_GPG + ",";
                Sql1 += "'" + DPG_TIP_DOC + "'";
                Sql1 += MTO_INT_DEV + "'";
                Sql1 += "'" + rg.FechaJuliana(FEC_LIB) + "'";

                //sqlquery.ExecuteNonQuery(Sql1);
            }
            catch (Exception ex)
            {
                codigo = 99;
                descripcion = "Error: " + ex.Message;
                return false;
            }

            codigo = 0;
            return true;
        }


        public string Actualiza_Documento_x_Pago_Retorna_QUERY(string doc_num, string correlativo_pago,
                                               string RUT_GPG, string rut_prv, string TIP_DOC, string int_doc,
                                               string TAS_MOR, string monto_aplicar, string DIF_PRE_GPG, string DPG_TIP_DOC,
                                                string MTO_INT_DEV, DateTime FEC_LIB, string FACTOR_CAMBIO, string MONTO_PAGADO)
        {
            string Sql1 = "";
            try
            {
                Sql1 = "";
                Sql1 = "EXEC sp_op_mod_docto '";
                Sql1 += doc_num + "',";
                Sql1 += correlativo_pago + ",";
                Sql1 += RUT_GPG + ",";
                Sql1 += rut_prv + ",";
                Sql1 += TIP_DOC + ",";
                Sql1 += rg.ComasXpuntosDecimal(int_doc) + ",";
                Sql1 += TAS_MOR + ",";
                Sql1 += rg.ComasXpuntosDecimal(monto_aplicar) + ",";
                Sql1 += rg.ComasXpuntosDecimal(DIF_PRE_GPG) + ",";
                Sql1 += "'" + DPG_TIP_DOC + "',";
                Sql1 += rg.ComasXpuntosDecimal(MTO_INT_DEV) + ",";
                Sql1 += "'" + rg.FechaJuliana(FEC_LIB) + "',";
                Sql1 += rg.ComasXpuntosDecimal(FACTOR_CAMBIO) + ",";
                Sql1 += rg.ComasXpuntosDouble6(MONTO_PAGADO);

            }
            catch (Exception ex)
            {
                return "";
            }

            return Sql1;
        }


        public DataSet Devuelve_Documentos_Pagados(string Num_Pago)
        {
            DataSet ds;
            String sqlstr;
            sqlstr = "";

            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;
        }

        public DataSet Devuelve_Diferencia_Precio_Ggp_Doc(int num_ope)
        {
            DataSet ds;
            string sql_str;

            sql_str = "EXEC SP_OP_DEVUELVE_DIFER_PREC_GPG " + num_ope;

            ds = sqlquery.ExecuteDataSet(sql_str);
            return ds;
        }

        #endregion

        #region CALIFICACIONES

        public DataSet DevuelveCalificacionDocumento(long rutpagador, long rutproveedor, string nrodocto, int tipodocto)
        {
            DataSet ds;
            String sqlstr = "";

            sqlstr = "SELECT RTRIM(LTRIM(ISNULL(CAL_OTO_GAM,'0')))CAL_OTO_GAM, RTRIM(LTRIM(ISNULL(CAL_OBJ_ETI,'0')))CAL_OBJ_ETI, ";
            sqlstr += "RTRIM(LTRIM(ISNULL(CAL_SUB_JET,'0')))CAL_SUB_JET, RTRIM(LTRIM(ISNULL(CAL_ARR_AST,'0')))CAL_ARR_AST, ";
            sqlstr += "RTRIM(LTRIM(ISNULL(CAL_DEF_INI,'0')))CAL_DEF_INI, RTRIM(LTRIM(ISNULL(CAL_DEF_INI_OLD,'0')))CAL_DEF_INI_OLD, ";
            sqlstr += "RTRIM(LTRIM(ISNULL(CAL_DEF_INI_OLD_MTO,'0')))CAL_DEF_INI_OLD_MTO, RTRIM(LTRIM(ISNULL(CAL_DEF_INI_FIR,'0'))) CAL_DEF_INI_FIR " +
                     "FROM CLF WHERE CAL_RUT_GPG = " + rutpagador +
                               " AND CAL_RUT_PVR = " + rutproveedor +
                               " AND CAL_DOC_NUM = '" + nrodocto + "'" +
                               " AND CAL_TIP_DOC = " + tipodocto;

            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;

        }

        public DataSet Devuelve_calificacion_pvr(string rutproveedor)
        {
            DataSet ds;
            String sqlstr = "";

            sqlstr = "select pvr_cal_ope from pvr where pvr_rut = '" + rutproveedor + "'";

            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;

        }

        public DataSet GuardaCalificacionDocumento(long rutproveedor, long rutpagador, string nrodocto, string cal_sub, string cal_arr)
        {
            DataSet ds;
            String sqlstr = "";

            sqlstr = "update clf set " +
                     "cal_sub_jet = '" + cal_sub + "' " +
                //"cal_arr_ast = '" + cal_arr + "' " +
                     "where cal_rut_gpg = " + rutpagador +
                      " and cal_rut_pvr = " + rutproveedor +
                      " and cal_doc_num = '" + nrodocto + "'";

            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;

        }

        public bool InsertarCalificacionDocum_X_Operacion(string Num_Operacion, string calificacion)
        {
            String sqlstr = "";
            List<string> lista = new List<string>();


            sqlstr = "EXEC SP_OP_INGRESA_CALIF_OPERACION_DOCUMENTOS " + Num_Operacion + "," + calificacion;

            lista.Add(sqlstr);

            sqlquery.ExecuteTransaccion(lista);

            if (sqlquery.estadoconsulta.Equals("99"))
            {
                return false;
            }
            return true;
        }

        #endregion

        #region DOCUMENTOS DIGITALIZADOS

        public int GuardaDoctosDigitalizados(long nrooperacion, string descripcion, byte[] imgdata)
        {
            String sqlstr = "";

            sqlstr = "INSERT INTO doc_dig_ope VALUES(" + nrooperacion.ToString() + ", '" +
                                                         descripcion.Trim().ToUpper() + "', @@imagen)";

            return sqlquery.ExecuteNonQuery(sqlstr, imgdata);

        }

        public int EliminaDoctosDigitalizado(long nrooperacion, long nrodocto)
        {
            String sqlstr = "";

            sqlstr = "delete from doc_dig_ope where id_ope = " + nrooperacion.ToString() + " and doc_dig_id = " + nrodocto.ToString();

            return sqlquery.ExecuteNonQuery(sqlstr);

        }

        public Byte[] DevuelveDoctosDigitalizado(long nrooperacion, long nrodocto)
        {
            String sqlstr = "";

            Byte[] aBytDocumento = null;
            sqlstr = "select ISNULL(doc_dig_file,'') AS DOC_IMG_FILE " +
                     "from doc_dig_ope where id_ope = " + nrooperacion.ToString() +
                                   " and doc_dig_id = " + nrodocto.ToString();

            DataSet ds;

            ds = sqlquery.ExecuteDataSet(sqlstr);

            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
                {
                    aBytDocumento = (Byte[])ds.Tables[0].Rows[x]["DOC_IMG_FILE"];
                }
            }

            return aBytDocumento;

        }

        public DataSet DevuelveListadoDoctosDigitalizados(long nrooperacion)
        {
            DataSet ds;
            String sqlstr = "";

            sqlstr = "Select * from doc_dig_ope where id_ope = " + nrooperacion.ToString();

            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;

        }


        public bool Actualiza_DOC_DIF_PRE_GPG(string dif_Prec_Gpg, string num_doc, string rut_gpg, string rut_pvr, DateTime doc_fec_mor)
        {
            String sqlstr = "";
            try
            {
                sqlstr = "Update Doc Set doc_dif_pre_gpg =  " + dif_Prec_Gpg + ",doc_fec_mor =  '" + rg.FechaJuliana(doc_fec_mor) + "'  where gpg_rut = " + rut_gpg + "and prv_rut =" + rut_pvr + " and doc_num = '" + num_doc + "'";

                sqlquery.ExecuteNonQuery(sqlstr);
                if (sqlquery.estadoconsulta == 99)
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
            return true;
        }

        public string Actualiza_DOC_DIF_PRE_GPG_Text(string dif_Prec_Gpg, string num_doc, string rut_gpg, string rut_pvr, DateTime doc_fec_mor)
        {
            String sqlstr = "";
            try
            {
                sqlstr = "Update Doc Set doc_dif_pre_gpg =  " + dif_Prec_Gpg + ",doc_fec_mor =  '" + rg.FechaJuliana(doc_fec_mor) + "'  where gpg_rut = " + rut_gpg + "and prv_rut =" + rut_pvr + " and doc_num = '" + num_doc + "'";
            }
            catch
            {
                return "";
            }
            return sqlstr;
        }

        public bool Actualiza_DOC_FEC_MOR(DateTime doc_fec_mor, string num_doc, string rut_gpg, string rut_pvr)
        {
            String sqlstr = "";
            try
            {
                sqlstr = "Update Doc Set doc_fec_mor =  '" + rg.FechaJuliana(doc_fec_mor) + "' where gpg_rut = " + rut_gpg + "and prv_rut =" + rut_pvr + " and doc_num = '" + num_doc + "'";

                sqlquery.ExecuteNonQuery(sqlstr);
            }
            catch
            {
                return false;
            }
            return true;
        }

        public string Actualiza_DOC_FEC_MOR_Text(DateTime doc_fec_mor, string num_doc, string rut_gpg, string rut_pvr)
        {
            String sqlstr = "";
            try
            {
                sqlstr = "Update Doc Set doc_fec_mor =  '" + rg.FechaJuliana(doc_fec_mor) + "' where gpg_rut = " + rut_gpg + "and prv_rut =" + rut_pvr + " and doc_num = '" + num_doc + "'";
            }
            catch
            {
                return "";
            }
            return sqlstr;
        }

        #endregion


        /////// ------------------ OPCIONES  FINANCIACIÓN EN PROCESO ---------------------- /////////////

        // 07/05/2014 DEVUELVE DOCUMENTOS CARGADOS SEGUN CRITERIO DE BUSQUEDA (ESTADO 10 DEL DOCUMENTO)
        public DataSet DevuelveDocumentosCargados(long RUTGPG, long RUTPVR, string FECHA_DESDE, string FECHA_HASTA,
                                                  int nro_radicado, string COD_PLAN, string NUM_DOC, string usr)
        {
            DataSet ds;
            String sqlstr = "";

            sqlstr = " EXEC SP_FA_DEV_DOC_CAR " + RUTPVR + "," + RUTGPG + ",'" + FECHA_DESDE + "','" + FECHA_HASTA + "','";
            sqlstr += COD_PLAN + "','" + NUM_DOC + "'," + nro_radicado + ",'" + usr + "'";

            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;

        }


        // 07/05/2014 DEVUELVE DOCUMENTOS CARGADOS  POR DESEMBOLSAR SEGUN CRITERIO DE BUSQUEDA (ESTADO 11 DEL DOCUMENTO)
        public DataSet DevuelveDocumentosCargadosxDesembolsar(long RUTGPG, long RUTPVR, string FECHA_DESDE, string FECHA_HASTA,
                                                  int nro_radicado, string COD_PLAN, string NUM_DOC, string usr, string FEC_DESEMBOLSO_DESDE, string FEC_DESEMBOLSO_HASTA)
        {
            DataSet ds;
            String sqlstr = "";

            sqlstr = " EXEC SP_FA_DEV_DOC_CAR_X_DES " + RUTPVR + "," + RUTGPG + ",'" + FECHA_DESDE + "','" + FECHA_HASTA + "','";
            sqlstr += COD_PLAN + "','" + NUM_DOC + "'," + nro_radicado + ",'" + usr + "','" + FEC_DESEMBOLSO_DESDE + "','" + FEC_DESEMBOLSO_HASTA + "'";

            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;

        }

        public DataSet DevuelveDocumentosCargadosxDesembolsarTotales(long RUTGPG, long RUTPVR, string FECHA_DESDE, string FECHA_HASTA,
                                          int nro_radicado, string COD_PLAN, string NUM_DOC, string usr, string FEC_DESEMBOLSO_DESDE, string FEC_DESEMBOLSO_HASTA)
        {
            DataSet ds;
            String sqlstr = "";

            sqlstr = " EXEC SP_FA_DEV_DOC_CAR_X_DES_TOTALES " + RUTPVR + "," + RUTGPG + ",'" + FECHA_DESDE + "','" + FECHA_HASTA + "','";
            sqlstr += COD_PLAN + "','" + NUM_DOC + "'," + nro_radicado + ",'" + usr + "','" + FEC_DESEMBOLSO_DESDE + "','" + FEC_DESEMBOLSO_HASTA + "'";

            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;

        }


        // 08/05/2014 DEVUELVE DOCUMENTOS QUE PUEDEN SER MODIFICADOS
        public DataSet DevuelveDocumentosXModificar(long RUTGPG, long RUTPVR, string FECHA_DESDE, string FECHA_HASTA,
                                                  int nro_radicado, string COD_PLAN, string NUM_DOC, string usr)
        {
            DataSet ds;
            String sqlstr = "";

            sqlstr = " EXEC SP_FA_DEV_DOC_X_MOD " + RUTPVR + "," + RUTGPG + ",'" + FECHA_DESDE + "','" + FECHA_HASTA + "','";
            sqlstr += COD_PLAN + "','" + NUM_DOC + "'," + nro_radicado + ",'" + usr + "'";

            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;

        }

        // devuelve monto total y cantidad de documentos segun parametros ingresados
        public DataSet DevuelveDocumentosXModificarTotales(long RUTGPG, long RUTPVR, string FECHA_DESDE, string FECHA_HASTA,
                                          int nro_radicado, string COD_PLAN, string NUM_DOC, string usr, int opcion)
        {
            DataSet ds;
            String sqlstr = "";

            sqlstr = " EXEC SP_FA_DEV_TOTALES " + RUTPVR + "," + RUTGPG + ",'" + FECHA_DESDE + "','" + FECHA_HASTA + "','";
            sqlstr += COD_PLAN + "','" + NUM_DOC + "'," + nro_radicado + ",'" + usr + "'," + opcion;

            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;

        }


        // 08/05/2014 DEVUELVE DOCUMENTOS CARGADOS QUE FUERON MODIFICADOS
        public DataSet DevuelveDocumentosModificados(long RUTGPG, long RUTPVR, string FECHA_DESDE, string FECHA_HASTA,
                                                     int nro_radicado, string COD_PLAN, string NUM_DOC, string usr)
        {
            DataSet ds;
            String sqlstr = "";

            sqlstr = " EXEC SP_FA_DEV_DOC_CAR_MOD " + RUTPVR + "," + RUTGPG + ",'" + FECHA_DESDE + "','" + FECHA_HASTA + "','";
            sqlstr += COD_PLAN + "','" + NUM_DOC + "'," + nro_radicado + ",'" + usr + "'";

            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;

        }


        // 31/03/2014 DEVUELVE DETALLE  DEL PLAN SELECCIONADO
        public DataSet DevuelveDetallePlan(string COD_PLN, long RUT_PVR)
        {
            DataSet ds;
            String sqlstr = "";

            sqlstr = " EXEC SP_FA_DEV_DETALLE_PLAN '" + COD_PLN + "'," + RUT_PVR;

            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;

        }

        // 31/03/2014 DEVUELVE MONTO DIPONIBLE entre Cliente - Proveedor
        public double Dev_Monto_Disponible(string RUT_PVR, string RUT_GPG, int TIP_MON)
        {
            DataSet ds;
            String sqlstr = "";

            sqlstr = " EXEC SP_DEV_MTO_DIS_LIN_FIN_X_UNO '" + RUT_PVR + "','" + RUT_GPG + "'," + TIP_MON;

            ds = sqlquery.ExecuteDataSet(sqlstr);

            return double.Parse(ds.Tables[0].Rows[0]["MONTO_DIPONIBLE"].ToString());

        }

        // 31/03/2014 DEVUELVE MONTO DIPONIBLE entre Cliente - Proveedor
        public double Estado_Linea(string RUT_PVR, string RUT_GPG, int TIP_MON)
        {
            DataSet ds;
            String sqlstr = "";

            sqlstr = " EXEC SP_DEV_MTO_DIS_LIN_FIN_X_UNO '" + RUT_PVR + "','" + RUT_GPG + "'," + TIP_MON;

            ds = sqlquery.ExecuteDataSet(sqlstr);

            return double.Parse(ds.Tables[0].Rows[0]["MONTO_DIPONIBLE"].ToString());

        }
        public bool Transaccion_Documentos(List<string> lista_Query)
        {
            if (sqlquery.ExecuteTransaccion(lista_Query))
                return true;
            else
                return false;
        }

        public DateTime dev_fec_sgte_habil()
        {
            string sqlstr = "";
            DateTime FEC;
            DataSet ds;
            sqlstr = " (SELECT DBO.DEVUELDE_DIA_HABIL(DATEADD(DAY,1,GETDATE())))";
            ds = sqlquery.ExecuteDataSet(sqlstr);

            FEC = DateTime.Parse(ds.Tables[0].Rows[0][0].ToString());
            return FEC;
        }

        public DataSet Devuelve_Dscto_X_Documento(long RUT_PVR, long RUT_GPG, int ID_PLAN, DateTime FECHA_DES, int MONEDA, string DOC_NUM, double Monto)
        {
            string sqlstr = "";
            DataSet ds;
            sqlstr = "SP_FA_DEV_DESCTO_X_DOC " + RUT_PVR + "," + RUT_GPG + "," + ID_PLAN + ",'" + rg.FechaJuliana(FECHA_DES) + "'," + MONEDA + ",'" + DOC_NUM + "'," + Monto;
            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;
        }

        public bool Actualiza_Doc_Modificado(long RUT_PVR, long RUT_GPG, DateTime FECHA_DES, int MONEDA, string DOC_NUM, double Monto, int MODIFICACION)
        {
            try
            {
                string sqlstr = "";
                sqlstr = "SP_FA_ACTUALIZA_DOC_MOD " + RUT_PVR + "," + RUT_GPG + ",'" + rg.FechaJuliana(FECHA_DES) + "'," + MONEDA + ",'" + DOC_NUM + "'," + Monto + "," + MODIFICACION;

                if (sqlquery.ExecuteNonQuery(sqlstr) > 0)
                    return true;
                else
                    return false;

            }
            catch (Exception ex)
            {
                codigo = 99;
                descripcion = "Error: " + ex.Message;
                return false;
            }
        }

        public bool Actualiza_Lin_Global(long rut_gpg)
        {
            String sqlstr = "";
            try
            {
                sqlstr = "EXEC BBVA_FACTOR..SP_LINEAGLOBAL_SINCRONIZACION '" + rut_gpg + "'";

                if (sqlquery.ExecuteNonQuery(sqlstr) > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public string Actualiza_Linea_Finan_GPG_String(string rut_Gpg)
        {

            string sql = "";
            sql = "EXEC BBVA_FACTOR..SP_LINEAGLOBAL_SINCRONIZACION '" + rut_Gpg + "'";
            return sql;
        }

        public DataSet DEVUELVE_DIA_PAGO(int ID_PLAN, DateTime FECHA_DES)
        {
            string sqlstr = "";
            DataSet ds;
            sqlstr = "SET LANGUAGE 'español' ";
            sqlstr += "EXEC SP_MA_DEV_DIA_PAGO " + ID_PLAN + ", '" + rg.FechaJuliana(FECHA_DES) + "'";
            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;
        }

        //funcion que inserta documentos en la tabla temp_doc_eliminar, para su posterior eliminacion
        public void InsertaDocumentos_Eliminar(long rutproveedor, long rutpagador, DateTime fecha, int mon, string doc_num, string id_doc, string usuario, double monto)
        {

            String sqlstr = "";
            String Fecdia = rg.FechaJuliana(DateTime.Now.ToString());

            sqlstr = "EXEC SP_OP_INSERTA_DOCUMENTOS_PARA_ELIMINAR " + rutproveedor + ", " +
                                                          rutpagador + ", " + "'" +
                                                          rg.FechaJuliana(fecha) + "'," +
                                                          mon + ", '" +
                                                          doc_num + "','" +
                                                          id_doc + "','" +
                                                          usuario + "', " +
                                                          monto;

            sqlquery.ExecuteNonQuery(sqlstr);

        }

        //funcion que elimina los documentos en la tabla [TEMP_DOC_ELIMINAR]
        public void BorraDocumentos_Eliminar(long rutpvr, long rutgpg, DateTime fecha, string mon, string num_doc, string doc_int, string usuario)
        {
            String sqlstr = "";


            sqlstr = "EXEC SP_OP_ELIMINA_DOCUMENTOS_PARA_ELIMINAR " + rutpvr + ", " +
                                                          rutgpg + ", " + "'" +
                                                          rg.FechaJuliana(fecha) + "'," +
                                                          mon + ", '" +
                                                          num_doc + "','" +
                                                          doc_int + "','" +
                                                          usuario + "'"
                                                          ;

            sqlquery.ExecuteNonQuery(sqlstr);

        }

        //elimina los documentos de la tabla temp_doc_eliminar, que esten asociados al usuario conectado
        public void BorraDocumentosParaEliminar(string usr)
        {
            String sqlstr = "";

            sqlstr = " DELETE TEMP_DOC_ELIMINAR WHERE usuario = '" + usr + "'";
            sqlstr += " AND  doc_est_int IN (10,1,4) ";

            sqlquery.ExecuteNonQuery(sqlstr);


        }


        //funcion que elimina los documentos, de las tablas temp_doc_eliminar,DOC Y INSERTA EN LA TABLA DEL
        public bool EliminaDocumentos_temp_doc_eliminar(string usr, int core)
        {
            String sqlstr = "";

            sqlstr = "EXEC SP_OP_ELIMINA_DOCUMENTOS_TEMP_DOC_ELIMINAR '" + usr + "', " + core;

            sqlquery.ExecuteNonQuery(sqlstr);

            if (sqlquery.estadoconsulta == 99)
                return false;
            //----->
            return true;

        }

        // cuenta la cantidad de datos en la tabla temp_doc_eliminar
        public DataSet Cuenta_Documentos_Eliminar(string usr)
        {
            DataSet ds;
            string sqlstr = "";

            sqlstr = " select COUNT(1) as CANT_DOC_SELEC from TEMP_DOC_ELIMINAR where usuario = '" + usr + "'";
            sqlquery.ExecuteNonQuery(sqlstr);

            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;

        }


        // devuelve datos plan segun documento ingresado
        public DataSet devuelve_datos_plan(int Id_plan)
        {
            DataSet ds;
            string sqlstr = "";

            sqlstr = "EXEC SP_MA_DEV_DATOS_PLAN " + Id_plan;
            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;

        }





        public DataSet Dev_Cliente_Temp_Doc_Eliminar(string usr)
        {
            DataSet ds;
            string sqlstr = "";

            sqlstr = " SELECT GPG_RUT FROM TEMP_DOC_ELIMINAR WHERE USUARIO = '" + usr + "' GROUP BY GPG_RUT";
            sqlquery.ExecuteNonQuery(sqlstr);

            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;

        }

        //MODIFICADO PARA PAGO CON RENDIMIENTO OPTIMO
        public string Actualiza_Documento_x_Pago_Retorna_QUERY_MODIFICADO(string correlativo_pago,
                                 string TAS_MOR, DateTime FEC_LIB, string FACTOR_CAMBIO, string usr, DateTime fec_pag )
        {
            string Sql1 = "";
            try
            {
                Sql1 = "";
                Sql1 = "EXEC  SP_OP_MOD_DOCTO_MODIFICADO_JUEVES_15 ";
                Sql1 += correlativo_pago + ",";
                Sql1 += TAS_MOR + ",";
                Sql1 += "'" + rg.FechaJuliana(FEC_LIB) + "',";
                Sql1 += rg.ComasXpuntosDecimal(FACTOR_CAMBIO) + ",";
                Sql1 += "'" + usr + "',";
                Sql1 += "'" + rg.FechaJuliana(fec_pag) + "'";

            }
            catch (Exception ex)
            {
                return "";
            }

            return Sql1;
        }


        // VALIDA QUE EL DOCUMENTO QUEDE DENTRO DEL PLAN SEGUN PLAZO DE FACT. DEL PROVEEDOR 
        // PUEDE SER UNA CANTIDAD DEFINIDA (SIN PLAZO VARIABLE) O UN RANGO DE DIAS(PLAZO VARIABLE) 
        public bool ValidaDiasDoc_VS_DiasPlan(int id_plan, int dias)
        {

            int result = 0;
            string sqlstr = "";
            DataSet ds;

            sqlstr = "SP_MA_VALIDA_DIAS_PLAN " + id_plan + ", " + dias;
            ds = sqlquery.ExecuteDataSet(sqlstr);

            if (ds.Tables[0].Rows.Count > 0)
            {
                result = int.Parse(ds.Tables[0].Rows[0]["VALIDA"].ToString());

                if (result == 1)
                    return true;
                else
                    return false;
            }
            return false;
        }


        public string Dev_plazo_plan(int id_plan)
        {
            DataSet ds;
            string sqlstr = "";
            string plazo_min, plazo_pvr;

            sqlstr = " SELECT PLN_DDF_PLAZO_VAR AS PLAZO,PLN_DDF_PLZ_FAC_PVR AS DIAS_PVR,";
            sqlstr += " PLN_DDF_PLAZO_MIN_FACT DIAS_DESDE FROM PLN_AUX WHERE ID_PLAN = " + id_plan;
            ds = sqlquery.ExecuteDataSet(sqlstr);

            if (ds.Tables[0].Rows.Count > 0)
            {
                if (int.Parse(ds.Tables[0].Rows[0]["PLAZO"].ToString()) == 2)
                {
                    plazo_pvr = ds.Tables[0].Rows[0]["DIAS_PVR"].ToString();
                    return plazo_pvr;
                }
                else
                {
                    plazo_min = ds.Tables[0].Rows[0]["DIAS_DESDE"].ToString();
                    plazo_pvr = ds.Tables[0].Rows[0]["DIAS_PVR"].ToString();
                    return plazo_min + " - " + plazo_pvr;
                }
            }
            return "";
        }


        public bool GuardaNominasPvr(long rutpvr, string digito, string fechaingreso, string nombre,
                              int nrolineas, int estadonomina, double montonomina, double Moneda,
                              string nombrearchivo, string ordencad, string ordencol, string usr)
        {
            try
            {
                String sqlstr = "";
                int codigo = 0;
                DataSet ds;


                sqlstr = "EXEC SP_WEB_INSERTA_NOMINA " + rutpvr + ", '" + digito + "', '" + fechaingreso + "', '" + nombre + "', "
                                                       + nrolineas + ", " + estadonomina + ", " + montonomina + ", " + Moneda + ", '"
                                                       + nombrearchivo + "', '" + ordencad + "', '" + ordencol + "', '" + usr + "'";


                ds = sqlquery.ExecuteDataSet(sqlstr);
                num_nomina = int.Parse(ds.Tables[0].Rows[0]["SIS_NUM_NMA"].ToString());
                return true;

            }
            catch (Exception e)
            {
                descripcion = "Error: " + e.Message;
                return false;
            }

        }



        // VALIDA Y CAMBIO DE ESTADO LAS LINEAS QUE YA ESTAN PASADA SU FECHA DE VENCIMIENTO
        public bool Valida_Vencimientos_Lineas()
        {
            string sqlstr = "";
            int ds;

            sqlstr = "EXEC SP_MAN_VAL_LIN_VEN";

            ds = sqlquery.ExecuteNonQuery(sqlstr);

            if (ds > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }



        //VALIDA QUE LA LINEA NO ESTE VENCIDA
        public bool Linea_Vencida(long rut_cli)
        {
            string sqlstr = "";
            DataSet ds;

            sqlstr = "SELECT ID_LDC,CLI_IDC,ID_P_0029 AS ESTADO FROM BBVA_LINEA_GLOBAL..LDC ";
            sqlstr += "WHERE id_P_0029 = 1 AND CONVERT(NUMERIC(12),CLI_IDC) = " + rut_cli;

            ds = sqlquery.ExecuteDataSet(sqlstr);

            if (ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }



        public bool valida_cliente_mora(long rut_cli)
        {
            string sqlstr = "";
            DataSet ds;

            sqlstr = "SELECT COUNT(DOC_NUM)AS DOC_MORA FROM DOC WHERE DOC_FEC_APAG < CONVERT(DATE,GETDATE(),112) ";
            sqlstr += "AND GPG_RUT = " + rut_cli + " AND par_est in (1,2)";

            ds = sqlquery.ExecuteDataSet(sqlstr);

            if (int.Parse(ds.Tables[0].Rows[0]["DOC_MORA"].ToString()) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
//*************************************************************************************************

    public static class ProductsSelectionManager
    {

        public static void KeepSelection(GridView grid)
        {
            //
            // se obtienen los id de producto checkeados de la pagina actual
            //
            List<int> checkedProd = (from item in grid.Rows.Cast<GridViewRow>()
                                     let check = (CheckBox)item.FindControl("CKB_RUT_PROVEEDOR")
                                     where check.Checked
                                     select Convert.ToInt32(grid.DataKeys[item.RowIndex].Value)).ToList();

            //
            // se recupera de session la lista de seleccionados previamente
            //
            List<int> productsIdSel = HttpContext.Current.Session["ProdSelection"] as List<int>;

            if (productsIdSel == null)
                productsIdSel = new List<int>();

            //
            // se cruzan todos los registros de la pagina actual del gridview con la lista de seleccionados,
            // si algun item de esa pagina fue marcado previamente no se devuelve
            //
            productsIdSel = (from item in productsIdSel
                             join item2 in grid.Rows.Cast<GridViewRow>()
                                on item equals Convert.ToInt32(grid.DataKeys[item2.RowIndex].Value) into g
                             where !g.Any()
                             select item).ToList();

            //
            // se agregan los seleccionados
            //
            productsIdSel.AddRange(checkedProd);

            HttpContext.Current.Session["ProdSelection"] = productsIdSel;

        }

        public static void RestoreSelection(GridView grid)
        {

            List<int> productsIdSel = HttpContext.Current.Session["ProdSelection"] as List<int>;

            if (productsIdSel == null)
                return;

            //
            // se comparan los registros de la pagina del grid con los recuperados de la Session
            // los coincidentes se devuelven para ser seleccionados
            //
            List<GridViewRow> result = (from item in grid.Rows.Cast<GridViewRow>()
                                        join item2 in productsIdSel
                                        on Convert.ToInt32(grid.DataKeys[item.RowIndex].Value) equals item2 into g
                                        where g.Any()
                                        select item).ToList();

            //
            // se recorre cada item para marcarlo
            //
            result.ForEach(x => ((CheckBox)x.FindControl("CKB_RUT_PROVEEDOR")).Checked = true);

        }


    }

     




 }
