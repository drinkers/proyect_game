﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using dim.rutinas;
using System.Data;
using System.Data.SqlClient;

namespace dim.datos
{
    public class ClsLineaDeCredito
    {

        SqlQuery sqlquery = new SqlQuery();
        RutinasGenerales rg = new RutinasGenerales();

        public int codigo { get; set; }
        public string descripcion { get; set; }

        public DataSet DevuelveSFM(long rut_gpg, int mes, int año, long lcr_num, int cod_moneda) //SH -20120410
        {

            string sqlstr;
            DataSet ds;

            sqlstr = "SP_GE_DEVUELVE_SALDO_FIN_MES " + rut_gpg + ", "
                                                     + año + ", "
                                                     + mes + ", "
                                                     + lcr_num  + ","
                                                     + cod_moneda  ;

            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;

        }

        public DataSet DevuelveLineaDeCreditoGPG(long  rut_gpg,string usr) //SH -20120410
        {

            String sqlstr = "";
            DataSet ds;
            
            sqlstr = "EXEC SP_GE_DEVUELVE_LCR_GPG " + rut_gpg+",'"
                                                    + usr+"'";
       
            ds = sqlquery.ExecuteDataSet(sqlstr);
            
            return ds;

        }
        
        public DataSet DevuelveLineaDeCredito(long rutpagador,int mon)
        {
            DataSet ds;
            String sqlstr = "";

            sqlstr = "EXEC SP_MA_BUSCA_LIN_CRE " + rutpagador + "," +
                                                  mon;                                                                 
                 
            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;

        }

        public DataSet DevuelveLineaDeCredito()
        {
            DataSet ds;
            String sqlstr = "";

            sqlstr = "EXEC SP_CO_DEVUELVE_LINEA ";

            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;

        }


        public bool GuardaLinea(long rutpagador, 
                                int nrolinea, int idestado, int idmoneda, double mtoaprobado, double mtodisponible, DateTime fecvtolin, decimal comision, string obslinea,
                                int nrocontrato, int idtipo, DateTime feccontrato, DateTime fecvtocon, string obscon, string Porc_Exceso , string Estado_Linea, string usr)
        {
            try
            {
                String sqlstr = "";

                sqlstr = "";
                if (nrolinea <= 0)
                {

                    sqlstr = "EXEC sp_ma_inserta_linea_cre " + rutpagador + ", " 
                                                             + nrolinea + ", "
                                                             + rg.ComasXpuntosMontos(mtoaprobado.ToString()) + ", "
                                                             + rg.ComasXpuntosMontos(mtodisponible.ToString()) + ", "
                                                             + rg.ComasXpuntosMontos(comision.ToString()) + ", " 
                                                             + idestado + ", " 
                                                             + nrocontrato + ", '"
                                                             + rg.FechaJuliana(feccontrato) + "', " 
                                                             + idtipo + ", '"
                                                             + rg.FechaJuliana(fecvtocon) + "', '" 
                                                             + obscon + "', '" 
                                                             + obslinea + "', '"
                                                             + rg.FechaJuliana(fecvtolin) + "', "
                                                             + idmoneda + ",'"
                                                             + usr + "',"
                                                             + Porc_Exceso
                                                              ;
                }
                else
                {
                    if (!Estado_Linea.Equals("3"))
                    {   // modifica la linea de financiamiento
                        sqlstr = "EXEC sp_ma_update_linea_cre " + rutpagador + ", ";
                    }
                    else
                    {   // cuando modifican el monto..o la activan.
                        sqlstr = "EXEC SP_MA_SOLIC_MODIF_LINEA_CRE " + rutpagador + ", ";
                    }

                    sqlstr +=                                nrolinea + ", "
                                                            + rg.ComasXpuntosMontos(mtoaprobado.ToString()) + ", "
                                                            + rg.ComasXpuntosMontos(mtodisponible.ToString()) + ", "
                                                            + rg.ComasXpuntosDecimal(comision.ToString()) + ", "
                                                            + idestado + ", "
                                                            + nrocontrato + ", '"
                                                            + rg.FechaJuliana(feccontrato) + "', "
                                                            + idtipo + ", '"
                                                            + rg.FechaJuliana(fecvtocon) + "', '"
                                                            + obscon + "', '"
                                                            + obslinea + "', '"
                                                            + rg.FechaJuliana(fecvtolin) + "', "
                                                            + idmoneda + ","
                                                            + Porc_Exceso + ",'"
                                                            + usr + "'"
                                                            ;

                }

                sqlquery.ExecuteNonQuery(sqlstr);

            }
            catch (Exception e)
            {
                codigo = 99;
                descripcion = "Error: " + e.Message;
                return false;
            }

            codigo = 0;
            descripcion = "Guardado";

            return true;

        }

        public bool ValidaLineaDeCredito(long  rutpagador, int idmoneda, int nrolinea)
        {
            DataSet ds;
            String sqlstr = "";
             //SQL = "EXEC SP_CO_DEVUELVE_LINEA_X_GPG " & gpg_rut                                
            sqlstr = "EXEC SP_MA_VALIDA_LINEA_CRE " + rutpagador + ", " + idmoneda;


            ds = sqlquery.ExecuteDataSet(sqlstr);

            //Traemos las lineas vigentes de la moneda
            for (int i = 0; i <= ds.Tables[0].Rows.Count - 1;i++)
            {
                //si la linea que modifica es vigente lo devuelve para grabar
                if (nrolinea.ToString() == ds.Tables[0].Rows[i][0].ToString())
                    return true;
                else
                    return false;
            }

            return true;
            
        }

        public int ValidaLineaDeCreditoGPG(long rutpagador, int idmoneda, int tipodocto,
                                            double MTO_SIM, DateTime fechasimulacion, string Sobre_Giro)
        {
            ClsDocumentos clsdoc = new ClsDocumentos();
            DataSet ds;
            String sqlstr = "";

            double MTO_MOR = 0;
            double MTO_DIS = 0;

            sqlstr = "EXEC SP_CO_DEVUELVE_LINEA_X_GPG  " + rutpagador + "," + idmoneda;

            ds = sqlquery.ExecuteDataSet(sqlstr);
            
            for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
            {
                MTO_MOR = clsdoc.DevuelveDocumentosMorosos(rutpagador); //Monto doctos morosos
                MTO_DIS = double.Parse(ds.Tables[0].Rows[0]["ANT_MON_DIS"].ToString());

                //Valida monto disponible de linea
                if (Sobre_Giro.Equals("N") & MTO_DIS < MTO_SIM)
                {
                    return 1;
                }                              

                //Valida fecha vecto linea
                if (DateTime.Parse(ds.Tables[0].Rows[0]["LCR_FVI_HAS"].ToString()) < fechasimulacion)
                    return 2;

                //Valida estado linea
                if (ds.Tables[0].Rows[0]["ANT_EST_INI"].ToString() != "3")
                    return 3;

                //Valida monto documnetos morosos
                if(MTO_MOR > 0)
                    return 4;
            }

            return 0;

        }

        public DataSet DevuelveLineaDeCreditoVigentes(long rutpagador)
        {
            DataSet ds;
            String sqlstr = "";

            sqlstr = "EXEC SP_MA_BUSCA_LIN_CRE_VIG " + rutpagador + "";

            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;

        }

        public DataSet DevuelveActasLineaDeCreditoVigentes(long  rutpagador, int num_Linea, int Cod_Moneda)
        {
            DataSet ds;
            String sqlstr = "";

            sqlstr = "Exec SP_MA_DEVUELVE_ACTA_LIN_FINAN " + rutpagador + "," + num_Linea+ "," +Cod_Moneda ;

            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;


        }

        public bool Guardar_Acta_IMG(string rut, string num_lin, string tip_mon, string descrip, Byte[] imagen)
        {
            String sqlstr = "";
            try
            {
                sqlstr = "Insert into ACT_LIN_IMG Values(" + rut + "," + num_lin + "," + tip_mon + ",'" + descrip + "',@@imagen)";
                sqlquery.ExecuteNonQuery(sqlstr,imagen);
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public bool Eliminar_Actas_IMG(string id_imagen)
        {
            String sqlstr = "";
            try
            {
                sqlstr = "Delete ACT_LIN_IMG where act_lin_id=" + id_imagen;
                sqlquery.ExecuteNonQuery(sqlstr);
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public Byte[] Devuelve_Actas_IMG_Byte(string id_imagen)
        {
            Byte[] aBytDocumento=null;
            String Sql_Str = "SELECT ISNULL(ACT_IMG_FILE,'') AS ACT_IMG_FILE FROM ACT_LIN_IMG WHERE ACT_LIN_ID =" + id_imagen;
            DataSet ds;

            ds = sqlquery.ExecuteDataSet(Sql_Str);

            if (ds.Tables[0].Rows.Count > 0)
            {
                for(int x = 0 ; x < ds.Tables[0].Rows.Count;x++){
                    aBytDocumento = (Byte[])ds.Tables[0].Rows[x]["ACT_IMG_FILE"];
                }
            }
            
            return aBytDocumento;
        }

        public bool Habilita_Linea_Financ(string rut, string num_lin, string tip_moneda, DateTime Fecha, string Usr)
        {
            List<String> lista = new List<string>();
            String sqlstr = "";

            try
            {
                sqlstr = "Exec SP_MA_HABILITA_LINEA_FINAN_GPG " + rut + "," + num_lin + "," + tip_moneda + ",'" + rg.FechaJuliana(Fecha) + "','"+ Usr+ "'";

                lista.Add(sqlstr);

                if (!sqlquery.ExecuteTransaccion(lista))
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public DataSet Devuelve_HISTO_LineaDeCredito(string Rut, string Num_lim)
        {
            DataSet ds;
            String sqlstr = "";

            sqlstr = "EXEC SP_MA_DEVUELVE_LINEA_HISTO_GPG " + Rut +","+ Num_lim;

            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;

        }


        /// <summary>
        /// cambiar linea por moneda
        /// </summary>
        /// <param name="rutpagador"></param>
        /// <param name="linea"></param>
        /// <returns></returns>
        public string devuelve_fecha_vencimiento_linea(long rutpagador, int moneda)
        {

            DataSet ds;
            String sqlstr = "";
            String lineavenc = "";

            try
            {

                sqlstr = "EXEC SP_MA_BUSCA_LINEA_CREDITO_PAGADOR " + rutpagador + "," + moneda;


                ds = sqlquery.ExecuteDataSet(sqlstr);

                if (ds.Tables[0].Rows.Count > 0)
                    lineavenc = ds.Tables[0].Rows[0]["fecha"].ToString().Trim();
                
                return lineavenc;
            }
            catch (Exception e)
            {
                return "";
            }
        
        
        
        }



    }

}
