﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using dim.rutinas;
using System.Data;
using System.Data.SqlClient;

namespace dim.datos
{
    public class ClsPagador
    {
        public int codigo { get; set; }
        public string descripcion { get; set; }

        SqlQuery sqlquery = new SqlQuery();
        RutinasGenerales rg = new RutinasGenerales();

        public DataSet DevuelvePagador(long  rutproveedor)
        {
            DataSet ds;
            String sqlstr = "";

            sqlstr = " SELECT DISTINCT G.GPG_RUT AS RUT_GPG, ";
            sqlstr += "       (SELECT ISNULL(G.GPG_RSN_SOC,'') + ' ' + ISNULL(G.GPG_APE_PTN,'') + ' ' + ISNULL(G.GPG_APE_MTN,'')) AS NOM_GPG ";
            sqlstr += " FROM GPG G, DOC D ";
            sqlstr += " WHERE D.gpg_rut = G.gpg_rut ";
            sqlstr += " AND G.gpg_ope_int_son = 'S'";
            sqlstr += " AND D.prv_rut = " + rutproveedor + " ";
            sqlstr += " AND D.par_tip_doc = 1 ";

            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;

        }

        public String DevuelveNombrePagador(long rutpagador)
        {
            DataSet ds;
            String sqlstr = "";
            String pagador = "";

            try
            {
                sqlstr = " SELECT G.GPG_RUT AS RUT_GPG, ";
                sqlstr += "       (ISNULL(G.GPG_RSN_SOC,'') + ' ' + ISNULL(G.GPG_APE_PTN,'') + ' ' + ISNULL(G.GPG_APE_MTN,'')) AS NOM_GPG ";
                sqlstr += " FROM GPG G";
                sqlstr += " WHERE gpg_rut = " + rutpagador.ToString();

                ds = sqlquery.ExecuteDataSet(sqlstr);

                if (ds.Tables[0].Rows.Count > 0)
                    pagador = ds.Tables[0].Rows[0]["NOM_GPG"].ToString().Trim();

                return pagador;

            }
            catch (Exception e)
            {
                return "";
            }

        }

        public DataSet AyudaPadagor(long rutayuda, string razonayuda)
        {
            DataSet ds;
            String sqlstr = "";

            try
            {
                sqlstr = "SELECT gpg_rut as 'RUT', (isnull(gpg_rsn_soc,'') + ' ' +  isnull(gpg_ape_ptn,'') + ' ' + isnull(gpg_ape_mtn,'')) as 'RAZON' FROM GPG ";
                //Busqueda X RUT
                if (rutayuda != 0)
                    sqlstr = sqlstr + "WHERE gpg_rut LIKE '" + rutayuda + "%' ";

                //Busqueda X Razón Social
                //if (!sqlstr.Contains("WHERE"))
                //{
                    if (razonayuda != "")
                        if (!sqlstr.Contains("WHERE"))
                        {
                            sqlstr = sqlstr + "WHERE gpg_rsn_soc LIKE '" + razonayuda.Replace("'", "''") + "%' ";
                        }
                        else
                        {
                            sqlstr = sqlstr + "AND gpg_rsn_soc LIKE '" + razonayuda.Replace("'", "''") + "%' ";
                        }
                    //else
                       // if (razonayuda != "")   
               // }

                sqlstr = sqlstr + " ORDER BY gpg_rsn_soc";

                ds = sqlquery.ExecuteDataSet(sqlstr);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    codigo = 1;
                    descripcion = "Se encontraron registros";
                }
                else
                {
                    codigo = 0;
                    descripcion = "No se encontraron registros";
                }

                return ds;

            }
            catch (Exception e)
            {
                return null;
            }

        }
        
        public DataSet DevuelvePagadorMantencion(long rutpagador)
        {
            DataSet ds;
            String sqlstr = "";

            try
            {
                sqlstr = "EXEC SP_MA_DEVUELVE_GPG_FACOPE " + rutpagador;
               
                ds = sqlquery.ExecuteDataSet(sqlstr);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    codigo = 1;
                    descripcion = "Se encontraron registros";
                }
                else
                {
                    codigo = 0;
                    descripcion = "No se encontraron registros";
                }

                return ds;

            }
            catch (Exception e)
            {
                return null;
            }

        }

        public bool GuardaPagador(long rutpagador, string digito, int idtipo, string razon, string paterno, string materno,
                                   int idsexo, string domicilio, int idcomuna, string idgiro, int idestado, int idejecutivo, int idactividad,
                                   int idciudad, string mail, string cuentacorriente, string accesointernet, string accion,
                                   string idsucursal, string id_N_Cliente, string Repre_Legal, string cod_corasu,
                                   string tipo_cuenta_banco, string Tip_Anio_Nego,int Tip_IDE, string ejec_oficina)
        {
            try
            {
                String sqlstr = "";

                sqlstr = "";

                if (accion.Trim().ToUpper() == "NUEVO")
                {

                    sqlstr = "EXEC SP_MA_INSERTA_PAGADOR " + rutpagador + ", '"
                                                           + digito + "', "
                                                           + idtipo + ", '"
                                                           + razon + "', '"
                                                           + paterno + "', '"
                                                           + materno + "', "
                                                           + idsexo + ", '"
                                                           + domicilio + "', "
                                                           + idcomuna + ", '"
                                                           + idgiro + "', "
                                                           + idestado + ", "
                                                           + idejecutivo + ", "
                                                           + idactividad + ", "
                                                           + idciudad + ", '"
                                                           + mail + "', '"
                                                           //+ idcomins + ", '"
                                                           + cuentacorriente + "', '"
                                                           + accesointernet + "',"
                                                           + idsucursal + ",'"
                                                           + id_N_Cliente + "','"
                                                           + Repre_Legal + "',"
                                                           + cod_corasu + ","
                                                           + tipo_cuenta_banco + ", '"
													       + Tip_Anio_Nego + "',"
                                                           + Tip_IDE + ",'" 
                                                           + ejec_oficina + "'";

                    descripcion = "Pagador guardado exitosamente";
                }
                else
                {
                    sqlstr = "EXEC SP_MA_MODIFICA_PAGADOR " + rutpagador + ", '"
                                                          + digito + "', "
                                                          + idtipo + ", '"
                                                          + razon + "', '"
                                                          + paterno + "', '"
                                                          + materno + "', "
                                                          + idsexo + ", '"
                                                          + domicilio + "', "
                                                          + idcomuna + ", '"
                                                          + idgiro + "', "
                                                          + idestado + ", "
                                                          + idejecutivo + ", "
                                                          + idactividad + ", "
                                                          + idciudad + ", '"
                                                          + mail + "', '"
                                                          //+ idcomins + ", '"
                                                          + cuentacorriente + "', '"
                                                          + accesointernet + "',"
                                                          + idsucursal + ",'"
                                                          + id_N_Cliente + "','"
                                                          + Repre_Legal + "',"
                                                          + cod_corasu + ","
                                                          + tipo_cuenta_banco + ",'"
                                                          + Tip_Anio_Nego + "',"
                                                          + Tip_IDE + ",'"
                                                          + ejec_oficina + "'";


                    descripcion = "Pagador modificado exitosamente";
                }


                if (sqlquery.ExecuteNonQuery(sqlstr) <= 0)
                {
                    codigo = 1;
                    descripcion = "Gran Pagador ya existe";
                    return false;
                }

            }
            catch (Exception e)
            {
                codigo = 99;
                descripcion = "Error: " + e.Message;
                return false;
            }

            codigo = 0;


            return true;

        }



        public DataSet DevuelveUsuariosPagador(long rutpagador)
        {
            DataSet ds;
            String sqlstr = "";

            try
            {
                sqlstr = "Select CLA_PFL_INT, " +
	                     "(Select par_des From par Where par_tab = 28 And par_cod = CLA_PFL_INT) as PERFIL, " + 
                         "CLA_LOG_USR, " +
	                     "CLA_FEC_CRE, " +
	                     "CLA_COR_CLA, " +
	                     "CLA_RUT_USR, " + 
                         "CLA_NOM_USR," +
                         "CLA_PAS_USR " +
                         "From cla Where CLA_RUT_CLI = " + rutpagador + " Order By CLA_PFL_INT, CLA_COR_CLA " ;

                ds = sqlquery.ExecuteDataSet(sqlstr);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    codigo = 1;
                    descripcion = "Se encontraron registros";
                }
                else
                {
                    codigo = 0;
                    descripcion = "No se encontraron registros";
                }

                return ds;

            }
            catch (Exception e)
            {
                return null;
            }

        }


        public bool GuardaUsuarioPagador(long rutpagador, long rutusuario, int idperfil, string nombreusuario, string login, string correlativo, string password)
        {
            try
            {
                String sqlstr = "";

                sqlstr = "";


                sqlstr = "EXEC SP_MA_INSERTA_ACCESO_INTERNET " + rutpagador + ", '"
                                                               + rutusuario + "', "
                                                               + idperfil + ", '"
                                                               + nombreusuario + "', '"
                                                               + login + "', "
                                                               + correlativo + ", '" 
                                                               + password + "'";

                codigo = 1;
                descripcion = "Pagador guardado exitosamente";
                
                sqlquery.ExecuteNonQuery(sqlstr);

            }
            catch (Exception e)
            {
                codigo = 99;
                descripcion = "Error: " + e.Message;

                return false;
            }

            return true;

        }

        public bool ValidaUsuarioDePagador(long rutpagador, long rutusuario)
        {
            DataSet ds;
            String sqlstr = "";
            String correlativo = "";

            try
            {
                sqlstr = "SELECT * FROM CLA WHERE CLA_RUT_CLI=" + rutpagador + " AND CLA_RUT_USR = " + rutusuario + "";

                ds = sqlquery.ExecuteDataSet(sqlstr);

                if (ds.Tables[0].Rows.Count > 0)
                    correlativo = ds.Tables[0].Rows[0]["cla_cor_cla"].ToString().Trim();
                else
                    correlativo = "0";

                //Si correlativo igual a 0, debe generar clave.
                return correlativo == "0"?true:false;

            }
            catch (Exception e)
            {
                return false;
            }
        }

        public String DevuelveCorrelativoUsuario(long rutpagador, long rutusuario)
        {
            DataSet ds;
            String sqlstr = "";
            String correlativo = "";

            try
            {
                sqlstr = "SELECT isnull(max(cla_cor_cla),0)+1 as cla_cor_cla FROM CLA " +
                         "WHERE CLA_RUT_CLI = " + rutpagador;
                
                ds = sqlquery.ExecuteDataSet(sqlstr);

                if (ds.Tables[0].Rows.Count > 0)
                    correlativo = ds.Tables[0].Rows[0]["cla_cor_cla"].ToString().Trim();

                return correlativo;

            }
            catch (Exception e)
            {
                return "";
            }
        }

        public bool EliminaUsuarioDePagador(long rutpagador, long rutusuario, int perfil)
        {
            String sqlstr = "";

            try
            {
                sqlstr = "DELETE FROM CLA WHERE CLA_RUT_CLI = " + rutpagador + " AND CLA_RUT_USR = " + rutusuario + " AND CLA_PFL_INT = " + perfil;
                
                if (sqlquery.ExecuteNonQuery(sqlstr) > 0)
                    return true;
                else
                    return false;
                
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public DataSet Retorna_Num_Cliente(string Num_Cliente, string Rut_PVR)
        {//retorna el numero de cliente de un pagador. El rut del pagador no es obligación

            DataSet ds;

            String sqlstr = "";

            sqlstr = " EXEC SP_MA_GPG_DEV_NUM_CLI '" + Num_Cliente + "','" + Rut_PVR + "'";

            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;

        }

        public DataSet Retorna_Num_Cliente_Gpg_PVR(string Rut)
        {
            DataSet ds;

            String sqlstr = "";

            sqlstr = " EXEC SP_MA_BUSCAR_NUM_CLI_GPG_PVR " + Rut;

            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;
        }

 		//funcion que permite traer que tipo de pagador es , y activar distintas condiciones
        //que cambiaran los label de nombre o razon social
        public int retornatipogpg(int valor)
        {
            DataSet ds;
            String sqlstr = "";
            string cond ="";
            int resultado = 0;

            sqlstr = "EXEC SP_MA_DEVUELVE_TIPO_GPG_RZN_NAT  " + valor;

            ds = sqlquery.ExecuteDataSet(sqlstr);

            for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
            {
                cond = ds.Tables[0].Rows[0]["pnu_atr_001"].ToString().ToUpper();

                if (cond.Equals("F"))
                {

                    resultado = 1;

                }
              
                else
                    resultado = 2;

            }

            return resultado;

        }
        
        public int retornatipoidentificaciongpg(int valor)
        {
            DataSet ds;
            String sqlstr = "";
            string cond = "";
            int resultado = 0;

            sqlstr = "EXEC SP_MA_DEVUELVE_TIPO_IDENTIFICACION_GPG  " + valor;

            ds = sqlquery.ExecuteDataSet(sqlstr);

            for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
            {
                cond = ds.Tables[0].Rows[0]["pnu_vld_dig"].ToString().ToUpper();

                if (cond.Equals("S"))
                {

                    resultado = 1;

                }

                else
                    resultado = 2;

            }

            return resultado;

        }
        
        //funcion que permite traer que tipo de pagador es , y activar distintas condiciones
        public int retornaayudatipogpg(long nit)
        {
            DataSet ds;
            String sqlstr = "";
            string cond = "";
            int resultado = 0;

            sqlstr = "EXEC SP_MA_DEVUELVE_TIPO_AYUDA_GPG " + nit;

            ds = sqlquery.ExecuteDataSet(sqlstr);

            for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
            {
                cond = ds.Tables[0].Rows[0]["pnu_vld_dig"].ToString().ToUpper();

                if (cond.Equals("N"))
                {

                    resultado = 1;

                }

                else
                    resultado = 2;

            }

            return resultado;

        }

#region LINEA DE FINANCIAMIENTO
        public DataSet Devuelve_Pagador_Linea_Financiamiento_Activo(string id_financiamiento, string rutproveedor)
        {
            /* puede recibir tanto un parametro como ninguno o todos*/
            DataSet ds;
            String sqlstr = "";

            sqlstr = "SP_MA_DEVUELVE_GPG_LINEA_FINAN_ACITVO '" + id_financiamiento + "','" + rutproveedor + "'";

            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;
        }
#endregion


        #region SEGMENTACION

        public bool ValidaCalendario(long rutGPG, DateTime fecha)
        {
            DataSet ds;
            String sqlstr = "";

            try
            {
                sqlstr = "SP_MA_DEVUELVE_FECHA " + rutGPG + ", '" + rg.FechaJuliana(fecha) + "'";

                ds = sqlquery.ExecuteDataSet(sqlstr);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public DataSet DevuelveFechas(long rutGPG, DateTime FEC_DES, DateTime FEC_HAS) //SH - 20120210
        {
            DataSet ds;
            string sqlstr;

            sqlstr = "SP_MA_DEVUELVE_CALENDARIO " + rutGPG + ", '" + rg.FechaJuliana(FEC_DES) + "', '" + rg.FechaJuliana(FEC_HAS) + "'";

            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;
        }

        public DataSet DevuelveFechasPorDias(long rutGPG, DateTime FEC_DES, DateTime FEC_HAS) //SH - 20120210
        {
            DataSet ds;
            string sqlstr;

            sqlstr = "SP_MA_DEVUELVE_CALENDARIO_por_dias " + rutGPG + ", '" + rg.FechaJuliana(FEC_DES) + "', '" + rg.FechaJuliana(FEC_HAS) + "'";

            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;
        }

        public DataSet DevuelveSegmentacion(long rutGPG) //SH - 20120224
        {
            DataSet ds;
            String sqlstr;

            sqlstr = "";
            sqlstr = "SP_MA_DEV_SEGMENTACION " + rutGPG + ", 0";

            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;

        }

        public DataSet DevuelveSegmentacion(long rutGPG, int Select_seg) //SH - 20120224
        {
            DataSet ds;
            String sqlstr;

            sqlstr = "";
            sqlstr = "SP_MA_DEV_SEGMENTACION " + rutGPG + ", " + Select_seg;

            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;

        }

        public DataSet DevuelveSegmentacionPVR(long rutGPG, long rutPVR1, long rutPVR2)   //SH - 20120228
        {
            try
            {
                DataSet ds;
                String sqlstr;

                sqlstr = "";
                sqlstr = "SP_MA_DEV_SEG_PROVEEDORES " + rutGPG + ", " + rutPVR1 + ", " + rutPVR2;
                ds = sqlquery.ExecuteDataSet(sqlstr);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    codigo = 1;
                    descripcion = "Se encontraron registros";
                }
                else
                {
                    codigo = 0;
                    descripcion = "No se encontraron registros";
                }


                return ds;

            }
            catch (Exception e)
            {
                return null;
            }
        
        }

        public bool GuardaSegmentacion(long GPG_RUT, long SEG_COR, double SEG_POR)   //SH - 20120227
        {
            String sqlstr;

            sqlstr = "";

            sqlstr = "SP_MA_MOD_SEGMENTACION " + GPG_RUT + ", " + SEG_COR + ", " + SEG_POR.ToString().Replace(",", ".");

            if (sqlquery.ExecuteNonQuery(sqlstr) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool GuardaSegmento(long GPG_RUT, long PRV_RUT, int SEG) //SH - 20120227
        {
            String sqlstr;

            sqlstr = "SP_MA_MOD_SEGMENTO " + GPG_RUT + ", " + PRV_RUT + ", " + SEG;

            if (sqlquery.ExecuteNonQuery(sqlstr) > 0)
                return true;
            return false;
        }  

		//-----------------------------------------------------------------
		// Metodios del Web Confirming
        public int DarOrdenDeNoPago(long rutproveedor, int nrodocto)
        {
            String sqlstr = "";

            sqlstr = "Exec sp_web_da_orden_no_pago " + rutproveedor + ", " + nrodocto;

            return sqlquery.ExecuteNonQuery(sqlstr);

        }
		
		 public DataSet DevuelveDetalleGpg(long rutpagador)
        {

            DataSet ds;
            String sqlstr = "";
            String Fecdia = rg.FechaJuliana(DateTime.Now.ToString());

            sqlstr = "EXEC SP_WEB_DevuelveDetalleSimulacion " + rutpagador;

            ds = sqlquery.ExecuteDataSet(sqlstr);

            return ds;

        }

         public double DevuelveTasaNegocioPagador(long rutpagador, long rutproveedor)
        {
            DataSet ds;
            String sqlstr = "";

            sqlstr += "if exists (Select NGP_SPR_COL FROM NGP WHERE GPG_RUT = " + rutpagador;
            sqlstr += " AND PVR_RUT  = " + rutproveedor + " AND NGP_COM_SPR = 'S' ) ";
            sqlstr += " select  NGP_SPR_COL as Tasa FROM NGP WHERE GPG_RUT = " + rutpagador + " AND PVR_RUT = " + rutproveedor;
            sqlstr += " AND NGP_COM_SPR = 'S' ";
            sqlstr += " Else ";
            sqlstr += " SELECT  GPG_TAS_COM_DEF as Tasa FROM GPG WHERE GPG_RUT = " + rutpagador;

            ds = sqlquery.ExecuteDataSet(sqlstr);

            if (ds.Tables[0].Rows.Count > 0)
                return double.Parse(ds.Tables[0].Rows[0][0].ToString());
            else
                return 0;

        }


         public double Devuelve_tasa_Mora_Pagador(string rut_GPG)
         {
             DataSet ds;
             String sqlstr = "";

             sqlstr += "EXEC SP_MA_DEVUELVE_TSA_MORA_GPG " + rut_GPG;

             ds = sqlquery.ExecuteDataSet(sqlstr);

             if (ds.Tables[0].Rows.Count > 0)
                 return double.Parse(ds.Tables[0].Rows[0][0].ToString());
             else
                 return 0;

         }

        
        #endregion


        #region PLANES
         public bool DEVOLVER_SI_TRABAJA_PLAN(string RUT_GPG)
         {
             //INDICA CON UN TRUE SI USA PLAN, FALSE SINO.
            DataSet ds;
            String sqlstr = "";
            
            try
            {
                sqlstr = " SP_PLN_DEVUELVE_USA_PLAN_GPG " + RUT_GPG;

                ds = sqlquery.ExecuteDataSet(sqlstr);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    sqlstr = ds.Tables[0].Rows[0]["GPG_TIP_PLN"].ToString();
                    if (sqlstr.Equals("T"))//tradicional
                    {
                        return false;
                    }
                    else
                    {
                        if (sqlstr.Equals("P"))//Plan
                        {
                            return true;
                        }
                    }
                }
                else
                {
                    codigo = 99;
                    descripcion = "Problemas conexion  a labasee";
                    
                }

                return true;   

            }
            catch (Exception e)
            {
                return false;
            }

         }

         public bool Actualizar_Base_Facturacion(string Rut_Gpg, int cod_Tip_Anio)
         {
             string tip_anio = "";
             string sqlStr;

             try
             {
                 if (cod_Tip_Anio == 1)//comercial
                 {
                     tip_anio = "C";
                 }
                 else
                 {
                     tip_anio = "R";
                 }

                 sqlStr = "Update Gpg set GPG_TIP_ANIO = '" + tip_anio + "' where gpg_rut = " + Rut_Gpg;

                 if (sqlquery.ExecuteNonQuery(sqlStr) > 0)
                     return true;
                 else
                     return false;
             }
             catch
             {
                 return false;
             }

         }

        #endregion

    }

}
