﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Web.UI.WebControls;
using System.Web.UI;
using AjaxControlToolkit;


namespace dim.rutinas
{
    public class RutinasWeb
    {
        
        rutinas.RutinasGenerales rg = new dim.rutinas.RutinasGenerales();

        public void ModalPopUp(ModalPopupExtender Modal)
        {
            try
            {
                Modal.Show();
            }
            catch (Exception e)
            { 
            }

        }



        public void Mensaje(UpdatePanel up, String strMessage)
        {
            String strScript = "alert('" + strMessage + "');";
            Guid guidKey = Guid.NewGuid();
            ScriptManager.RegisterStartupScript(up, up.GetType(), guidKey.ToString(), strScript, true);
        }

        public void EjecutaScript(UpdatePanel up, String function)
        {
            String strScript = function;
            Guid guidKey = Guid.NewGuid();
            ScriptManager.RegisterStartupScript(up, up.GetType(), guidKey.ToString(), strScript, true);
        }

        public void LlenaDrop(List<ListItem> DatosLst, DropDownList lst)
        {

            //lst.DataTextField = "Descripcion";
            //lst.DataValueField = "Codigo";

            lst.DataSource = DatosLst;
            lst.DataBind();

        }

        public void BuscaCombo(DropDownList lst, int valor)
        {
            for (int i=0;i<=lst.Items.Count-1;i++)
            {
                if (lst.Items[i].Value == valor.ToString())
                {
                    lst.Items[i].Selected = true;
                    return;
                }
            }
        }

        public void EjecutaScript(Page Pg, String nombrefuncion)
        {
            String Script;
            Script = "<script language=JavaScript>"+ nombrefuncion + "</script>";
            Pg.RegisterStartupScript("Pgn", Script);
        }

        public void MensajeScript(Page pg, String mensaje)
        {
            String Script;
            Script = "<script language=JavaScript>alert('" + rg.LimpiaExcepcion(mensaje) + "');</script>";
            pg.RegisterStartupScript("Pgn", Script);
        }

    }

}
