﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace dim.rutinas
{
    public class ClsLog
    {

        public void eLog(string mensaje)
        {

            String ArchLog = AppDomain.CurrentDomain.SetupInformation.ApplicationBase + @"ProcesoAutomatico.log";

            string fecha = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss");

            mensaje = fecha + "; " + mensaje + ";";
                       
            Console.WriteLine(mensaje);              
            
            System.IO.StreamWriter sw = new System.IO.StreamWriter(ArchLog, true);
            sw.WriteLine(mensaje);
            sw.Close();


       }

    } 

} 