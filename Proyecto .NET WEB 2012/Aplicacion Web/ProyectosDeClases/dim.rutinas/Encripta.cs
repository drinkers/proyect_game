﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace dim.rutinas
{
    public class Encripta
    {

        public string Desencriptar(string valor)
        {

            // Recibe el rut del usuario sin digito ni guión
            string StrClave;
            try
            {
                StrClave = Decrypt(valor, "");
                return StrClave;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public string Encriptar(string valor)
        {
            // Recibe el rut del usuario sin digito ni guión
            string StrClave;
            try
            {
                StrClave = Encrypt(valor, "");
                return StrClave;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        private string Decrypt(string encryptedText, string password)
        {
            UTF8Encoding encoding = new UTF8Encoding();
            byte[] buffer = Convert.FromBase64String(encryptedText);
            byte[] iv = { 0, 0, 0, 0, 0, 0, 0, 0 };
            SymmetricAlgorithm provider = SymmetricAlgorithm.Create("TripleDES");
            byte[] key = new PasswordDeriveBytes(password, iv).CryptDeriveKey("TripleDES", "MD5", provider.KeySize, iv);
            ICryptoTransform decryptor = provider.CreateDecryptor(key, iv);
            try
            {
                return encoding.GetString(decryptor.TransformFinalBlock(buffer, 0, buffer.Length));
            }
            finally
            {
                decryptor.Dispose();
            }
        }

        private string Encrypt(string text, string password)
        {
            byte[] buffer = Encoding.UTF8.GetBytes(text);
            byte[] iv = { 0, 0, 0, 0, 0, 0, 0, 0 };
            SymmetricAlgorithm provider = SymmetricAlgorithm.Create("TripleDES");
            byte[] key = new PasswordDeriveBytes(password, iv).CryptDeriveKey("TripleDES", "MD5", provider.KeySize, iv);
            ICryptoTransform encryptor = provider.CreateEncryptor(key, iv);
            
            try
            {
                return Convert.ToBase64String(encryptor.TransformFinalBlock(buffer, 0, buffer.Length));
            }
            finally
            {
                encryptor.Dispose();
            }
        }

    }

}
