﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AjaxControlToolkit;
using System.Web.UI.WebControls;
using System.Web.UI;
using Microsoft.VisualBasic;

namespace dim.rutinas
{
    public class ClsMensaje
    {
        public enum TipoDeMensaje : short
        {
            _Error = 1,
            _Exclamacion = 2,
            _Informacion = 3,
            _Confirmacion = 4,
            _Excepcion = 5,
            _Redireccion = 6
        }



        public void Mensaje(Page P, string Titulo, string Texto, TipoDeMensaje Tipo, string LB, bool Contenedora)
        {

            //LB= "";
            //Contenedora = true;

            ModalPopupExtender Modal = default(ModalPopupExtender);
            Image Image_Mensaje = default(Image);
            Button Boton_Aceptar = default(Button);
            Button Boton_Cancel = default(Button);
            Label Lbl_Titulo = default(Label);
            Label Lbl_Mensaje = default(Label);


            if (Contenedora)
            {
                Lbl_Titulo = (System.Web.UI.WebControls.Label)P.FindControl("Mensaje1$Lbl_error"); //(System.Web.UI.WebControls.Label)P.FindControl("ctl00$Mensaje1$Lbl_error");
                Lbl_Mensaje = (System.Web.UI.WebControls.Label)P.FindControl("Mensaje1$Txt_Mensaje");
                Modal = (AjaxControlToolkit.ModalPopupExtender)P.FindControl("Mensaje1$Modal_Mensaje");
                Image_Mensaje = (System.Web.UI.WebControls.Image)P.FindControl("Mensaje1$Img_Mensaje");
                Boton_Cancel = (System.Web.UI.WebControls.Button)P.FindControl("Mensaje1$canc");
                Boton_Aceptar = (System.Web.UI.WebControls.Button)P.FindControl("Mensaje1$Okbutton");


            }
            else
            {
                Lbl_Titulo = (System.Web.UI.WebControls.Label)P.FindControl("ctl00$Body$Mensaje1$Lbl_error");
                Lbl_Mensaje = (System.Web.UI.WebControls.Label)P.FindControl("ctl00$Body$Mensaje1$Txt_Mensaje");

                Modal = (AjaxControlToolkit.ModalPopupExtender)P.FindControl("ctl00$Body$Mensaje1$Modal_Mensaje");
                Image_Mensaje = (System.Web.UI.WebControls.Image)P.FindControl("ctl00$Body$Mensaje1$Img_Mensaje");
                Boton_Cancel = (System.Web.UI.WebControls.Button)P.FindControl("ctl00$Body$Mensaje1$canc");
                Boton_Aceptar = (System.Web.UI.WebControls.Button)P.FindControl("ctl00$Body$Mensaje1$Okbutton");

            }

            Lbl_Titulo.Text = Titulo;
            Lbl_Mensaje.Text = Texto;

            Boton_Aceptar.Attributes.Remove("onClick");

            switch (Tipo)
            {

                case TipoDeMensaje._Error:
                    Image_Mensaje.ImageUrl = "~/Imagenes/Iconos/error.gif";
                    Boton_Cancel.Visible = false;
                    break;
                case TipoDeMensaje._Exclamacion:
                    Image_Mensaje.ImageUrl = "~/Imagenes/Iconos/Info.gif";
                    Boton_Cancel.Visible = false;
                    break;
                case TipoDeMensaje._Informacion:
                    Image_Mensaje.ImageUrl = "~/Imagenes/Iconos/Info.gif";
                    Boton_Cancel.Visible = false;
                    break;
                case TipoDeMensaje._Redireccion:
                    Image_Mensaje.ImageUrl = "~/Imagenes/Iconos/Info.gif";
                    Boton_Cancel.Visible = false;

                    if (!string.IsNullOrEmpty(LB))
                    {
                        Boton_Aceptar.Attributes.Add("onClick", "__doPostBack('" + LB + "', '');");
                    }
                    break;
                case TipoDeMensaje._Confirmacion:
                    Image_Mensaje.ImageUrl = "~/Imagenes/Iconos/Question.gif";
                    Boton_Cancel.Visible = true;
                    Modal.OkControlID = Boton_Cancel.ClientID;

                    if (!string.IsNullOrEmpty(LB))
                    {
                        Boton_Aceptar.Attributes.Add("onClick", "__doPostBack('" + LB + "', '');");

                    }

                    break;
                case TipoDeMensaje._Excepcion:
                    Image_Mensaje.ImageUrl = "~/Imagenes/Iconos/Info.gif";
                    Boton_Cancel.Visible = false;
                    Modal.OkControlID = Boton_Aceptar.ClientID;
                    break;
            }

            Modal.Show();

        }


    }
}
