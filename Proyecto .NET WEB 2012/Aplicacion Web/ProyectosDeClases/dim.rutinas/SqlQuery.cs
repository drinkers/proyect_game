﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using dim.rutinas;

namespace dim.rutinas
{
    public class SqlQuery
    {
        ClsConexion con = new ClsConexion();
        SqlConnection consql = new SqlConnection();

        
        public int estadoconsulta { get; set; }
        public string descripcionconsulta { get; set; }


        //public SqlDataReader ExecuteDataReader(String queryString)
        //{
        //    SqlDataReader reader;

        //    try
        //    {
        //        //SqlConnection connection = new SqlConnection(con.DB_ConnectionString);

        //        using (SqlConnection connection = new SqlConnection(con.DB_ConnectionString))
        //        {
        //            SqlCommand command = new SqlCommand(queryString, connection);
        //            command.Connection.Open();

        //            reader = command.ExecuteReader();
        //            //command.Connection.Close();//NO SACAR COMENTARIO.YA QUE NO SE DEBE CERRAR CONECCION
        //            connection.Close();
        //            connection.Dispose();
        //        }

        //        estadoconsulta = 1;
        //        descripcionconsulta = "OK";


        //    }
        //    catch (Exception e)
        //    {
        //        estadoconsulta = 1;
        //        descripcionconsulta = "Error: " + e.Message;
        //        reader = null;
        //    }

        //    return reader;

        //    //-----------------------------------------------------------------//
        //    //al terminar se debe cerrar el objeto para que cierre la conexion
        //    //-----------------------------------------------------------------//

        //}

        public int ExecuteNonQuery(String queryString)
        {
            int filas = 0;

            try
            {

                using (SqlConnection connection = new SqlConnection(con.DB_ConnectionString))
                {
                    SqlCommand command = new SqlCommand(queryString, connection);
                    command.Connection.Open();
                    filas = command.ExecuteNonQuery();
                    //command.Connection.Close();
                    //***
                    connection.Close();
                    connection.Dispose();
                }

                estadoconsulta = 1;
                descripcionconsulta = "OK";

            }
            catch (SqlException e)
            {
                estadoconsulta = 99;
                descripcionconsulta = "Error: " + e.Message;
            }
            finally
            {
                
            }

            return filas;

        }

        public int ExecuteNonQuery(String queryString, Byte[] Bytee)
        {
            int filas = 0;

            try
            {
                using (SqlConnection connection = new SqlConnection(con.DB_ConnectionString))
                {

                    SqlCommand command = new SqlCommand(queryString, connection);
                    command.Parameters.Add("@@imagen", Bytee);
                    command.Connection.Open();
                    filas = command.ExecuteNonQuery();
                    //command.Connection.Close();
                    //****
                    connection.Close();
                    connection.Dispose();
                }

                
                estadoconsulta = 1;
                descripcionconsulta = "OK";

            }
            catch (Exception e)
            {
                estadoconsulta = 99;
                descripcionconsulta = "Error: " + e.Message;

            }

            return filas;
        }

        public DataSet ExecuteDataSet(String queryString)
        {
            DataSet ds = new DataSet();

            try
            {
                SqlDataAdapter adapter;
          
                using (SqlConnection connection = new SqlConnection(con.DB_ConnectionString))
                {
                    SqlCommand command = new SqlCommand(queryString, connection);
                    command.Connection.Open();
                    command.CommandTimeout = 5000;
                    adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);
                    
                    //** 
                    
                    connection.Close();
                    connection.Dispose();
                }
                estadoconsulta = 1;
                descripcionconsulta = "OK";
            }
            catch (SqlException e)
            {
                estadoconsulta = 99;
                descripcionconsulta = "Error: " + e.Message;
            }

            return ds;
          
        }

        public DataSet ExecuteDataSet_FACTOR(String queryString)
        {
            DataSet ds = new DataSet();

            try
            {
                SqlDataAdapter adapter;

                using (SqlConnection connection = new SqlConnection(con.DB_ConnectionFACTORString))
                {
                    SqlCommand command = new SqlCommand(queryString, connection);
                    command.Connection.Open();
                    adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);
                    //command.Connection.Close();
                    //***
                    connection.Close();
                    connection.Dispose();
                }

                estadoconsulta = 1;
                descripcionconsulta = "OK";

            }
            catch (SqlException e)
            {
                estadoconsulta = 99;
                descripcionconsulta = "Error: " + e.Message;
            }

            return ds;

        }

        public bool ExecuteTransaccion(List<String> queryString)
        {
            //DataSet ds = new DataSet();

            try
            {
               
                
                    using (SqlConnection connection = new SqlConnection(con.DB_ConnectionString))
                    {
                        // Abrimos la conexión
                        connection.Open();
                        // Creamos el objeto Transaction
                        SqlTransaction tran = connection.BeginTransaction();

                         // lo pongo en un Try/Catch para detectar los errores
                        try
                        {

                            foreach (String query in queryString)
                            {
                               

                                // Creamos el objeto SqlCommand y asignamos los datos
                                // a los parámetros
                                SqlCommand cmd = new SqlCommand(query, connection);
                                
                                cmd.CommandTimeout = 8000;
                                // Asignamos la transacción al comando
                                cmd.Transaction = tran;

                                // Insertamos los datos

                                // Ejecutamos el comando
                                cmd.ExecuteNonQuery();

                            }

                            // Si llega aquí es que todo fue bien,
                            // por tanto, llamamos al método Commit
                            tran.Commit();

                            estadoconsulta = 1;
                            descripcionconsulta = "Se han actualizado los datos";

                        }
                        catch (Exception ex)
                        {
                            // Si hay error, desahacemos lo que se haya hecho
                            tran.Rollback();
                            estadoconsulta = 99;
                            descripcionconsulta = "ERROR: \r\n" + ex.Message;
                            return false;
                        }

                        // Cerramos la conexión,
                        // aunque no es necesario ya que al finalizar
                        // el using se cerrará
                        connection.Close();
                        connection.Dispose();
                    }

            }
            catch (SqlException e)
            {

                estadoconsulta = 99;
                descripcionconsulta = "Error: " + e.Message;
                return false;
            }

            estadoconsulta = 1;
            descripcionconsulta = "Transaccion terminada";
            return true;

        }

        public DataTable ExecuteDataTable(String queryString)
        {
            DataTable ds = new DataTable();

            try
            {
                SqlDataAdapter adapter;

                using (SqlConnection connection = new SqlConnection(con.DB_ConnectionString))
                {
                    SqlCommand command = new SqlCommand(queryString, connection);
                    command.Connection.Open();
                    adapter = new SqlDataAdapter(command);

                    adapter.Fill(ds);

                    //command.Connection.Close();
                    connection.Close();
                    connection.Dispose();
                }

                estadoconsulta = 1;
                descripcionconsulta = "OK";

            }
            catch (SqlException e)
            {
                estadoconsulta = 99;
                descripcionconsulta = "Error: " + e.Message;
            }

            return ds;

        }
        
        public bool ExecuteBulkCopyExcel(string Tabla, DataTable data, string usr)
        {
            string str_sql = "";
            System.Data.SqlClient.SqlBulkCopy bulkCopy;

            try
            {
                str_sql = "DELETE BBVA_LINEA_TRIANGULAR..PAGOMASIVO WHERE USUARIO = '" + usr + "'";
                ExecuteNonQuery(str_sql);

                using (SqlConnection connection = new SqlConnection(con.DB_ConnectionString))
                {
                    //se abre la conexion
                    connection.Open();

                    bulkCopy = new SqlBulkCopy(connection);

                    bulkCopy.DestinationTableName = Tabla;
                    bulkCopy.WriteToServer(data, 0);
                    bulkCopy.Close();

                    //se cierra la conexion
                    connection.Close();
                    connection.Dispose();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }

            return true;

        }

    }

}
