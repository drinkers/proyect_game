﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Net.Mime;

namespace dim.rutinas
{
    public class ClassEnviarMail
    {

        public void EnviarMail(String[] Para, String De, String Asunto, String Cuerpo, String[] files, String Servidor)
        {

            MailMessage msg = new MailMessage();
            String file = "";

            for (int i = 0; i <= Para.Length - 1; i++)
            {
                if (Para[i] == null)
                    break;

                msg.To.Add(new MailAddress(Para[i]));
            }

            msg.From = new MailAddress(De);
            msg.Subject = Asunto;
            msg.Body = Cuerpo;
            msg.IsBodyHtml = true;

            if (files != null)
            {
                for (int i = 0; i <= files.Length - 1; i++)
                {
                    if (files[i] == null)
                        break;

                    file = files[i];
                    // Create  the file attachment for this e-mail message.
                    Attachment data = new Attachment(file, MediaTypeNames.Application.Octet);
                    // Add time stamp information for the file.
                    ContentDisposition disposition = data.ContentDisposition;
                    disposition.CreationDate = System.IO.File.GetCreationTime(file);
                    disposition.ModificationDate = System.IO.File.GetLastWriteTime(file);
                    disposition.ReadDate = System.IO.File.GetLastAccessTime(file);
                    // Add the file attachment to this e-mail message.
                    msg.Attachments.Add(data);

                }
            }

            SmtpClient clienteSmtp = new SmtpClient(Servidor);

            try
            {
                clienteSmtp.Send(msg);
            }

            catch (Exception ex)
            {
                Console.Write(ex.Message);
                Console.ReadLine();
            }

        }

        public void EnviarMail(String[] A, String[] copia, String De, String Asunto, String Cuerpo, String[] files, String Servidor)
        {

            MailMessage msg = new MailMessage();
            String file = "";

            for (int i = 0; i <= A.Length - 1; i++)
            {
                if (A[i] == null)
                    break;

                msg.To.Add(new MailAddress(A[i]));
            }

            for (int i = 0; i <= copia.Length - 1; i++)
            {
                if (copia[i] == null)
                    break;

                msg.CC.Add(new MailAddress(copia[i]));
            }

            msg.From = new MailAddress(De);
            msg.Subject = Asunto;
            msg.Body = Cuerpo;
            msg.IsBodyHtml = true;

            if (files != null)
            {
                for (int i = 0; i <= files.Length - 1; i++)
                {
                    if (files[i] == null)
                        break;

                    file = files[i];
                    // Create  the file attachment for this e-mail message.
                    Attachment data = new Attachment(file, MediaTypeNames.Application.Octet);
                    // Add time stamp information for the file.
                    ContentDisposition disposition = data.ContentDisposition;
                    disposition.CreationDate = System.IO.File.GetCreationTime(file);
                    disposition.ModificationDate = System.IO.File.GetLastWriteTime(file);
                    disposition.ReadDate = System.IO.File.GetLastAccessTime(file);
                    // Add the file attachment to this e-mail message.
                    msg.Attachments.Add(data);

                }
            }

            SmtpClient clienteSmtp = new SmtpClient(Servidor);

            try
            {
                clienteSmtp.Send(msg);
            }

            catch (Exception ex)
            {
                Console.Write(ex.Message);
                Console.ReadLine();
            }

        }

    }

}
