﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace dim.rutinas
{
    public class ClsSession
    {

        public string Descripcion { get; set; }
        public int Objetivo { get; set; }
        public string NitProveedor { get; set; }
        public string DvProveedor { get; set; }
        public string NombreProveedor { get; set; }
        public int IDX { get; set; }
        public int CantColumnasArchivo { get; set; }
    }
}
