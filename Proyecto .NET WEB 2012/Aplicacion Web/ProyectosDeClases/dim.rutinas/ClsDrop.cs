﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace dim.rutinas
{
    public class ClsDrop
    {
        
        private string _codigo;
        private string _descripcion;

        public string codigo
        {
            get { return _codigo; }
            set { _codigo = value; }
        }
         public string descripcion
        {
            get { return _descripcion; }
            set { _descripcion = value; }
        }

         public ClsDrop(string cod, string des)
        {
            _codigo = cod;
            _descripcion = des;
        }

         public ClsDrop()
         {
             _codigo = null;
             _descripcion = null;
         }
    

    }
}
