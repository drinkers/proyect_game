﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LibreriaClases.BLL;
using dim.rutinas;

namespace Portal
{
    public partial class Registros_de_Adm : System.Web.UI.Page
    {
      

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                CargaSucursales();
            }
        }

        protected void Btn_Agregar_Click(object sender, EventArgs e)
        {
            try
            {
                Administrador adm = new Administrador();
                RutinasGenerales rg = new RutinasGenerales();
                RutinasWeb rw = new RutinasWeb();
                
                if (TxtRut.Text == string.Empty)
                {
                    rw.MensajeScript(Page, "Debe Ingresa un Rut");
                    return;
                }

                if (TxtName.Text == string.Empty)
                {
                    rw.MensajeScript(Page, "Debe Ingresa un Nombre");
                    return;
                }

                if (TxtApellido.Text == string.Empty)
                {
                    rw.MensajeScript(Page, "Debe Ingresa un Apellidop");
                    return;
                }

                if (Txt_mail.Text == string.Empty)
                {
                    rw.MensajeScript(Page, "Debe Ingresa un Correo");
                    return;
                }

                if (Txt_pass.Text == string.Empty)
                {
                    rw.MensajeScript(Page, "Debe Ingresa la Contraseña");
                    return;
                }

                if (Txt_confirPass.Text == string.Empty)
                {
                    rw.MensajeScript(Page, "Debe Reengresar la Contraseña");
                    return;
                }

                if (Chk_admin.Checked == false && Chk_vendedor.Checked == false)
                {
                    rw.MensajeScript(Page, "Debe Seleccionar un Perfil de Ingreso");
                    return;
                }


                if (validaAdmin())
                {

                    rw.MensajeScript(Page, "El administrador ya esta registrador");
                    Limpiar();
                    return;
                }

                adm.RutAdmin = TxtRut.Text;
                adm.NombreAdmin = TxtName.Text;
                adm.ApellidoAdmin = TxtApellido.Text;
                adm.Sucursal.IdSucursal = Ddp_Sucursal.SelectedIndex;
                adm.Email = Txt_mail.Text;
                adm.ClaveIngreso = Txt_pass.Text;
                if (Chk_admin.Checked == true)
                    adm.Pfl = 0;
                else
                    adm.Pfl = 1;


                string confirmingacion = Txt_confirPass.Text;

                if (adm.ClaveIngreso != confirmingacion)
                {
                    rw.MensajeScript(Page, "La Contraseñas deben ser Identicas");
                    return;
                }

                adm = adm.Create(adm);

                if (adm != null)
                {
                    rw.MensajeScript(Page, "Se guardo con Exito");
                    Limpiar();
                    return;
                }
                else
                {
                    rw.MensajeScript(Page, "Error al Guardar");
                    return;
                }
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        private bool validaAdmin()
        {
            Administrador adm = new Administrador();
            Administrador biblioBO = adm.ValidaAdmin(TxtRut.Text);
            string rut_valido = biblioBO.RutAdmin;
            string rut_ingreso = TxtRut.Text;


            if (rut_ingreso.Equals(rut_valido))
            {

                return true;
            }
            else
                return false;
        }

        private void CargaSucursales()
        {
            this.Ddp_Sucursal.DataSource = Enum.GetValues(typeof(TipoSucursal));
            this.Ddp_Sucursal.DataBind();
        }

        private void Limpiar()
        {
            TxtRut.Text = string.Empty;
            TxtName.Text = string.Empty;
            TxtApellido.Text = string.Empty;
            Txt_mail.Text = string.Empty;
            Txt_pass.Text = string.Empty;
            Txt_confirPass.Text = string.Empty;
        }

    }
}