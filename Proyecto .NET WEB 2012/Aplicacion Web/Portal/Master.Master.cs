﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LibreriaClases.BLL;

namespace Portal
{
    public partial class Master : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["usr"] == null)
            {
                Response.Redirect("~/FinSesion.aspx", false);
                return;
            }

            if (!Page.IsPostBack)
            {
                acceso_menu();
            }
        }

        protected void acceso_menu()
        {

            try
            {

                string usr_admin = string.Empty, Perfil = string.Empty, Usuario = string.Empty;

                if (Session["usr"] != null)
                {
                    usr_admin = Session["usr"].ToString();
                    BuscarPerfil(usr_admin);
                }

                else
                {
                    Response.Redirect("~/FinSesion.aspx", false);
                    return;
                }
            }
            catch (Exception x)
            {
                Console.WriteLine(x.Message);
            
            }

        }

        private void BuscarPerfil(string usuario)
        {
            try
            {
                Administrador adm = new Administrador();
                Administrador biblioBO = adm.BuscarPerfil(usuario);
                int perfil = biblioBO.Pfl;
                if (perfil == 3)
                {
                    Response.Redirect("~/FinSesion.aspx", false);
                    return;
                }
                else
                { 
                    if(perfil == 0)
                        vende.Visible = true;
                    else
                        vende.Visible = false;
                
                }

              
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        protected void btnCerrarSession_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            HttpContext.Current.Response.Redirect("~/Login.aspx", true);

            //Response.Redirect("~/Login.aspx");
        }

    }
}