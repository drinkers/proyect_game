﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Portal.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Auntentificacion</title>
    <link href="Content/css/Style2.css" rel="stylesheet" type="text/css" />
    <link href="Content/css/Styles.css" rel="stylesheet" type="text/css" />
   

    <style type="text/css">
        #TableLogin {
            width: 333px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div id="Banner">
    </div>    
        <%--<div class="div-align">--%>
        <div  align="center">
			    <br /><br />            
                <br /><br />
                <div class="test_contenedor" style="width:300px">
                <table id="TableLogin" height="300" cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td align="center" colspan="2">
                            <img src="images/DuocUC.png" width="245" style="height: 117px" />
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:Label ID="Label3" runat="server" CssClass="SubTitulos">CONTROL DE ACCESO</asp:Label>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Label ID="Label1" runat="server" CssClass="clsLab">Usuario</asp:Label>&nbsp;
                        </td>
                        <td>
                            <asp:TextBox ID="TxtUsr" TabIndex="1" runat="server" CssClass="clsMandatorio" Width="150px"
                                MaxLength="15"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Label ID="Label2" runat="server" CssClass="clsLab">Contraseña</asp:Label>&nbsp;
                        </td>
                        <td>
                            <asp:TextBox ID="TxtPwd" TabIndex="2" runat="server" CssClass="clsMandatorio" Width="150px" TextMode="Password"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td align="center" colspan="2">
                            <asp:Button ID="Btn_Aceptar" runat="server" Text="Aceptar" CssClass="button" Width="100px" OnClick="btnAceptar_Click" />
                            &nbsp;&nbsp;&nbsp;
                            <asp:Button ID="Btn_Cerrar" runat="server" Text="Limpiar" CssClass="button" Width="100px" OnClick="btnLimpiar_Click"/>
                        </td>
                        <td align="center">
                        </td>
                    </tr>
                  <tr>
                        <td align="center" colspan="2">
                            <a href="Registros_de_Adm.aspx" class="clsLab" style="color: #330066">Registro de Perfiles</a>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                        <asp:Label ID="Label4" runat="server" Text="VERSION: 1.0.0" CssClass="SubTitulos"></asp:Label>
                        </td>
                        <td>
                            
                        </td>
                    </tr>
                </table>
                </div>
			</div>
		
    </form>
</body>
</html>
