﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Portal
{
    public partial class Inicio : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
               try
            {
               String usr = Request.QueryString["usr"];
                if (usr == null)
                {
                    Response.Redirect("FinSesion.aspx");
                }
                else
                {
                    if (usr.Equals(""))
                    {
                        Response.Redirect("FinSesion.aspx");
                    }
                    else
                    {
                        usr = usr.ToUpper();
                        Session.Add("usr", usr);
                        if (Session != null)
                            Response.Redirect("~/Principal.aspx", false);
                           
                        else
                            Response.Redirect("~/FinSesion.aspx", false);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        
        }
    }
}