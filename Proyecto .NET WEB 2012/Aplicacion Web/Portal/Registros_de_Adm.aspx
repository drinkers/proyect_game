﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Registros_de_Adm.aspx.cs" Inherits="Portal.Registros_de_Adm" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="Content/css/jquery-3.1.0.min.js" type="text/javascript"></script>
    <link href="Content/css/main.css" rel="stylesheet" type="text/css" />
    <link href="Content/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="Content/css/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    	<!-- Google Fonts -->
		<link href='https://fonts.googleapis.com/css?family=Passion+One' rel='stylesheet' type='text/css'/>
		<link href='https://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'/>


</head>
<body>
    <form id="form1" runat="server">
   <div class="container">
			<div class="row main">
				<div class="panel-heading">
	               <div class="panel-title text-center">
	               		<h1 class="title">Ingreso de Administrador</h1>
	               		<hr />
	               	</div>
	            </div> 
				<div class="main-login main-center">
					<form class="form-horizontal" method="post" action="#">
                    <div class="form-group">
							<label for="name" class="cols-sm-2 control-label">Ingreso de Rut</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                    <asp:TextBox ID="TxtRut" runat="server" class="form-control" placeholder="Ingresa tu Rut"></asp:TextBox>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="name" class="cols-sm-2 control-label">Ingreso de Nombre</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                    <asp:TextBox ID="TxtName" runat="server" class="form-control" placeholder="Ingresa tu Nombre"></asp:TextBox>
								</div>
							</div>
						</div>
                        <div class="form-group">
							<label for="name" class="cols-sm-2 control-label">Ingreso de Apellido</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                    <asp:TextBox ID="TxtApellido" runat="server" class="form-control" placeholder="Ingresa tu Apellido"></asp:TextBox>
								</div>
							</div>
						</div>
                        <div class="form-group">
							<label for="name" class="cols-sm-2 control-label">Seleccion de Sucursal</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                    <asp:DropDownList ID="Ddp_Sucursal" runat="server" class="form-control" placeholder="Seleccione Sucursal"></asp:DropDownList>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="cols-sm-2 control-label">Ingreso de Mail</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
									  <asp:TextBox ID="Txt_mail" runat="server" class="form-control" placeholder="Ingresa tu Mail"></asp:TextBox>
								</div>
							</div>
						</div>
                        <div class="form-group">
                        <label for="email" class="cols-sm-2 control-label">Seleccion de Perfil</label>
							<div class="cols-sm-10">
								<div class="input-group">
								
									 <asp:CheckBox ID="Chk_admin" runat="server" Text="Admin" CssClass="clsMandatorio" />&nbsp; <asp:CheckBox ID="Chk_vendedor" Text="Vendedor" CssClass="clsMandatorio" runat="server" />
								</div>
							</div>
                        
                        </div>
						<div class="form-group">
							<label for="password" class="cols-sm-2 control-label">Password</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
									  <asp:TextBox ID="Txt_pass" runat="server" class="form-control" placeholder="Ingresa tu Password" TextMode="Password"></asp:TextBox>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="confirm" class="cols-sm-2 control-label">Confirmar Password</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
									  <asp:TextBox ID="Txt_confirPass" runat="server" class="form-control" placeholder="ReIngresa tu Password" TextMode="Password"></asp:TextBox>
								</div>
							</div>
						</div>
						<div class="form-group ">
                            <asp:Button ID="Btn_Agregar" runat="server" Text="Registrar"  
                                class="btn btn-primary btn-lg btn-block login-button" 
                                onclick="Btn_Agregar_Click"/>
						</div>
						<div class="login-register">
				            <a href="Login.aspx">Regresar Al Login</a>
				         </div>
					</form>
				</div>
			</div>
		</div>
    </form>
</body>
</html>
