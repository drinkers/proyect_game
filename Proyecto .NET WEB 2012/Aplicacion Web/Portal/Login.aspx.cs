﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using dim.rutinas;
using LibreriaClases.BLL;


namespace Portal
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if(!IsPostBack)
            {
                    
            }
            
        }

        protected void btnAceptar_Click(object sender, EventArgs e)
        {

            RutinasGenerales rg = new RutinasGenerales();
            RutinasWeb rw = new RutinasWeb();

            try
            {
                

                if (TxtUsr.Text == string.Empty)
                {
                    rw.MensajeScript(Page, "Debe Ingresa un Usuario");
                    return;
                }

                if (TxtPwd.Text == string.Empty)
                {
                    rw.MensajeScript(Page, "Debe Ingresa la Contraseña");
                    return;

                }
                    
                string codigo = TxtPwd.Text;
                string pass = TxtPwd.Text;
                string login = TxtUsr.Text;


                if (!validaAdmin(login,pass))
                {

                    rw.MensajeScript(Page, "El administrador no se encuentra registrado");
                    return;
                }

                if (TxtPwd.Text == pass && TxtUsr.Text == login)
                    Response.Redirect("Inicio.aspx" + "?usr=" + rg.LimpiaRut(login));       
            }

            catch (Exception x)
            {
                Response.Write(x.Message);
            }


        }

        protected void btnLimpiar_Click(object sender, EventArgs e)
        {
            TxtUsr.Text = string.Empty;
            TxtPwd.Text = string.Empty;

        }

        private bool validaAdmin(string login, string pass)
        {
            Administrador adm = new Administrador();
            Administrador biblioBO = adm.ValidaIngresoAdmin(login,pass);
            string rut_valido = biblioBO.RutAdmin;
            string pass_valido = biblioBO.ClaveIngreso;
            string rut_ingreso = TxtUsr.Text;
            string pass_ingreso =  TxtPwd.Text;

            if (rut_ingreso.Equals(rut_valido) && pass_ingreso.Equals(pass_valido))
            {
                return true;
            }
            else
                return false;
        }


    }
}