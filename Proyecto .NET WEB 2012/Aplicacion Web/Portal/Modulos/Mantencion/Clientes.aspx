﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Clientes.aspx.cs" Inherits="Portal.Modulos.Mantencion.Clientes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

  <asp:Panel ID="PanelVendedor" runat="server">
      <div class="panel-heading">
	               <div class="panel-title text-center">
	               		<h1 class="title">Ingrese Datos para Clientes</h1>
	               		<hr />
	               	</div>
	            </div> 
    <asp:Table ID="TableCliente" runat="server" style="width:50%; margin: 0 auto">
            <asp:TableRow>
                <asp:TableCell><asp:Label ID="lblIdCliente" runat="server" Text="Rut Cliente"></asp:Label></asp:TableCell>
                <asp:TableCell><asp:TextBox ID="txtIdCliente" runat="server" class="form-control"></asp:TextBox></asp:TableCell>
                <asp:TableCell>
                             <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Rut Obligatorio" ControlToValidate="txtIdCliente"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="RegularExpressionValidator" Text="*Solo se admiten números" ControlToValidate="txtIdCliente" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                </asp:TableCell>
            </asp:TableRow>   
            <asp:TableRow>
                <asp:TableCell><asp:Label ID="lblNombreCliente" runat="server" Text="Nombre del Cliente"></asp:Label></asp:TableCell>
                <asp:TableCell><asp:TextBox ID="txtNombreVendedor" runat="server" class="form-control"></asp:TextBox></asp:TableCell>
                <asp:TableCell>
                             <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Nombre Obligatorio" ControlToValidate="txtNombreVendedor"></asp:RequiredFieldValidator>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell><asp:Label ID="lblApellidoPaterno" runat="server" Text="Apellido Paterno"></asp:Label></asp:TableCell>
                <asp:TableCell><asp:TextBox ID="txtApellidoPaterno" runat="server" class="form-control"></asp:TextBox></asp:TableCell>
                <asp:TableCell>
                             <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Apellido Obligatorio" ControlToValidate="txtApellidoPaterno"></asp:RequiredFieldValidator>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                 <asp:TableCell><h2><asp:Label ID="Label1" runat="server" Text=""></asp:Label></h2></asp:TableCell>
            </asp:TableRow>
        </asp:Table>
      <asp:Label ID="lblMensaje" runat="server" Text=""></asp:Label>
   <br/>
        <asp:Button ID="btnIngresar" runat="server" Text="Ingresar" OnClick="btnIngresar_Click" />
        <asp:Button ID="btnLimpiar" runat="server" Text="Limpiar" OnClick="btnLimpiar_Click" />
           
    </asp:Panel>
</asp:Content>

