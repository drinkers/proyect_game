﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Ventas.aspx.cs" Inherits="Portal.Modulos.Mantencion.Ventas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div style="text-align: -moz-center; overflow: auto">
                <div class="panel-heading">
	               <div class="panel-title text-center">
	               		<h1 class="title">Ventas</h1>
	               		<hr />
	               	</div>
	            </div> 
                <asp:Table ID="TableVentas" runat="server" style="width:50%; margin: 0 auto">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Label ID="lblIdProducto" runat="server" Text="Id Producto" CssClass="title"></asp:Label>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtIdProducto" runat="server" class="form-control"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="btnBuscar" runat="server" Text="Buscar" OnClick="btnBuscar_Click"/> 
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Id Obligatorio" ControlToValidate="txtIdProducto"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="RegularExpressionValidator" Text="*Solo se admiten números" ControlToValidate="txtIdProducto" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Label ID="lblNombreProducto" runat="server" Text="Nombre del Producto"  CssClass="title"></asp:Label>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtNombreProducto" runat="server" ReadOnly="true" class="form-control"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Label ID="lblCategoriaProducto" runat="server" Text="Categoria Producto"  CssClass="title"></asp:Label>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:DropDownList ID="ddlCategoriaProducto" runat="server" Width="135" AutoPostBack="True" class="form-control" Enabled="false"></asp:DropDownList>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Label ID="lblValorProducto" runat="server" Text="Valor del Producto" CssClass="title"></asp:Label>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtValorProducto" runat="server" ReadOnly="true" class="form-control"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    
                    <asp:TableRow>
                        <asp:TableCell>
                            <h2>
                                <asp:Label ID="Label1" runat="server" Text=""  CssClass="title"></asp:Label></h2>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <br />
                <asp:Label ID="lblMensaje" runat="server" Text=""></asp:Label>
                <table style="width: 50%; margin: 0 auto" class="center">
                    <tr>
                        <td>
                            <asp:Button ID="btnEliminar" runat="server" Text="Eliminar" CssClass="bottom" Width="80px " OnClick="btnEliminar_Click"/>
                            <asp:Button ID="btnLimpiar" runat="server" Text="Limpiar" CssClass="bottom" Width="80px" OnClick="btnLimpiar_Click" />
                        </td>
                    </tr>
                </table>            
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
