﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LibreriaClases.BLL;

namespace Portal.Modulos.Mantencion
{
    public partial class MostrarProductos : System.Web.UI.Page
    {
        ProductoCollection collection = new ProductoCollection();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                MostrarDatos();
            }
        }

        private void MostrarDatos()
        {
            gvMostrar.DataSource = collection.ReadAll();
            gvMostrar.DataBind();
        }

    

        //private void MostrarDatos()
        //{
        //    gvMostrar.DataSource = Session["listadoproducto"] as List<Producto>;
        //    gvMostrar.DataBind();
        //}

        //protected void gvMostrar_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        //{
        //    gvMostrar.EditIndex = -1;
        //    MostrarDatos();
        //}

        //protected void gvMostrar_RowDeleting(object sender, GridViewDeleteEventArgs e)
        //{
        //    int indice = e.RowIndex;
        //    List<Producto> listado = Session["listadoproducto"] as List<Producto>;

        //    listado.RemoveAt(indice);
        //    MostrarDatos();
        //}

        //protected void gvMostrar_RowEditing(object sender, GridViewEditEventArgs e)
        //{
        //    gvMostrar.EditIndex = e.NewEditIndex;
        //    MostrarDatos();
        //}

        //protected void gvMostrar_RowUpdating(object sender, GridViewUpdateEventArgs e)
        //{
        //    List<Producto> listado = Session["listadoproducto"] as List<Producto>;

        //    string idProducto = e.NewValues["IdProducto"].ToString(); 
        //    string nombreProducto = e.NewValues["nombreProducto"].ToString();
        //    string valor = e.NewValues["Valor"].ToString();

        //    int vPrecio;
        //    int iID;

        //    int.TryParse(valor, out vPrecio);
        //    int.TryParse(idProducto, out iID);

        //    listado[e.RowIndex].NombreProducto = nombreProducto;
        //    listado[e.RowIndex].ValorProducto = vPrecio;
        //    listado[e.RowIndex].IdProducto = iID;

        //    gvMostrar.EditIndex = -1;
        //    MostrarDatos();

        //}


    }
}