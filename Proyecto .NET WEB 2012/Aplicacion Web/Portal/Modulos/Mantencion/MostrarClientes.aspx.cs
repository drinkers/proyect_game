﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LibreriaClases.BLL;

namespace Portal.Modulos.Mantencion
{
    public partial class MostrarClientes : System.Web.UI.Page
    {
        ClienteColeccion collection = new ClienteColeccion();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                MostrarDatos();
            }
        }

        private void MostrarDatos()
        {
            gvMostrar.DataSource = collection.ReadAll();
            gvMostrar.DataBind();
        }
    }
}