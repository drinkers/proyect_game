﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LibreriaClases.BLL;

namespace Portal.Modulos.Mantencion
{
    public partial class IngresoSucursales : System.Web.UI.Page
    {
        SucursalRepositorio repo = new SucursalRepositorio();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CargarDrop();
                lblMensaje.Text = string.Empty;
                Limpiar();
            }
        }

        protected void btnIngresar_Click(object sender, EventArgs e)
        {
            try
            {
                Sucursal sucursal = new Sucursal();

                sucursal.IdSucursal = int.Parse(txtIdSucursales.Text);
                sucursal.NombreSucursal = (TipoSucursal)ddlTipoSucursal.SelectedIndex;

                repo.Create(sucursal);

                lblMensaje.Text = "Agregado Correctamente!!";
            }
            catch (Exception x)
            {
                lblMensaje.Text = x.Message;
            }
        }

        protected void btnLimpiar_Click(object sender, EventArgs e)
        {
            Limpiar();
        }

        private void Limpiar()
        {
            txtIdSucursales.Text = string.Empty;
            ddlTipoSucursal.ClearSelection();
            lblMensaje.Text = string.Empty;
        }

        private void CargarDrop()
        {
            ddlTipoSucursal.DataSource = Enum.GetValues(typeof(TipoSucursal));
            ddlTipoSucursal.DataBind();
        }
    }
}