﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Vendedores.aspx.cs" Inherits="Portal.Modulos.Mantencion.Vendedores" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

    <asp:Panel ID="PanelVendedor" runat="server">
    <div class="panel-heading">
	               <div class="panel-title text-center">
	               		<h1 class="title">Ingreso de Vendedores</h1>
	               		<hr />
	               	</div>
	            </div> 
     <asp:Table ID="TableVendedor" runat="server" style="width:50%; margin: 0 auto">
            <asp:TableRow>
                <asp:TableCell>
                    <asp:Label ID="lblIdVendedor" runat="server" Text="Rut Vendedor" CssClass="title"></asp:Label>
                </asp:TableCell>
                <asp:TableCell>
                    <asp:TextBox ID="txtIdVendedor" runat="server" class="form-control"></asp:TextBox>
                </asp:TableCell>
                <asp:TableCell>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Rut Obligatorio" ControlToValidate="txtIdVendedor"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="RegularExpressionValidator" Text="*Solo se admiten números" ControlToValidate="txtIdVendedor" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                </asp:TableCell>
            </asp:TableRow>   
            <asp:TableRow>
                <asp:TableCell><asp:Label ID="lblNombreVendedor" runat="server" Text="Nombre del Vendedor" CssClass="title"></asp:Label></asp:TableCell>
                <asp:TableCell><asp:TextBox ID="txtNombreVendedor" runat="server" class="form-control"></asp:TextBox></asp:TableCell>
                <asp:TableCell>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Nombre Obligatorio" ControlToValidate="txtNombreVendedor"></asp:RequiredFieldValidator>
                </asp:TableCell>
            </asp:TableRow>

            <asp:TableRow>
                <asp:TableCell><asp:Label ID="lblApellidoPaterno" runat="server" Text="Apellido Paterno" CssClass="title"></asp:Label></asp:TableCell>
                <asp:TableCell><asp:TextBox ID="txtApellidoPaterno" runat="server" class="form-control"></asp:TextBox></asp:TableCell>
                 <asp:TableCell>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Apellido Paterno Obligatorio" ControlToValidate="txtApellidoPaterno"></asp:RequiredFieldValidator>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell><asp:Label ID="lblApellidoMaterno" runat="server" Text="Apellido Materno" CssClass="title"></asp:Label></asp:TableCell>
                <asp:TableCell><asp:TextBox ID="txtApellidoMaterno" runat="server" class="form-control"></asp:TextBox></asp:TableCell>
                <asp:TableCell>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Apellido Materno Obligatorio" ControlToValidate="txtApellidoMaterno"></asp:RequiredFieldValidator>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell><asp:Label ID="lblNuevaClave" runat="server" Text="Ingrese Clave" CssClass="title"></asp:Label></asp:TableCell>
                <asp:TableCell><asp:TextBox ID="txtClave" runat="server"  class="form-control" TextMode="Password"></asp:TextBox></asp:TableCell>
                <asp:TableCell>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Clave Obligatorio" ControlToValidate="txtClave"></asp:RequiredFieldValidator>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell><asp:Label ID="lblConfirmarClave" runat="server" Text="Confirme Clave" CssClass="title"></asp:Label></asp:TableCell>
                <asp:TableCell><asp:TextBox ID="txtRepiteClave" runat="server" class="form-control" TextMode="Password"></asp:TextBox></asp:TableCell>
                <asp:TableCell>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Confirme Clave" ControlToValidate="txtRepiteClave"></asp:RequiredFieldValidator>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                 <asp:TableCell><h2><asp:Label ID="Label1" runat="server" Text=""></asp:Label></h2></asp:TableCell>
            </asp:TableRow>
        </asp:Table>
        <br />
        <asp:Label ID="lblMensaje" runat="server" Text=""></asp:Label>
        <br />
        <asp:Button ID="btnIngresar" runat="server" Text="Ingresar" CssClass="bottom" OnClick="btnIngresar_Click" />
        <asp:Button ID="btnLimpiar" runat="server" Text="Limpiar" CssClass="bottom" OnClick="btnLimpiar_Click" />
           
    </asp:Panel>
</asp:Content>




