﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="IngresoSucursales.aspx.cs" Inherits="Portal.Modulos.Mantencion.IngresoSucursales" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
      <div>
         <div class="panel-heading">
	               <div class="panel-title text-center">
	               		<h1 class="title">Ingreso de Sucursal</h1>
	               		<hr />
	               	</div>
	            </div> 
        <asp:Table ID="TableSucursal" runat="server" style="width:50%; margin: 0 auto">
            <asp:TableRow>
                <asp:TableCell><asp:Label ID="lblIdProducto" runat="server" Text="Id sucursales" CssClass="title"></asp:Label></asp:TableCell>
                <asp:TableCell><asp:TextBox ID="txtIdSucursales" runat="server" class="form-control"></asp:TextBox></asp:TableCell>
                <asp:TableCell>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="ID Obligatorio" ControlToValidate="txtIdSucursales"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="RegularExpressionValidator" Text="*Solo se admiten números" ControlToValidate="txtIdSucursales" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                </asp:TableCell>
                
            </asp:TableRow> 
 
            <asp:TableRow>
                <asp:TableCell><asp:Label ID="lblNombreSucursal" runat="server" Text="Nombre Sucursal" CssClass="title"></asp:Label></asp:TableCell>
                <asp:TableCell>
                    <asp:DropDownList ID="ddlTipoSucursal" runat="server" Width="132" AutoPostBack="True" class="form-control"></asp:DropDownList></asp:TableCell>
            </asp:TableRow>
        </asp:Table>
        <br/>
        </div>
        <asp:Label ID="lblMensaje" runat="server" Text=""></asp:Label><br />
        <asp:Button ID="btnIngresar" runat="server" Text="Ingresar" CssClass="bottom" Width="80px " OnClick="btnIngresar_Click" />
        <asp:Button ID="btnLimpiar" runat="server" Text="Limpiar" CssClass="bottom" Width="80px " OnClick="btnLimpiar_Click" />
           
</asp:Content>
