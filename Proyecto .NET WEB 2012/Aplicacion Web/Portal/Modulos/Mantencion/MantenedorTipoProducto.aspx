﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="MantenedorTipoProducto.aspx.cs" Inherits="Portal.Modulos.Mantencion.MantenedorTipoProducto" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
 <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

    <asp:Panel ID="PanelVendedor" runat="server">
      <h1><strong><asp:Label ID="lblTitulo" runat="server" Text="Mantenedor Tipos de Producto"></asp:Label></strong></h1>
      <div>
        <asp:Table ID="TableProducto" runat="server" style="width:50%; margin: 0 auto">
            <asp:TableRow>
                <asp:TableCell><asp:Label ID="lblIdProducto" runat="server" Text="Id producto"></asp:Label></asp:TableCell>
                <asp:TableCell><asp:TextBox ID="txtIdProducto" runat="server"></asp:TextBox></asp:TableCell>
            </asp:TableRow>   
            <asp:TableRow>
                <asp:TableCell><asp:Label ID="lblNombreProducto" runat="server" Text="Nombre del Producto"></asp:Label></asp:TableCell>
                <asp:TableCell><asp:TextBox ID="txtNombreProducto" runat="server"></asp:TextBox></asp:TableCell>
            </asp:TableRow>
        </asp:Table>
        <br/>

        </div>
        <asp:Button ID="btnIngresar" runat="server" Text="Ingresar" OnClick="btnIngresar_Click" />
        <asp:Button ID="btnLimpiar" runat="server" Text="Limpiar" OnClick="btnLimpiar_Click" />
           
    </asp:Panel>

</asp:Content>
