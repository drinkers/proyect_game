﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LibreriaClases.BLL;

namespace Portal.Modulos.Mantencion
{
    public partial class MostrarSucursales : System.Web.UI.Page
    {
        SucursalCollection collection = new SucursalCollection();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                MostrarDatos();
            }
        }

        private void MostrarDatos()
        {
            gvDatos.DataSource = collection.ReadAll();
            gvDatos.DataBind();
        }
    }
}