﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="IngresoProductos.aspx.cs" Inherits="Portal.Modulos.Mantencion.IngresoProductos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div style="text-align: -moz-center; overflow: auto">
                <div class="panel-heading">
	               <div class="panel-title text-center">
	               		<h1 class="title">Ingreso de Productos</h1>
	               		<hr />
	               	</div>
	            </div> 
                <%--<asp:Table ID="Table1" runat="server" Style="width: auto; margin: 0 auto" CssClass="center">--%>
                <asp:Table ID="TableProducto" runat="server" style="width:50%; margin: 0 auto">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Label ID="lblIdProducto" runat="server" Text="Id Producto" CssClass="title"></asp:Label>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtIdProducto" runat="server" class="form-control"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell>
                             <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="ID Obligatorio" ControlToValidate="txtIdProducto"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="RegularExpressionValidator" Text="*Solo se admiten números" ControlToValidate="txtIdProducto" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Label ID="lblNombreProducto" runat="server" Text="Nombre del Producto"  CssClass="title"></asp:Label>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtNombreProducto" runat="server" class="form-control"></asp:TextBox>
                        </asp:TableCell>
                         <asp:TableCell>
                             <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Nombre Obligatorio" ControlToValidate="txtNombreProducto"></asp:RequiredFieldValidator>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Label ID="lblCategoriaProducto" runat="server" Text="Categoria Producto"  CssClass="title"></asp:Label>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:DropDownList ID="ddlCategoriaProducto" runat="server" Width="135" AutoPostBack="True" class="form-control"></asp:DropDownList>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Categoria Obligatorio" ControlToValidate="ddlCategoriaProducto" ></asp:RequiredFieldValidator>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Label ID="lblValorProducto" runat="server" Text="Valor del Producto" CssClass="title"></asp:Label>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtValorProducto" runat="server" class="form-control"></asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Valor Obligatorio" ControlToValidate="txtValorProducto"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="RegularExpressionValidator" Text="*Solo se admiten números" ControlToValidate="txtValorProducto" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Label ID="lblFechaIngreso" runat="server" Text="Fecha de Ingreso" CssClass="title"></asp:Label>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtFechaIngreso" runat="server" Enabled="false" class="form-control"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Label ID="lblSeleccionarImagen" runat="server" Text="Seleccione Imagen"  CssClass="title"></asp:Label>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:FileUpload ID="FileUpload1" runat="server"/>
                        </asp:TableCell>
                    </asp:TableRow>     
                </asp:Table>
                <br />
                <asp:Label ID="Label1" runat="server" Text=""></asp:Label><br />
                <asp:Label ID="lblMensaje" runat="server" Text=""></asp:Label>
                <table style="width: 50%; margin: 0 auto" class="center">
                    <tr>
                        <td>
                            <asp:Button ID="btnIngresar" runat="server" Text="Ingresar" OnClick="btnIngresar_Click" CssClass="bottom" Width="80px " />
                            <asp:Button ID="btnLimpiar" runat="server" Text="Limpiar" OnClick="btnLimpiar_Click" CssClass="bottom" Width="80px" />
                        </td>
                    </tr>
                </table>            
            </div>
        </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnIngresar" />
            </Triggers>
    </asp:UpdatePanel>


</asp:Content>
