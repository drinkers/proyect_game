﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="MostrarClientes.aspx.cs" Inherits="Portal.Modulos.Mantencion.MostrarClientes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <link href="../../Content/css/test1.css" rel="stylesheet" />
    <div class="panel-heading">
	               <div class="panel-title text-center">
	               		<h1 class="title">Informes de Clientes</h1>
	               	</div>
	</div> 
    <div>   
        <asp:GridView ID="gvMostrar" runat="server" CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt"  
                PageSize="6">
<AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>

<PagerStyle CssClass="pgr"></PagerStyle>
         </asp:GridView>
    </div>
</asp:Content>
