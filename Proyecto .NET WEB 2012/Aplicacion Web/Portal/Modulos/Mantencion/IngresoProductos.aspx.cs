﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using LibreriaClases.BLL;

namespace Portal.Modulos.Mantencion
{
    public partial class IngresoProductos : System.Web.UI.Page
    {
        ProductoRepositorio repo = new ProductoRepositorio();
        dim.rutinas.RutinasWeb rw = new dim.rutinas.RutinasWeb();
        LibreriaClases.BLL.Producto p = new LibreriaClases.BLL.Producto();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Inicializar();
                CargarDrop();
                Limpiar();
            }

        }

        private void Limpiar()
        {
            txtIdProducto.Text = string.Empty;
            txtNombreProducto.Text = string.Empty;
            txtValorProducto.Text = string.Empty;
            ddlCategoriaProducto.ClearSelection();
            lblMensaje.Text = string.Empty;
            Label1.Text = string.Empty;
        }

        private void CargarDrop()
        {
            this.ddlCategoriaProducto.DataSource = Enum.GetValues(typeof(TipoGenero));
            this.ddlCategoriaProducto.DataBind();
        }

        private void Comprobar()
        {
            try
            {
                Boolean fileOK = false;
                String path = Server.MapPath("~/ImagenesSubidas/");
                if (FileUpload1.HasFile)
                {
                    String fileExtension =
                        System.IO.Path.GetExtension(FileUpload1.FileName).ToLower();
                    //String[] allowedExtensions = { ".gif", ".png", ".jpeg", ".jpg" };
                    String[] allowedExtensions = {".jpg" };
                    for (int i = 0; i < allowedExtensions.Length; i++)
                    {
                        if (fileExtension == allowedExtensions[i])
                        {
                            fileOK = true;
                        }
                    }
                }

                if (fileOK)
                {
                    FileUpload1.PostedFile.SaveAs(path + txtIdProducto.Text + ".jpg");
                    //FileUpload1.PostedFile.SaveAs(path + FileUpload1.FileName);
                    Label1.Text = "Imagen Subida!";
                }
                else
                {
                    Label1.Text = "No se Acepta Este tipo de Extensiones";
                }
            }

            catch (Exception ex)
            {
                rw.MensajeScript(Page, ex.Message);
            }
        }

        private void Inicializar()
        {
            txtIdProducto.Text = string.Empty;
            txtNombreProducto.Text = string.Empty;
            txtValorProducto.Text = string.Empty;
            ddlCategoriaProducto.ClearSelection();
            txtFechaIngreso.Text = DateTime.Today.ToString("dd-MM-yyyy");
            lblMensaje.Text = string.Empty;
        }


        protected void btnIngresar_Click(object sender, EventArgs e)
        {
            try
            {
                Producto producto = new Producto();

                producto.IdProducto = int.Parse(txtIdProducto.Text);
                producto.NombreProducto = txtNombreProducto.Text;
                producto.ValorProducto = int.Parse(txtValorProducto.Text);
                producto.CategoriaProducto = (TipoGenero)ddlCategoriaProducto.SelectedIndex;

                Comprobar();

                if (repo.Create(producto) == null)
                {
                    lblMensaje.Text = "Producto Ya existe";
                }
                else
                {
                    lblMensaje.Text = "Agregado Correctamente!!";
                }
                
            }
            catch (Exception x)
            {
                lblMensaje.Text = x.Message;

            }

        }

        protected void btnLimpiar_Click(object sender, EventArgs e)
        {
            Limpiar();
        }

    }
}
