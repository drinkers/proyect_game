﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LibreriaClases.BLL;

namespace Portal.Modulos.Mantencion
{
    public partial class Vendedores : System.Web.UI.Page
    {
        VendedorRepositorio repo = new VendedorRepositorio();
        dim.rutinas.RutinasWeb rw = new dim.rutinas.RutinasWeb();
        LibreriaClases.BLL.Vendedor vendedor = new LibreriaClases.BLL.Vendedor();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Inicializar();
            }
        }

        private void Inicializar()
        {
            txtIdVendedor.Text = string.Empty;
            txtNombreVendedor.Text = string.Empty;
            txtApellidoPaterno.Text = string.Empty;
            txtApellidoMaterno.Text = string.Empty;
            txtClave.Text = string.Empty;
            txtRepiteClave.Text = string.Empty;
        }

        protected void btnIngresar_Click(object sender, EventArgs e)
        {
            String respuesta = "";
            try
            {
                Vendedor vendedor = new Vendedor();

                //Validaciones
                if (txtIdVendedor.Text == "")
                {
                    respuesta = "Error: Debe ingresar un Id para el vendedor";
                }
                if (txtNombreVendedor.Text == "")
                {
                    respuesta = "Error: Debe ingresar un Nombre para el vendedor";
                }
                if (txtApellidoPaterno.Text == "")
                {
                    respuesta = "Error: Debe ingresar un Apellido Paterno para el vendedor";
                }
                if (txtApellidoMaterno.Text == "")
                {
                    respuesta = "Error: Debe ingresar un Apellido Materno para el vendedor";
                }
                if (txtClave.Text == "")
                {
                    respuesta = "Error: Debe ingresar una Clave para el vendedor";
                }
                if (txtRepiteClave.Text == "")
                {
                    respuesta = "Error: Debe confirmar Clave para el vendedor";
                }

                vendedor.IdVendedor = int.Parse(txtIdVendedor.Text);
                vendedor.NombreVend = txtNombreVendedor.Text;
                vendedor.ApellidoP = txtApellidoPaterno.Text;
                vendedor.ApellidoM = txtApellidoMaterno.Text;
                vendedor.Clave = txtClave.Text;
                repo.Create(vendedor);
                lblMensaje.Text = "Agregado Correctamente!";
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
            }
        }

        protected void btnLimpiar_Click(object sender, EventArgs e)
        {
            Inicializar();
        }
    }
}