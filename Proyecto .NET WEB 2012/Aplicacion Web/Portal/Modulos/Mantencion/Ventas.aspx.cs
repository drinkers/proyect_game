﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LibreriaClases.BLL;

namespace Portal.Modulos.Mantencion
{
    public partial class Ventas : System.Web.UI.Page
    {
        ProductoRepositorio repo = new ProductoRepositorio();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.ddlCategoriaProducto.DataSource = Enum.GetValues(typeof(TipoGenero));
                this.ddlCategoriaProducto.DataBind();
                lblMensaje.Text = string.Empty;
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Producto producto = repo.Read(int.Parse(txtIdProducto.Text));

                if (producto == null)
                {
                    lblMensaje.Text = "Producto no existe en la base de datos";
                }
                else
                {
                    txtIdProducto.Text = producto.IdProducto.ToString();
                    txtNombreProducto.Text = producto.NombreProducto;
                    txtValorProducto.Text = producto.ValorProducto.ToString();
                    ddlCategoriaProducto.SelectedIndex = (int)producto.CategoriaProducto;
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
            }
        }

        protected void btnLimpiar_Click(object sender, EventArgs e)
        {
            Limpiar();
        }

        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                if (repo.Delete(int.Parse(txtIdProducto.Text)))
                {
                    lblMensaje.Text = "Borrado";
                    Limpiar();
                }
                else
                {
                    lblMensaje.Text = "Ooops no funciona!";
                }
            }

            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message;
            }
        }

        private void Limpiar()
        {
            txtIdProducto.Text = string.Empty;
            txtNombreProducto.Text = string.Empty;
            txtValorProducto.Text = string.Empty;
            lblMensaje.Text = string.Empty;
            ddlCategoriaProducto.ClearSelection();
        }
    }
}