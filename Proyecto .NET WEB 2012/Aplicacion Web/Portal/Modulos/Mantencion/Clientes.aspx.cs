﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LibreriaClases.BLL;

namespace Portal.Modulos.Mantencion
{
    public partial class Clientes : System.Web.UI.Page
    {
        ClienteRepositorio repo = new ClienteRepositorio();
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void btnLimpiar_Click(object sender, EventArgs e)
        {
            Limpiar();
        }

        protected void btnIngresar_Click(object sender, EventArgs e)
        {
            try
            {
                Cliente cliente = new Cliente();

                cliente.RutCliente = txtIdCliente.Text;
                cliente.NombreCliente = txtNombreVendedor.Text;
                cliente.ApellidoP = txtApellidoPaterno.Text;
                repo.Create(cliente);

                lblMensaje.Text = "Agregado Correctamente!!";
                
            }
            catch (Exception x)
            {
                lblMensaje.Text = x.Message;
            }
        }

        private void Limpiar()
        {
            txtIdCliente.Text = string.Empty;
            txtNombreVendedor.Text = string.Empty;
            txtApellidoPaterno.Text = string.Empty;
        }
    }
}